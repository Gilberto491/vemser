package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Repository.PaisRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class PaisService extends ServiceAbstract<PaisRepository, PaisEntity, Integer>{

    @Autowired
    private PaisRepository repository;

    public PaisEntity buscarPorNome(String nome){
        return repository.findByNome(nome);
    }

    public List<PaisEntity> buscarPorListaDeNomes(String nome){
        return repository.findAllByNome(nome);
    }
}