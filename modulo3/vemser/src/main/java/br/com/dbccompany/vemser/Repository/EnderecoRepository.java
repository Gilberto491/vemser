package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.*;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface EnderecoRepository extends CrudRepository<EnderecoEntity, Integer> {

    EnderecoEntity findByLogradouroAndNumeroAndComplementoAndCepAndBairro(
            String logradouro,
            int numero,
            String complemento,
            char cep,
            String bairro
    );

    List<EnderecoEntity> findAllByLogradouro (String logradouro);
    List<EnderecoEntity> findAllByNumero (int numero);
    List<EnderecoEntity> findAllByComplemento (String complemento);
    List<EnderecoEntity> findAllByCep (char cep);
    List<EnderecoEntity> findAllByBairro (String bairro);
}
