package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.PaisEntity;
import br.com.dbccompany.vemser.Service.PaisService;
import br.com.dbccompany.vemser.VemserApplication;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/api/pais")
public class PaisController extends ControllerAbstract<PaisEntity, PaisService, Integer>{

    private Logger logger = LoggerFactory.getLogger(VemserApplication.class);

    /*
        logger.info //informação
        logger.warn //aviso
        logger.error //erro

     */
}