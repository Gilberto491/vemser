package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface BancoRepository extends CrudRepository<BancoEntity, Integer> {

    BancoEntity findByNomeAndCodigo (String nome, int codigo);

    List<BancoEntity> findAllByNome(String nome);
    List<BancoEntity> findAllByCodigo (int codigo);
}
