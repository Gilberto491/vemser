package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.EnderecoEntity;
import br.com.dbccompany.vemser.Repository.CidadeRepository;
import br.com.dbccompany.vemser.Repository.EnderecoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EnderecoService extends  ServiceAbstract<EnderecoRepository, EnderecoEntity, Integer> {

    @Autowired
    private EnderecoRepository repository;

    public EnderecoEntity buscarTudo(String logradouro, int numero, String complemento, char cep, String bairro) {
        return repository.findByLogradouroAndNumeroAndComplementoAndCepAndBairro(logradouro, numero, complemento, cep, bairro);
    }

    public List<EnderecoEntity> buscarPorListaLogradouro(String logradouro){
        return repository.findAllByLogradouro(logradouro);
    }

    public List<EnderecoEntity> buscarPorListaNumero(int numero){
        return repository.findAllByNumero(numero);
    }

    public List<EnderecoEntity> buscarPorListaComplemento(String complemento){
        return repository.findAllByComplemento(complemento);
    }

    public List<EnderecoEntity> buscarPorListaCep(char cep){
        return repository.findAllByCep(cep);
    }

    public List<EnderecoEntity> buscarPorListaBairro(String bairro){
        return repository.findAllByBairro(bairro);
    }
}