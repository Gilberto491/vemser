package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.List;

@Repository
public interface ConsolidacaoRepository extends CrudRepository<ConsolidacaoEntity, Integer> {

    ConsolidacaoEntity findBySaldoAtualAndSaquesAndDepositos(double saldoAtual, double saques, double depositos);

    List<ConsolidacaoEntity> findAllBySaldoAtual (double saldoAtual);
    List<ConsolidacaoEntity> findAllBySaques (double saques);
    List<ConsolidacaoEntity> findAllByDepositos (double depositos);
    List<ConsolidacaoEntity> findAllByQtdCorrentistas (int qtdCorrentistas);
}
