package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.CidadeEntity;
import br.com.dbccompany.vemser.Repository.CidadeRepository;
import br.com.dbccompany.vemser.Repository.EstadoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class CidadeService extends ServiceAbstract<CidadeRepository, CidadeEntity, Integer>{

    @Autowired
    private CidadeRepository repository;

    public CidadeEntity buscarPorNome(String nome){
        return repository.findByNome(nome);
    }

    public List<CidadeEntity> buscarPorListaNome(String nome){
        return repository.findAllByNome(nome);
    }
}
