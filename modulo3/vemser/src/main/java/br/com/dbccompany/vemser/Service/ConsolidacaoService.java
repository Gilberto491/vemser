package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.ConsolidacaoEntity;
import br.com.dbccompany.vemser.Repository.ConsolidacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ConsolidacaoService extends ServiceAbstract<ConsolidacaoRepository, ConsolidacaoEntity, Integer>{

    @Autowired
    private ConsolidacaoRepository repository;

    public ConsolidacaoEntity buscarTudo(double saldoAtual, double saques, double depositos){
        return repository.findBySaldoAtualAndSaquesAndDepositos(saldoAtual,saques,depositos);
    }

    public List<ConsolidacaoEntity> buscarPorListaSaldoAtual(double saldoAtual){
        return repository.findAllBySaldoAtual(saldoAtual);
    }

    public List<ConsolidacaoEntity> buscarPorListaSaques(double saques){
        return repository.findAllBySaques(saques);
    }

    public List<ConsolidacaoEntity> buscarPorListaDepositos(double depositos){
        return repository.findAllByDepositos(depositos);
    }
}