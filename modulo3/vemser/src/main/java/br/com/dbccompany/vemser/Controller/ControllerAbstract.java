package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.EntityAbstract;
import br.com.dbccompany.vemser.Service.ServiceAbstract;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

public abstract class ControllerAbstract <
        T extends EntityAbstract,
        S extends ServiceAbstract, Integer> {

    @Autowired
    S service;

    //localhost: 8080 ou DNS + /api/pais + /todos
    //Header && Body
    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<T> todosEntidades(){
        return service.todos();
    }

    //Request -> <- Response
    //200, 202, 303, 404
    //Status code HTTP Resquest
    @PostMapping( value = "/novo" )
    @ResponseBody
    public T salvar(@RequestBody T entidade){
        return (T) service.salvar(entidade);
    }

    //localhost:8080/api/pais/ver/1
    //localhost:8080/api/pais/ver/2
    //localhost:8080/api/pais/ver/3
    @GetMapping( value = "/ver/{id}")
    @ResponseBody
    public T EntidadeEspecifico(@PathVariable Integer id){
        return (T) service.porId(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public T editarEntidade(@PathVariable Integer id, @RequestBody T entidade){
        return (T) service.editar(entidade, id);
    }
}
