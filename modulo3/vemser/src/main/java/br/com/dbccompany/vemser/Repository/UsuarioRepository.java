package br.com.dbccompany.vemser.Repository;

import br.com.dbccompany.vemser.Entity.Enum.EstadoCivil;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.sql.Date;
import java.util.List;
import java.util.Optional;

@Repository
public interface UsuarioRepository extends CrudRepository<UsuarioEntity, Integer> {

    UsuarioEntity findByNomeAndCpfAndEstadoCivilAndDataNascimento (String nome, String cpf, EstadoCivil estadoCivil, Date dataNascimento);

    List<UsuarioEntity> findAllByNome(String nome);
    List<UsuarioEntity> findAllByCpf (String cpf);
    List<UsuarioEntity> findAllByEstadoCivil (EstadoCivil estadoCivil);
    List<UsuarioEntity> findAllByDataNascimento (Date dataNascimento);
    Optional<UsuarioEntity> findByLogin(String login);

}