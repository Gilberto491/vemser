package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Service.AgenciaService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/agencia")
public class AgenciaController extends ControllerAbstract<AgenciaEntity, AgenciaService, Integer>{}