package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.Enum.EstadoCivil;
import br.com.dbccompany.vemser.Entity.UsuarioEntity;
import br.com.dbccompany.vemser.Repository.UsuarioRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

@Service
public class UsuarioService extends ServiceAbstract<UsuarioRepository, UsuarioEntity, Integer> {

    @Autowired
    UsuarioRepository repository;

    public UsuarioEntity buscarTudo (String nome, String cpf, EstadoCivil estadoCivil, Date dataNascimento){
        return repository.findByNomeAndCpfAndEstadoCivilAndDataNascimento(nome, cpf, estadoCivil, dataNascimento);
    }

    public List<UsuarioEntity> buscarListaPorNome (String nome){
        return repository.findAllByNome(nome);
    }

    public List<UsuarioEntity> buscarListaPorCpf (String cpf){
        return repository.findAllByCpf(cpf);
    }

    public List<UsuarioEntity> buscarListaPorEstadoCivil (EstadoCivil estadoCivil){
        return repository.findAllByEstadoCivil(estadoCivil);
    }

    public List<UsuarioEntity> buscarListaPorDataNascimento (Date dataNascimento){
        return repository.findAllByDataNascimento(dataNascimento);
    }
}