package br.com.dbccompany.vemser.Entity.Enum;

public enum EstadoCivil {

    SOLTEIRO, CASADO, DIVORCIADO, UNIAO_ESTAVEL, VIUVO
}
