package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.Enum.TipoGerente;
import br.com.dbccompany.vemser.Entity.GerenteEntity;
import br.com.dbccompany.vemser.Repository.GerenteRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class GerenteService extends ServiceAbstract<GerenteRepository, GerenteEntity, Integer>{

    @Autowired
    GerenteRepository repository;

    public GerenteEntity buscarTudo(int codFuncionario, TipoGerente tipoGerente){
        return repository.findByCodFuncionarioAndTipoGerente(codFuncionario, tipoGerente);
    }

    public List<GerenteEntity> buscarListaPorCodFuncionario(int codFuncionario){
        return repository.findAllByCodFuncionario(codFuncionario);
    }

    public List<GerenteEntity> buscarListaPorTipoGerente (TipoGerente tipoGerente){
        return repository.findAllByTipoGerente(tipoGerente);
    }
}