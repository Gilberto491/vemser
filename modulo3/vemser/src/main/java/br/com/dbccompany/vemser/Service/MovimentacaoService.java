package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.Enum.TipoMovimentacao;
import br.com.dbccompany.vemser.Entity.MovimentacaoEntity;
import br.com.dbccompany.vemser.Repository.MovimentacaoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovimentacaoService extends ServiceAbstract<MovimentacaoRepository, MovimentacaoEntity, Integer>{

    @Autowired
    MovimentacaoRepository repository;

    public MovimentacaoEntity buscarTudo(double valor, TipoMovimentacao tipoMovimentacao){
        return repository.findByValorAndTipoMovimentacao(valor, tipoMovimentacao);
    }

    public List<MovimentacaoEntity> buscarPorListaValor (double valor){
        return repository.findAllByValor(valor);
    }

    public List<MovimentacaoEntity> buscarPorListaTipoMovimentacao (TipoMovimentacao tipoMovimentacao){
        return repository.findAllByTipoMovimentacao(tipoMovimentacao);
    }
}