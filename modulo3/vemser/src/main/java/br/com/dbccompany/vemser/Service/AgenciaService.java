package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.AgenciaEntity;
import br.com.dbccompany.vemser.Repository.AgenciaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class AgenciaService extends ServiceAbstract<AgenciaRepository, AgenciaEntity, Integer>{

    @Autowired
    private AgenciaRepository repository;

    public AgenciaEntity buscarTudo(int codigo,String nome){
        return repository.findByCodigoAndNome(codigo, nome);
    }

    public List<AgenciaEntity> bucarPorListaCodigo(int codigo){
        return repository.findAllByCodigo(codigo);
    }

    public List<AgenciaEntity> buscarPorListaNome (String nome){
        return repository.findAllByNome(nome);
    }
}