package br.com.dbccompany.vemser.Entity;

import br.com.dbccompany.vemser.Entity.Enum.TipoGerente;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
@PrimaryKeyJoinColumn( name = "id")
public class GerenteEntity extends UsuarioEntity implements Serializable{

    private int codFuncionario;

    @Enumerated ( EnumType.STRING )
    private TipoGerente tipoGerente;

    @ManyToMany( cascade = CascadeType.ALL )
    @JoinTable( name = "CONTA_X_GERENTE",
            joinColumns = { @JoinColumn( name = "ID_GERENTE" ) },
            inverseJoinColumns = { @JoinColumn( name = "ID_CONTA" ), @JoinColumn( name = "ID_TIPO_CONTA") }
    )
    private List<ContaEntity> contas;

    public int getCodFuncionario() {
        return codFuncionario;
    }

    public void setCodFuncionario(int codFuncionario) {
        this.codFuncionario = codFuncionario;
    }

    public TipoGerente getTipoGerente() {
        return tipoGerente;
    }

    public void setTipoGerente(TipoGerente tipoGerente) {
        this.tipoGerente = tipoGerente;
    }

    public List<ContaEntity> getContas() {
        return contas;
    }

    public void setContas(List<ContaEntity> contas) {
        this.contas = contas;
    }
}