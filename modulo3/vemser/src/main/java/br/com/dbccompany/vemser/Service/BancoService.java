package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.BancoEntity;
import br.com.dbccompany.vemser.Repository.BancoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class BancoService extends ServiceAbstract<BancoRepository, BancoEntity, Integer>{

    @Autowired
    BancoRepository repository;

    public BancoEntity buscarTudo(String nome, int codigo){
        return repository.findByNomeAndCodigo(nome, codigo);
    }

    public List<BancoEntity> buscarPorListaNome (String nome){
        return repository.findAllByNome(nome);
    }

    public List<BancoEntity> buscarPorListaCodigo (int codigo){
        return repository.findAllByCodigo(codigo);
    }
}