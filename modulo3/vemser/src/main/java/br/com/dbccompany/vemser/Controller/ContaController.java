package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.ContaEntity;
import br.com.dbccompany.vemser.Entity.ContaEntityId;
import br.com.dbccompany.vemser.Service.ContaService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.lang.NonNull;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/conta")
public class ContaController {

    @Autowired
    ContaService service;

    @GetMapping( value = "/todos" )
    @ResponseBody
    public List<ContaEntity> todosEstados() {
        return service.todos();
    }

    @PostMapping( value = "/novo" )
    @ResponseBody
    public ContaEntity salvar(@RequestBody @NonNull ContaEntity estado){
        return service.salvar(estado);
    }

    @GetMapping( value = "/ver/{id}/{idTipoConta}")
    @ResponseBody
    public ContaEntity estadoEspecifico(@PathVariable Integer id, @PathVariable Integer idTipoConta){
        ContaEntityId newId = new ContaEntityId(id, idTipoConta);
        return service.porId(newId);
    }
}