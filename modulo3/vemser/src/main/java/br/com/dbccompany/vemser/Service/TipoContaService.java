package br.com.dbccompany.vemser.Service;

import br.com.dbccompany.vemser.Entity.TipoContaEntity;
import br.com.dbccompany.vemser.Repository.TipoContaRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class TipoContaService extends ServiceAbstract<TipoContaRepository, TipoContaEntity, Integer>{

    @Autowired
    TipoContaRepository repository;

    public TipoContaEntity buscarPorNome (String nome){
        return repository.findByNome(nome);
    }

    public List<TipoContaEntity> buscarPorListaNome (String nome){
        return repository.findAllByNome(nome);
    }
}