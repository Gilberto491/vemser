package br.com.dbccompany.vemser.Controller;

import br.com.dbccompany.vemser.Entity.MovimentacaoEntity;
import br.com.dbccompany.vemser.Service.MovimentacaoService;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;

@Controller
@RequestMapping("/api/movimentacao")
public class MovimentacaoController extends ControllerAbstract<MovimentacaoEntity, MovimentacaoService, Integer>{ }