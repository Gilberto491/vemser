package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Entity.Enum.TipoPagamento;
import br.com.coworking.coworking.Entity.PagamentosEntity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class PagamentosDTO extends AbstractDTO<Integer>{

    private Integer id;
    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;
    private ClientesPacotesEntity clientesPacotes;
    @Enumerated(EnumType.STRING)
    private ContratacaoEntity contratacao;

    public PagamentosDTO (PagamentosEntity pagamentos){
        this.id = pagamentos.getId();
        this.tipoPagamento = pagamentos.getTipoPagamento();
        this.clientesPacotes = pagamentos.getClientesPacotes();
        this.contratacao = pagamentos.getContratacao();

    }

    public PagamentosEntity convert(){
        PagamentosEntity pagamentos = new PagamentosEntity();
        pagamentos.setId(this.id);
        pagamentos.setTipoPagamento(this.tipoPagamento);
        pagamentos.setClientesPacotes(this.clientesPacotes);
        pagamentos.setContratacao(this.contratacao);

        return pagamentos;
    }

    public PagamentosDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientesPacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }
}
