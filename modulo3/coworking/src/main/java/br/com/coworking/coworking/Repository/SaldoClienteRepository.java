package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import br.com.coworking.coworking.Entity.SaldoClienteId;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface SaldoClienteRepository extends CrudRepository<SaldoClienteEntity, SaldoClienteId> {

    SaldoClienteEntity findByQuantidade(int quantidade);
    SaldoClienteEntity findByVencimento(String vencimento);
}