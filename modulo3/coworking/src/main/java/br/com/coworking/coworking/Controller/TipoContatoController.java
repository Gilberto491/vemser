package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.TipoContatoDTO;
import br.com.coworking.coworking.Entity.TipoContatoEntity;
import br.com.coworking.coworking.Service.TipoContatoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/tipoContato")
public class TipoContatoController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    TipoContatoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<TipoContatoDTO> todosTipoContato(){
        logger.info("Listando Tipo Contato");
        logger.warn("Tipo Contato pode não ter sido criado");
        return service.retornarListaTipoContato();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public TipoContatoEntity salvarTipoContato(@RequestBody TipoContatoDTO dto){
        logger.info("Novo Tipo Contato");
        return service.salvarTipoContato(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public TipoContatoDTO tipoContatoEspecifico(@PathVariable Integer id){
        logger.info("Vendo Tipo Contato Especifico");
        logger.warn("Tipo Contato pode não ter sido criado");
        return service.retornarUsuarioEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public TipoContatoEntity editarTipoContato(@PathVariable Integer id, @RequestBody TipoContatoDTO dto){
        logger.info("Editando Tipo Contato");
        logger.warn("Tipo Contato pode não ter sido criado");
        return service.editarUsuarios(id, dto);
    }
}