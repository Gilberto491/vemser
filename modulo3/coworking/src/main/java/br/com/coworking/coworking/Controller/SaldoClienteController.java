package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.SaldoClienteDTO;
import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import br.com.coworking.coworking.Entity.SaldoClienteId;
import br.com.coworking.coworking.Service.SaldoClienteService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/saldoCliente")
public class SaldoClienteController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    SaldoClienteService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<SaldoClienteDTO> todosSaldoCliente(){
        logger.info("Listando Saldo Cliente");
        logger.warn("Saldo Cliente pode não ter sido criado");
        return service.retornarListaSaldoCliente();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public SaldoClienteEntity salvarSaldoCliente(@RequestBody SaldoClienteDTO dto){
        logger.info("Novo Saldo Cliente");
        return service.salvarSaldoCliente(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public SaldoClienteDTO saldoClienteEspecifico(@PathVariable SaldoClienteId id){
        logger.info("Vendo Saldo Cliente Especifico");
        logger.warn("Saldo Cliente pode não ter sido criado");
        return service.retornarSaldoClienteEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public SaldoClienteEntity editarSaldoCliente(@PathVariable SaldoClienteId id, @RequestBody SaldoClienteDTO dto){
        logger.info("Editando Saldo Cliente");
        logger.warn("Saldo Cliente pode não ter sido criado");
        return service.editarSaldoCliente(id, dto);
    }
}