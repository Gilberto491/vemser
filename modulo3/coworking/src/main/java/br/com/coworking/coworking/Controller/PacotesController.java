package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.PacotesDTO;
import br.com.coworking.coworking.Entity.PacotesEntity;
import br.com.coworking.coworking.Service.PacotesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pacotes")
public class PacotesController{

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    PacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<PacotesDTO> todosPacotes(){
        logger.info("Listando Pacotes");
        logger.warn("Pacotes pode não ter sido criado");
        return service.retornarPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public PacotesEntity salvarPacotes(@RequestBody PacotesDTO dto){
        logger.info("Novo Pacote");
        return service.salvarPacotes(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PacotesDTO pacotesEspecifico(@PathVariable Integer id){
        logger.info("Vendo Pacote Especifico");
        logger.warn("Pacotes pode não ter sido criado");
        return service.retornarPacotesEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PacotesEntity editarPacotes(@PathVariable Integer id, @RequestBody PacotesDTO dto){
        logger.info("Editando Pacote");
        logger.warn("Pacotes pode não ter sido criado");
        return service.editarPacotes(id,dto);
    }
}