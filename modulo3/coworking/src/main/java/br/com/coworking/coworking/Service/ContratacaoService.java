package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.ContratacaoDTO;
import br.com.coworking.coworking.Entity.AcessosEntity;
import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Entity.Enum.TipoContratacao;
import br.com.coworking.coworking.Repository.ContratacaoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContratacaoService extends AbstractService<ContratacaoRepository, ContratacaoEntity, Integer>{

    public List<ContratacaoDTO> retornarListaContratacao(){
        List<ContratacaoDTO> listaDTO = new ArrayList<>();
        for (ContratacaoEntity contratacao : this.todos()){
            listaDTO.add(new ContratacaoDTO(contratacao));
        }
        return listaDTO;
    }

    public ContratacaoEntity salvarContratacao(ContratacaoDTO dto){
        ContratacaoEntity contratacao = this.salvar(dto.convert());
        return contratacao;
    }

    public ContratacaoDTO retornarContracaoEspecifico(Integer id){
        ContratacaoDTO contratacao = new ContratacaoDTO(this.porId(id));
        return contratacao;
    }

    public ContratacaoEntity editarContratacao(Integer id, ContratacaoDTO dto){
        ContratacaoEntity contratacao = this.editar(dto.convert(), id);
        return contratacao;
    }
}