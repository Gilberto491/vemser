package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.PagamentosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PagamentosRepository extends CrudRepository<PagamentosEntity, Integer> {}