package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Entity.EspacosEntity;
import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import br.com.coworking.coworking.Entity.SaldoClienteEntity;

import java.util.List;

public class EspacosDTO extends AbstractDTO<Integer>{

    private Integer id;
    private String nome;
    private int qtdPessoas;
    private double valor;
    private List<SaldoClienteEntity> saldosClientes;
    private List<EspacosPacotesEntity> espacosPacotes;
    private List<ContratacaoEntity> contratacoes;

    public EspacosDTO (EspacosEntity espacos){
        this.id = espacos.getId();
        this.nome = espacos.getNome();
        this.qtdPessoas = espacos.getQtdPessoas();
        this.valor = espacos.getValor();
        this.saldosClientes = espacos.getSaldosClientes();
        this.espacosPacotes = espacos.getEspacosPacotes();
        this.contratacoes = espacos.getContratacoes();
    }

    public EspacosEntity convert(){
        EspacosEntity espacos = new EspacosEntity();
        espacos.setId(this.id);
        espacos.setNome(this.nome);
        espacos.setQtdPessoas(this.qtdPessoas);
        espacos.setValor(this.valor);
        espacos.setSaldosClientes(this.saldosClientes);
        espacos.setEspacosPacotes(this.espacosPacotes);
        espacos.setContratacoes(this.contratacoes);

        return espacos;
    }

    public EspacosDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public int getQtdPessoas() {
        return qtdPessoas;
    }

    public void setQtdPessoas(int qtdPessoas) {
        this.qtdPessoas = qtdPessoas;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

}
