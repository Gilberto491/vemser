package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.AcessosDTO;
import br.com.coworking.coworking.Entity.AcessosEntity;
import br.com.coworking.coworking.Service.AcessosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/acessos")
public class AcessosController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    AcessosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<AcessosDTO> todosAcessos(){
        logger.info("Listando Acessos");
        logger.warn("Acessos pode não ter sido criado");
        return service.retornarListaAcessos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public AcessosEntity salvarAcessos(@RequestBody AcessosDTO dto){
        logger.info("Novo Acessos");
        return service.salvarAcessos(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public AcessosDTO acessosEspecifico(@PathVariable Integer id){
        logger.info("Vendo Acessos Especifico");
        logger.warn("Acessos pode não ter sido criado");
        return service.retornarAcessosEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public AcessosEntity editarAcessos(@PathVariable Integer id, @RequestBody AcessosDTO dto){
        logger.info("Editando Acessos");
        logger.warn("Acessos pode não ter sido criado");
        return service.editarAcessos(id, dto);
    }
}