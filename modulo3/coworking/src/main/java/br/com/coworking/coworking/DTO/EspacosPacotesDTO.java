package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.Enum.TipoContratacao;
import br.com.coworking.coworking.Entity.EspacosEntity;
import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import br.com.coworking.coworking.Entity.PacotesEntity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;

public class EspacosPacotesDTO extends AbstractDTO<Integer>{

    private Integer id;
    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;
    private int quantidade;
    private int prazo;
    private EspacosEntity espacos;
    private PacotesEntity pacotes;

    public EspacosPacotesDTO (EspacosPacotesEntity espacosPacotes){
        this.id = espacosPacotes.getId();
        this.tipoContratacao = espacosPacotes.getTipoContratacao();
        this.quantidade = espacosPacotes.getQuantidade();
        this.prazo = espacosPacotes.getPrazo();
        this.espacos = espacosPacotes.getEspacos();
        this.pacotes = espacosPacotes.getPacotes();

    }

    public EspacosPacotesEntity convert(){
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        espacosPacotes.setId(this.id);
        espacosPacotes.setTipoContratacao(this.tipoContratacao);
        espacosPacotes.setQuantidade(this.quantidade);
        espacosPacotes.setPrazo(this.prazo);
        espacosPacotes.setEspacos(this.espacos);
        espacosPacotes.setPacotes(this.pacotes);

        return espacosPacotes;
    }

    public EspacosPacotesDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }
}
