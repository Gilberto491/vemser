package br.com.coworking.coworking.Entity;

import javax.persistence.*;

@Entity
public class ContatoEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "CONTATO_SEQ", sequenceName = "CONTATO_SEQ")
    @GeneratedValue( generator = "CONTATO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private String valor;

    @ManyToOne
    @JoinColumn( name = "ID_TIPO_CONTATO")
    private TipoContatoEntity tipoContato;

    @ManyToOne
    @JoinColumn( name = "ID_CLIENTES")
    private ClientesEntity clientes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }
}
