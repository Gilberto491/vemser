package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.TipoContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TipoContatoRepository extends CrudRepository<TipoContatoEntity, Integer> {

    TipoContatoEntity findByNome(String nome);
}