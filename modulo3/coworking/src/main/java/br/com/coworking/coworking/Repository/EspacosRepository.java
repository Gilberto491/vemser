package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.EspacosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosRepository extends CrudRepository<EspacosEntity, Integer> {

    EspacosEntity findByNome(String nome);
    EspacosEntity findByValor(double valor);
    EspacosEntity findByQtdPessoas(int qtdPessoas);
}