package br.com.coworking.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class PacotesEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "PACOTES_SEQ", sequenceName = "PACOTES_SEQ")
    @GeneratedValue( generator = "PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private double valor;

    @OneToMany( mappedBy = "pacotes" )
    private List<ClientesPacotesEntity> clientesPacotes;

    @OneToMany( mappedBy = "pacotes" )
    private List<EspacosPacotesEntity> espacosPacotes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }
}
