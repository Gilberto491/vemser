package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.AcessosEntity;
import br.com.coworking.coworking.Entity.ClientesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import java.util.Date;

@Repository
public interface ClientesRepository extends CrudRepository<ClientesEntity, Integer> {

    ClientesEntity findByCpf(String cpf);
    ClientesEntity findByNome(String nome);
    ClientesEntity findByDataNascimento(String dataNascimento);

}