package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.UsuariosDTO;
import br.com.coworking.coworking.Entity.UsuariosEntity;
import br.com.coworking.coworking.Service.UsuariosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/usuarios")
public class UsuariosController {

    @Autowired
    UsuariosService service;

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<UsuariosDTO> todosUsuarios(){
        logger.info("Listando Usuários");
        logger.warn("Usuario pode não ter sido criado");
        return service.retornarListaUsuario();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public UsuariosEntity salvarUsuarios(@RequestBody UsuariosDTO dto){
        logger.info("Novo usuário");
        return service.salvarComEncriptografiaDeSenha(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public UsuariosDTO usuariosEspecifico(@PathVariable Integer id){
        logger.info("Vendo Usuário Especifico");
        logger.warn("Usuário pode não ter sido criado");
        return service.retornarUsuarioEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public UsuariosEntity editarUsuarios(@PathVariable Integer id, @RequestBody UsuariosDTO dto){
        logger.info("Editando Usuário");
        logger.warn("Usuário pode não ter sido criado");
        return service.editarUsuarios(id, dto);
    }

    @PostMapping (value = "/login")
    @ResponseBody
    public boolean fazerLogin(@RequestBody UsuariosDTO login){
        logger.info("Logando no sistema");
        logger.warn("Login pode estar incorreto");
        return service.login(login.getLogin(), login.getSenha());
    }
}