package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.EspacosPacotesDTO;
import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import br.com.coworking.coworking.Repository.EspacosPacotesRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacosPacotesService extends AbstractService<EspacosPacotesRepository, EspacosPacotesEntity, Integer>{

    public List<EspacosPacotesDTO> retornarEspacosPacotes(){
        List<EspacosPacotesDTO> listaDTO = new ArrayList<>();
        for (EspacosPacotesEntity espacosPacotes : this.todos()){
            listaDTO.add(new EspacosPacotesDTO(espacosPacotes));
        }
        return listaDTO;
    }

    public EspacosPacotesEntity salvarEspacosPacotes(EspacosPacotesDTO dto){
        EspacosPacotesEntity espacosPacotes = this.salvar(dto.convert());
        return espacosPacotes;
    }

    public EspacosPacotesDTO retornarEspacosPacotesEspecifico(Integer id){
        EspacosPacotesDTO espacosPacotes = new EspacosPacotesDTO(this.porId(id));
        return espacosPacotes;
    }

    public EspacosPacotesEntity editarEspacosPacotes(Integer id, EspacosPacotesDTO dto){
        EspacosPacotesEntity espacosPacotes = this.editar(dto.convert(), id);
        return espacosPacotes;
    }
}