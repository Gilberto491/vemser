package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.*;

import java.util.List;

public class ClientesDTO extends AbstractDTO<Integer>{

    private Integer id;
    private String nome;
    private String cpf;
    private String dataNascimento;
    private List<ContatoEntity> contato;
    private List<ClientesPacotesEntity> clientesPacotes;
    private List<ContratacaoEntity> contratacoes;
    private List<SaldoClienteEntity> saldosClientes;

    public ClientesDTO (ClientesEntity clientes){
       this.id = clientes.getId();
       this.nome = clientes.getNome();
       this.cpf = clientes.getCpf();
       this.dataNascimento = clientes.getDataNascimento();
       this.contato = clientes.getContato();
       this.clientesPacotes = clientes.getClientesPacotes();
       this.contratacoes = clientes.getContratacoes();
       this.saldosClientes = clientes.getSaldosClientes();
    }

    public ClientesDTO(){}

    public ClientesEntity convert(){
        ClientesEntity clientes = new ClientesEntity();

        clientes.setId(this.id);
        clientes.setNome(this.nome);
        clientes.setCpf(this.cpf);
        clientes.setDataNascimento(this.dataNascimento);
        clientes.setContato(this.contato);
        clientes.setClientesPacotes(this.clientesPacotes);
        clientes.setContratacoes(this.contratacoes);
        clientes.setSaldosClientes(this.saldosClientes);

        return clientes;
    }

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getCpf() {
        return cpf;
    }

    public void setCpf(String cpf) {
        this.cpf = cpf;
    }

    public String getDataNascimento() {
        return dataNascimento;
    }

    public void setDataNascimento(String dataNascimento) {
        this.dataNascimento = dataNascimento;
    }

    public List<ContatoEntity> getContato() {
        return contato;
    }

    public void setContato(List<ContatoEntity> contato) {
        this.contato = contato;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<ContratacaoEntity> getContratacoes() {
        return contratacoes;
    }

    public void setContratacoes(List<ContratacaoEntity> contratacoes) {
        this.contratacoes = contratacoes;
    }

    public List<SaldoClienteEntity> getSaldosClientes() {
        return saldosClientes;
    }

    public void setSaldosClientes(List<SaldoClienteEntity> saldosClientes) {
        this.saldosClientes = saldosClientes;
    }
}
