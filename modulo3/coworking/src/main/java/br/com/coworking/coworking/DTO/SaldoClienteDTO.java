package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.*;
import br.com.coworking.coworking.Entity.Enum.TipoContratacao;

import javax.persistence.EmbeddedId;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

public class SaldoClienteDTO extends AbstractDTO<SaldoClienteId>{

    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;
    private int quantidade;
    private String vencimento;
    private ClientesEntity clientes;
    private EspacosEntity espacos;
    private List<AcessosEntity> acessos;

    public SaldoClienteDTO (SaldoClienteEntity saldoCliente){
        this.id = saldoCliente.getId();
        this.quantidade = saldoCliente.getQuantidade();
        this.vencimento = saldoCliente.getVencimento();
        this.clientes = saldoCliente.getClientes();
        this.espacos = saldoCliente.getEspacos();
        this.acessos = saldoCliente.getAcessos();

    }

    public SaldoClienteEntity convert(){
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setId(this.id);
        saldoCliente.setQuantidade(this.quantidade);
        saldoCliente.setVencimento(this.vencimento);
        saldoCliente.setClientes(this.clientes);
        saldoCliente.setEspacos(this.espacos);
        saldoCliente.setAcessos(this.acessos);

        return saldoCliente;
    }

    public SaldoClienteDTO(){}

    @Override
    public SaldoClienteId getId() {
        return id;
    }

    @Override
    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public List<AcessosEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosEntity> acessos) {
        this.acessos = acessos;
    }
}
