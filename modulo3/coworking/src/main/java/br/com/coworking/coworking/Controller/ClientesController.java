package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.ClientesDTO;
import br.com.coworking.coworking.Entity.ClientesEntity;
import br.com.coworking.coworking.Service.ClientesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientes")
public class ClientesController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    ClientesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesDTO> todosClientes(){
        logger.info("Listando Clientes");
        logger.warn("Clientes pode não ter sido criado");
        return service.retornarListaClientes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesEntity salvarClientes(@RequestBody ClientesDTO dto){
        logger.info("Novo Cliente");
        return service.salvarClientes(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClientesDTO clientesEspecifico(@PathVariable Integer id){
        logger.info("Vendo Clientes Especifico");
        logger.warn("Clientes pode não ter sido criado");
        return service.retornarClientesEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesEntity editarClientes(@PathVariable Integer id, @RequestBody ClientesDTO dto){
        logger.info("Editando Clientes");
        logger.warn("Clientes pode não ter sido criado");
        return service.editarClientes(id, dto);
    }
}