package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.Convert.ConverterValor;
import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.AcessosDTO;
import br.com.coworking.coworking.Entity.AcessosEntity;
import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Entity.Enum.TipoContratacao;
import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import br.com.coworking.coworking.Entity.SaldoClienteId;
import br.com.coworking.coworking.Exception.ExceptionDescontoDoSaldo;
import br.com.coworking.coworking.Exception.ValorInvalidoNoDescontoDoSaldo;
import br.com.coworking.coworking.Repository.AcessosRepository;
import br.com.coworking.coworking.Repository.SaldoClienteRepository;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class AcessosService extends AbstractService<AcessosRepository, AcessosEntity, Integer> {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    public List<AcessosDTO> retornarListaAcessos(){
        List<AcessosDTO> listaDTO = new ArrayList<>();
        for (AcessosEntity acessos : this.todos()){
            listaDTO.add(new AcessosDTO(acessos));
        }
        return listaDTO;
    }

    public AcessosEntity salvarAcessos(AcessosDTO dto){
        AcessosEntity acessos = this.salvar(dto.convert());
        return acessos;
    }

    public AcessosDTO retornarAcessosEspecifico(Integer id){
        AcessosDTO acessos = new AcessosDTO(this.porId(id));
        return acessos;
    }

    public AcessosEntity editarAcessos(Integer id, AcessosDTO dto){
        AcessosEntity acessos = this.editar(dto.convert(), id);
        return acessos;
    }

    @Autowired
    SaldoClienteRepository saldoClienteRepository;

    /*
        Método para caso seja
        entrada
    */

    public AcessosDTO salvarEntrada (AcessosDTO acessos){
        if( acessos.isEntrada()){
            SaldoClienteId id = acessos.getSaldoCliente().getId();
            SaldoClienteEntity saldo = saldoClienteRepository.findById(id).get();
           if(saldo.getQuantidade() <= 0){
               throw new NullPointerException(); //TRATAMENTO DE ERRO
           }
        }
        AcessosEntity acessosEntity = acessos.convert();
        AcessosDTO newDto = new AcessosDTO(super.salvar(acessosEntity));
        return newDto;
    }

    /*
        Método para validar se o saldo
        do cliente é suficiente e retornar
        valor já tratado
    */

    public String validar(SaldoClienteEntity saldoClienteEntity){
        ConverterValor converter = new ConverterValor();
        if(saldoClienteEntity.getQuantidade() <= 0){
            logger.error("Saldo insuficiente");
        }
         return converter.converter(saldoClienteEntity.getQuantidade());
    }

    /*
        Método para descontar o saldo
        conforme cada regra
    */

    public String descontoDoSaldo(ContratacaoEntity contratacao){

        AcessosEntity acessosEntity = new AcessosEntity();
        ConverterValor converterValor = new ConverterValor();
        ExceptionDescontoDoSaldo erro = new ValorInvalidoNoDescontoDoSaldo();

        if (contratacao.getTipoContratacao() == TipoContratacao.MINUTO){
            return converterValor.converter(contratacao.getEspacos().getValor() * (contratacao.getQuantidade() / 60));
        }
        if (contratacao.getTipoContratacao() == TipoContratacao.HORAS){
            return converterValor.converter(contratacao.getEspacos().getValor() * contratacao.getQuantidade());
        }
        if (contratacao.getTipoContratacao() == TipoContratacao.DIARIAS){
            return converterValor.converter(contratacao.getEspacos().getValor() * (24 * contratacao.getQuantidade()));
        }
        if (contratacao.getTipoContratacao() == TipoContratacao.TURNO){
            return converterValor.converter(contratacao.getEspacos().getValor() * (5 * contratacao.getQuantidade()));
        }
        if (contratacao.getTipoContratacao() == TipoContratacao.SEMANAS){
            return converterValor.converter(contratacao.getEspacos().getValor() * (168 * contratacao.getQuantidade()));
        }
        if(contratacao.getTipoContratacao() == TipoContratacao.MESES){
            return converterValor.converter(contratacao.getEspacos().getValor() * (730 * contratacao.getQuantidade()));
        }

        return erro.getMensagem();
    }


}