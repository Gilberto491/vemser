package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ContatoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContatoRepository extends CrudRepository<ContatoEntity, Integer> {

    ContatoEntity findByValor(String valor);
}