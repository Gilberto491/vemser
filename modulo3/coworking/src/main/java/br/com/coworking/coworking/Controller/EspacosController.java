package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.EspacosDTO;
import br.com.coworking.coworking.Entity.EspacosEntity;
import br.com.coworking.coworking.Service.EspacosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacos")
public class EspacosController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    EspacosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EspacosDTO> todosEspacos(){
        logger.info("Listando Espaços");
        logger.warn("Espaços pode não ter sido criado");
        return service.retornarListaEspacos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacosEntity salvarEspacos(@RequestBody EspacosDTO dto){
        logger.info("Novo Espaço");
        return service.salvarEspacos(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public EspacosDTO EspacosEspecifico(@PathVariable Integer id){
        logger.info("Vendo Espaço Especifico");
        logger.warn("Espaços pode não ter sido criado");
        return service.retornarEspacosEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacosEntity editarEspacos(@PathVariable Integer id, @RequestBody EspacosDTO dto){
        logger.info("Editando Espaços");
        logger.warn("Espaços pode não ter sido criado");
        return service.editarEspacos(id, dto);
    }
}