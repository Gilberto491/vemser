package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.PagamentosDTO;
import br.com.coworking.coworking.Entity.PagamentosEntity;
import br.com.coworking.coworking.Repository.PagamentosRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PagamentosService extends AbstractService<PagamentosRepository, PagamentosEntity, Integer>{

    public List<PagamentosDTO> retornarListaPagamentos(){
        List<PagamentosDTO> listaDTO = new ArrayList<>();
        for (PagamentosEntity pagamentos : this.todos()){
            listaDTO.add(new PagamentosDTO(pagamentos));
        }
        return listaDTO;
    }

    public PagamentosEntity salvarPagamentos(PagamentosDTO dto){
        PagamentosEntity pagamentos = this.salvar(dto.convert());
        return pagamentos;
    }

    public PagamentosDTO retornarPagamentosEspecifico(Integer id){
        PagamentosDTO pagamentos = new PagamentosDTO(this.porId(id));
        return pagamentos;
    }

    public PagamentosEntity editarPagamentos(Integer id, PagamentosDTO dto){
        PagamentosEntity pagamentos = this.editar(dto.convert(), id);
        return pagamentos;
    }
}