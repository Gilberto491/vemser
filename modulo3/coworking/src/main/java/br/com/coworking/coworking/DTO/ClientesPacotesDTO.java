package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ClientesEntity;
import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import br.com.coworking.coworking.Entity.PacotesEntity;
import br.com.coworking.coworking.Entity.PagamentosEntity;

import java.util.List;

public class ClientesPacotesDTO extends AbstractDTO<Integer>{

    private Integer id;
    private int quantidade;
    private ClientesEntity clientes;
    private PacotesEntity pacotes;
    private List<PagamentosEntity> pagamentos;

    public ClientesPacotesDTO (ClientesPacotesEntity clientesPacotes){
        this.id = clientesPacotes.getId();
        this.quantidade = clientesPacotes.getQuantidade();
        this.clientes = clientesPacotes.getClientes();
        this.pacotes = clientesPacotes.getPacotes();
        this.pagamentos = clientesPacotes.getPagamentos();
    }

    public ClientesPacotesEntity convert(){
        ClientesPacotesEntity clientesPacotes = new ClientesPacotesEntity();
        clientesPacotes.setId(this.id);
        clientesPacotes.setQuantidade(this.quantidade);
        clientesPacotes.setClientes(this.clientes);
        clientesPacotes.setPacotes(this.pacotes);
        clientesPacotes.setPagamentos(this.pagamentos);

        return clientesPacotes;
    }

    public ClientesPacotesDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
