package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ClientesPacotesRepository extends CrudRepository<ClientesPacotesEntity, Integer> {

    ClientesPacotesEntity findByQuantidade(int quantidade);
}