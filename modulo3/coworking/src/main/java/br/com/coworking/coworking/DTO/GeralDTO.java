package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.*;

public class GeralDTO {

    private AcessosEntity acessos;
    private ClientesEntity clientes;
    private ClientesPacotesEntity clientesPacotes;
    private ContatoEntity contatos;
    private ContratacaoEntity contatacao;
    private EspacosEntity espacos;
    private EspacosPacotesEntity espacosPacotes;
    private PacotesEntity pacotes;
    private PagamentosEntity pagamentos;
    private SaldoClienteEntity saldoCliente;
    private TipoContatoEntity tipoContato;
    private UsuariosEntity usuarios;

    public GeralDTO(){}

    public AcessosEntity getAcessos() {
        return acessos;
    }

    public void setAcessos(AcessosEntity acessos) {
        this.acessos = acessos;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public ClientesPacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ContatoEntity getContatos() {
        return contatos;
    }

    public void setContatos(ContatoEntity contatos) {
        this.contatos = contatos;
    }

    public ContratacaoEntity getContatacao() {
        return contatacao;
    }

    public void setContatacao(ContratacaoEntity contatacao) {
        this.contatacao = contatacao;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public EspacosPacotesEntity getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(EspacosPacotesEntity espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }

    public PagamentosEntity getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(PagamentosEntity pagamentos) {
        this.pagamentos = pagamentos;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public UsuariosEntity getUsuarios() {
        return usuarios;
    }

    public void setUsuarios(UsuariosEntity usuarios) {
        this.usuarios = usuarios;
    }
}
