package br.com.coworking.coworking.Entity.Enum;

public enum TipoPagamento {
    CREDITO, DEBITO, DINHEIRO, TRANSFERENCIA
}
