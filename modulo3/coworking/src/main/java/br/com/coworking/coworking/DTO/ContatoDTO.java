package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ClientesEntity;
import br.com.coworking.coworking.Entity.ContatoEntity;
import br.com.coworking.coworking.Entity.TipoContatoEntity;

public class ContatoDTO extends AbstractDTO<Integer>{

    private Integer id;
    private String valor;
    private TipoContatoEntity tipoContato;
    private ClientesEntity clientes;

    public ContatoDTO (ContatoEntity contato){
        this.id = contato.getId();
        this.valor = contato.getValor();
        this.tipoContato = contato.getTipoContato();
        this.clientes = contato.getClientes();
    }

    public ContatoEntity convert(){
        ContatoEntity contato = new ContatoEntity();
        contato.setId(this.id);
        contato.setValor(this.valor);
        contato.setTipoContato(this.tipoContato);
        contato.setClientes(this.clientes);

        return contato;
    }

    public ContatoDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getValor() {
        return valor;
    }

    public void setValor(String valor) {
        this.valor = valor;
    }

    public TipoContatoEntity getTipoContato() {
        return tipoContato;
    }

    public void setTipoContato(TipoContatoEntity tipoContato) {
        this.tipoContato = tipoContato;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }
}
