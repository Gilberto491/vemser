package br.com.coworking.coworking.Entity;

import br.com.coworking.coworking.Entity.Enum.TipoContratacao;

import javax.persistence.*;

@Entity
public class EspacosPacotesEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "ESPACOS_PACOTES_SEQ", sequenceName = "ESPACOS_PACOTES_SEQ")
    @GeneratedValue( generator = "ESPACOS_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    private int quantidade;
    private int prazo; //EM DIAS

    @ManyToOne
    @JoinColumn( name = "ID_ESPACOS" )
    private EspacosEntity espacos;

    @ManyToOne
    @JoinColumn( name = "ID_PACOTES" )
    private PacotesEntity pacotes;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }
}
