package br.com.coworking.coworking.Entity;

import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;

@Entity
public class AcessosEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "ACESSOS_SEQ", sequenceName = "ACESSOS_SEQ")
    @GeneratedValue( generator = "ACESSOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private String data;
    private boolean isexcecao = false;
    private boolean isEntrada = false;

    @ManyToOne
    @JoinColumns({
            @JoinColumn( name = "ID_CLIENTES"),
            @JoinColumn( name = "ID_ESPACOS")
    })
    private SaldoClienteEntity saldoCliente;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isIsexcecao() {
        return isexcecao;
    }

    public void setIsexcecao(boolean isexcecao) {
        this.isexcecao = isexcecao;
    }

    public boolean isEntrada() {
        return this.isEntrada;
    }

    public void setEntrada(boolean entrada) {
        this.isEntrada = entrada;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
