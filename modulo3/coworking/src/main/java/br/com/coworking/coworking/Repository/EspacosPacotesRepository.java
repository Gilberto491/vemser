package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface EspacosPacotesRepository extends CrudRepository<EspacosPacotesEntity, Integer> {

    EspacosPacotesEntity findByPrazo(int prazo);
    EspacosPacotesEntity findByQuantidade(int quantidade);

}