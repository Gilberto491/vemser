package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ContatoEntity;
import br.com.coworking.coworking.Entity.TipoContatoEntity;

import java.util.List;

public class TipoContatoDTO extends AbstractDTO<Integer>{

    private Integer id;
    private String nome;
    private List<ContatoEntity> contatos;

    /*
         Criando dois tipo de contato para que eles
         sempre venham criados. Dessa forma o usuário tem que colocar
         o telefone e o whatsapp
    */

    private TipoContatoEntity telefone = new TipoContatoEntity();
    private TipoContatoEntity whatsapp = new TipoContatoEntity();


    public TipoContatoDTO (TipoContatoEntity tipoContato){
        this.id = tipoContato.getId();
        this.nome = tipoContato.getNome();
        this.contatos = tipoContato.getContatos();
    }

    public TipoContatoEntity convert(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setId(this.id);
        tipoContato.setNome(this.nome);
        tipoContato.setContatos(this.contatos);

        return tipoContato;
    }

    public TipoContatoDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public List<ContatoEntity> getContatos() {
        return contatos;
    }

    public void setContatos(List<ContatoEntity> contatos) {
        this.contatos = contatos;
    }
}
