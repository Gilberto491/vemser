package br.com.coworking.coworking.Entity;

import javax.persistence.*;
import java.util.List;

@Entity
public class ClientesPacotesEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "CLIENTES_PACOTES_SEQ", sequenceName = "CLIENTES_PACOTES_SEQ")
    @GeneratedValue( generator = "CLIENTES_PACOTES_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;
    private int quantidade;

    @Override
    public Integer getId() {

        return id;
    }

    @ManyToOne
    @JoinColumn( name = "ID_CLIENTES" )
    private ClientesEntity clientes;

    @ManyToOne
    @JoinColumn( name = "ID_PACOTES" )
    private PacotesEntity pacotes;

    @OneToMany( mappedBy = "clientesPacotes")
    private List<PagamentosEntity> pagamentos;

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public PacotesEntity getPacotes() {
        return pacotes;
    }

    public void setPacotes(PacotesEntity pacotes) {
        this.pacotes = pacotes;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
