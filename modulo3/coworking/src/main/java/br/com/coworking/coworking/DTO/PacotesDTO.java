package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import br.com.coworking.coworking.Entity.PacotesEntity;

import java.util.List;

public class PacotesDTO extends AbstractDTO<Integer>{

    private Integer id;
    private double valor;
    private List<ClientesPacotesEntity> clientesPacotes;
    private List<EspacosPacotesEntity> espacosPacotes;

    public PacotesDTO (PacotesEntity pacotes){
        this.id = pacotes.getId();
        this.valor = pacotes.getValor();
        this.clientesPacotes = pacotes.getClientesPacotes();
        this.espacosPacotes = pacotes.getEspacosPacotes();
    }

    public PacotesEntity convert(){
        PacotesEntity pacotes = new PacotesEntity();
        pacotes.setId(this.id);
        pacotes.setValor(this.valor);
        pacotes.setClientesPacotes(this.clientesPacotes);
        pacotes.setEspacosPacotes(this.espacosPacotes);
        return pacotes;
    }

    public PacotesDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }

    public List<ClientesPacotesEntity> getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(List<ClientesPacotesEntity> clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public List<EspacosPacotesEntity> getEspacosPacotes() {
        return espacosPacotes;
    }

    public void setEspacosPacotes(List<EspacosPacotesEntity> espacosPacotes) {
        this.espacosPacotes = espacosPacotes;
    }

}
