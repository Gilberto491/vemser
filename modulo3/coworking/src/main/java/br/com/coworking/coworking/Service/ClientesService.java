package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.ClientesDTO;
import br.com.coworking.coworking.Entity.ClientesEntity;
import br.com.coworking.coworking.Entity.ContatoEntity;
import br.com.coworking.coworking.Repository.ClientesRepository;
import br.com.coworking.coworking.Repository.ContatoRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientesService extends AbstractService<ClientesRepository, ClientesEntity, Integer>{

    public List<ClientesDTO> retornarListaClientes(){
        List<ClientesDTO> listaDTO = new ArrayList<>();
        for (ClientesEntity clientes : this.todos()){
            listaDTO.add(new ClientesDTO(clientes));
        }
        return listaDTO;
    }

    public ClientesEntity salvarClientes(ClientesDTO dto){
        ClientesEntity clientes = this.salvar(dto.convert());
        return clientes;
    }

    public ClientesDTO retornarClientesEspecifico(Integer id){
        ClientesDTO clientes = new ClientesDTO(this.porId(id));
        return clientes;
    }

    public ClientesEntity editarClientes(Integer id, ClientesDTO dto){
        ClientesEntity clientes = this.editar(dto.convert(), id);
        return clientes;
    }

    @Autowired
    ContatoRepository contatoRepository;

    public ClientesDTO salvarComContatos(ClientesDTO cliente){
        List<ContatoEntity> contato = new ArrayList<>();
        contato.add(validacaoContato("email"));
        contato.add(validacaoContato("telefone"));

        ClientesEntity clienteFinal = cliente.convert();
        ClientesDTO newDTO = new ClientesDTO(repository.save(clienteFinal));

        return newDTO;
    }

    private ContatoEntity validacaoContato (String nome){
        ContatoEntity contato = contatoRepository.findByValor(nome);
        if(contato == null){
            contato = contatoRepository.save(new ContatoEntity());
        }
        return contato;
    }
}