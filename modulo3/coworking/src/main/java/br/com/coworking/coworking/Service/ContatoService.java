package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.ContatoDTO;
import br.com.coworking.coworking.Entity.ContatoEntity;
import br.com.coworking.coworking.Repository.ContatoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ContatoService extends AbstractService<ContatoRepository, ContatoEntity, Integer>{

    public List<ContatoDTO> retornarListaContato(){
        List<ContatoDTO> listaDTO = new ArrayList<>();
        for (ContatoEntity contato : this.todos()){
            listaDTO.add(new ContatoDTO(contato));
        }
        return listaDTO;
    }

    public ContatoEntity salvarContato(ContatoDTO dto){
        ContatoEntity contato = this.salvar(dto.convert());
        return contato;
    }

    public ContatoDTO retornarContatoEspecifico(Integer id){
        ContatoDTO contato = new ContatoDTO(this.porId(id));
        return contato;
    }

    public ContatoEntity editarContato(Integer id, ContatoDTO dto){
        ContatoEntity contato = this.editar(dto.convert(), id);
        return contato;
    }
}