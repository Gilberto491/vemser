package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.EspacosDTO;
import br.com.coworking.coworking.Entity.EspacosEntity;
import br.com.coworking.coworking.Repository.EspacosRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class EspacosService extends AbstractService<EspacosRepository, EspacosEntity, Integer>{

    public List<EspacosDTO> retornarListaEspacos(){
        List<EspacosDTO> listaDTO = new ArrayList<>();
        for (EspacosEntity espacos : this.todos()){
            listaDTO.add(new EspacosDTO(espacos));
        }
        return listaDTO;
    }

    public EspacosEntity salvarEspacos(EspacosDTO dto){
        EspacosEntity espacos = this.salvar(dto.convert());
        return espacos;
    }

    public EspacosDTO retornarEspacosEspecifico(Integer id){
        EspacosDTO espacos = new EspacosDTO(this.porId(id));
        return espacos;
    }

    public EspacosEntity editarEspacos(Integer id, EspacosDTO dto){
        EspacosEntity espacos = this.editar(dto.convert(), id);
        return espacos;
    }
}