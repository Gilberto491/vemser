package br.com.coworking.coworking.Entity;

import br.com.coworking.coworking.Entity.Enum.TipoPagamento;

import javax.persistence.*;

@Entity
public class PagamentosEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "PAGAMENTOS_SEQ", sequenceName = "PAGAMENTOS_SEQ")
    @GeneratedValue( generator = "PAGAMENTOS_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoPagamento tipoPagamento;


    @ManyToOne
    @JoinColumn( name = "ID_CLIENTES_PACOTES")
    private ClientesPacotesEntity clientesPacotes;

    @ManyToOne
    @JoinColumn( name = "ID_CONTRATACAO")
    private ContratacaoEntity contratacao;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoPagamento getTipoPagamento() {
        return tipoPagamento;
    }

    public void setTipoPagamento(TipoPagamento tipoPagamento) {
        this.tipoPagamento = tipoPagamento;
    }

    public ClientesPacotesEntity getClientesPacotes() {
        return clientesPacotes;
    }

    public void setClientesPacotes(ClientesPacotesEntity clientesPacotes) {
        this.clientesPacotes = clientesPacotes;
    }

    public ContratacaoEntity getContratacao() {
        return contratacao;
    }

    public void setContratacao(ContratacaoEntity contratacao) {
        this.contratacao = contratacao;
    }
}
