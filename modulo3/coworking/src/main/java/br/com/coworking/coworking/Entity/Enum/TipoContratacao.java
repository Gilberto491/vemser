package br.com.coworking.coworking.Entity.Enum;

public enum TipoContratacao {

    MINUTO, HORAS, TURNO, DIARIAS, SEMANAS, MESES
}
