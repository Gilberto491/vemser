package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.UsuariosDTO;
import br.com.coworking.coworking.Entity.UsuariosEntity;
import br.com.coworking.coworking.Repository.UsuariosRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class UsuariosService extends AbstractService<UsuariosRepository, UsuariosEntity, Integer>{

    @Autowired
    private PasswordEncoder passwordEncoder;

    public List<UsuariosDTO> retornarListaUsuario(){
        List<UsuariosDTO> listaDTO = new ArrayList<>();
        for (UsuariosEntity usuario : this.todos()){
            listaDTO.add(new UsuariosDTO(usuario));
        }
        return listaDTO;
    }

    /*
        Salvando usuário e encriptografando
        a senha com passwordEncoder
    */

    public UsuariosEntity salvarComEncriptografiaDeSenha(UsuariosDTO dto){

        try{
            UsuariosDTO usuariosDTO = new UsuariosDTO();

            String valorSenha = usuariosDTO.getSenha();
            usuariosDTO.setSenha(passwordEncoder.encode(valorSenha));

            UsuariosEntity usuarios = this.salvar(dto.convert());
            return usuarios;
        }catch (Exception e){
            throw new RuntimeException();
        }
    }

    public UsuariosDTO retornarUsuarioEspecifico(Integer id){
        UsuariosDTO usuarios = new UsuariosDTO(this.porId(id));
        return usuarios;
    }

    public UsuariosEntity editarUsuarios(Integer id, UsuariosDTO dto){
        UsuariosEntity usuarios = this.editar(dto.convert(), id);
        return usuarios;
    }

    public boolean login(String login, String senha){
        UsuariosEntity usuarios = repository.findByLoginAndSenha(login, senha);
        return usuarios == null ? false : true;
    }

    public boolean login(String login){
        UsuariosEntity usuarios = repository.findByLogin(login);
        return usuarios == null ? false : true;
    }

}