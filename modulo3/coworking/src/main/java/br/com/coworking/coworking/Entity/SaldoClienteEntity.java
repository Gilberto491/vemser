package br.com.coworking.coworking.Entity;

import br.com.coworking.coworking.Entity.Enum.TipoContratacao;
import com.fasterxml.jackson.annotation.JsonFormat;

import javax.persistence.*;
import java.io.Serializable;
import java.util.List;

@Entity
public class SaldoClienteEntity extends AbstractEntity<SaldoClienteId> implements Serializable {

    @EmbeddedId
    private SaldoClienteId id;

    @Enumerated(EnumType.STRING)
    @Column(nullable = false)
    private TipoContratacao tipoContratacao;

    @Column(nullable = false)
    private int quantidade;

    @Column(nullable = false)
    @JsonFormat(shape = JsonFormat.Shape.STRING, pattern = "dd/MM/yyyy")
    private String vencimento;

    @Override
    public SaldoClienteId getId() {
        return id;
    }

    @Override
    public void setId(SaldoClienteId id) {
        this.id = id;
    }

    @ManyToOne
    @JoinColumn( name = "ID_CLIENTES", insertable = false, updatable = false)
    private ClientesEntity clientes;

    @ManyToOne
    @JoinColumn( name = "ID_ESPACOS", insertable = false, updatable = false)
    private EspacosEntity espacos;

    @OneToMany (mappedBy = "saldoCliente")
    private List<AcessosEntity> acessos;


    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public String getVencimento() {
        return vencimento;
    }

    public void setVencimento(String vencimento) {
        this.vencimento = vencimento;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public List<AcessosEntity> getAcessos() {
        return acessos;
    }

    public void setAcessos(List<AcessosEntity> acessos) {
        this.acessos = acessos;
    }
}
