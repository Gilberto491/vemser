package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.AcessosEntity;
import br.com.coworking.coworking.Entity.SaldoClienteEntity;

public class AcessosDTO extends AbstractDTO<Integer> {

    private Integer id;
    private String data;
    private boolean isExcecao;
    private boolean isEntrada;
    private SaldoClienteEntity saldoCliente;

    public AcessosDTO (AcessosEntity acessos){
        this.id = acessos.getId();
        this.data = acessos.getData();
        this.isExcecao = acessos.isIsexcecao();
        this.isEntrada = acessos.isEntrada();
    }

    public AcessosDTO(){}

    public AcessosEntity convert(){
        AcessosEntity acessos = new AcessosEntity();
        acessos.setId(this.id);
        acessos.setData(this.data);
        acessos.setIsexcecao(this.isExcecao);
        acessos.setEntrada(this.isEntrada);
        acessos.setSaldoCliente(this.saldoCliente);

        return acessos;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getData() {
        return data;
    }

    public void setData(String data) {
        this.data = data;
    }

    public boolean isExcecao() {
        return isExcecao;
    }

    public void setExcecao(boolean excecao) {
        isExcecao = excecao;
    }

    public boolean isEntrada() {
        return isEntrada;
    }

    public void setEntrada(boolean entrada) {
        isEntrada = entrada;
    }

    public SaldoClienteEntity getSaldoCliente() {
        return saldoCliente;
    }

    public void setSaldoCliente(SaldoClienteEntity saldoCliente) {
        this.saldoCliente = saldoCliente;
    }
}
