package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.ClientesPacotesDTO;
import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import br.com.coworking.coworking.Service.ClientesPacotesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/clientesPacotes")
public class ClientesPacotesController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    ClientesPacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ClientesPacotesDTO> todosClientesPacotes(){
        logger.info("Listando Clientes Pacotes");
        logger.warn("Clientes Pacotes pode não ter sido criado");
        return service.retornarListaClientesPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ClientesPacotesEntity salvarClientesPacotes(@RequestBody ClientesPacotesDTO dto){
        logger.info("Novo Clientes Pacotes");
        return service.salvarClientesPacotes(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ClientesPacotesDTO clientesPacotesEspecifico(@PathVariable Integer id){
        logger.info("Vendo Clientes Pacotes Especifico");
        logger.warn("Clientes Pacotes pode não ter sido criado");
        return service.retornarClientesPacotesEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ClientesPacotesEntity editarClientesPacotes(@PathVariable Integer id, @RequestBody ClientesPacotesDTO dto){
        logger.info("Editando Clientes Pacotes");
        logger.warn("Clientes Pacotes pode não ter sido criado");
        return service.editarClientesPacotes(id, dto);
    }

}