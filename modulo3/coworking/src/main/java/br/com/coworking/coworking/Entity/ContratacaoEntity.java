package br.com.coworking.coworking.Entity;

import br.com.coworking.coworking.Entity.Enum.TipoContratacao;

import javax.persistence.*;
import java.util.List;

@Entity
public class ContratacaoEntity extends AbstractEntity<Integer>{

    @Id
    @SequenceGenerator(name = "CONTRATACAO_SEQ", sequenceName = "CONTRATACAO_SEQ")
    @GeneratedValue( generator = "CONTRATACAO_SEQ", strategy = GenerationType.SEQUENCE)
    private Integer id;

    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;

    private int quantidade;
    private double desconto;
    private int prazo;

    @ManyToOne
    @JoinColumn( name = "ID_ESPACOS")
    private EspacosEntity espacos;
    
    @ManyToOne
    @JoinColumn( name = "ID_CLIENTES")
    private ClientesEntity clientes;

    @OneToMany( mappedBy = "contratacao")
    private List<PagamentosEntity> pagamentos;

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
