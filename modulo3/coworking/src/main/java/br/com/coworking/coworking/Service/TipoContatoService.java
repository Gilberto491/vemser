package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.TipoContatoDTO;
import br.com.coworking.coworking.Entity.TipoContatoEntity;
import br.com.coworking.coworking.Repository.TipoContatoRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class TipoContatoService extends AbstractService<TipoContatoRepository, TipoContatoEntity, Integer>{

    public List<TipoContatoDTO> retornarListaTipoContato(){
        List<TipoContatoDTO> listaDTO = new ArrayList<>();
        for (TipoContatoEntity tipoContato : this.todos()){
            listaDTO.add(new TipoContatoDTO(tipoContato));
        }
        return listaDTO;
    }

    public TipoContatoEntity salvarTipoContato(TipoContatoDTO dto){
        TipoContatoEntity tipoContato = this.salvar(dto.convert());
        return tipoContato;
    }

    public TipoContatoDTO retornarUsuarioEspecifico(Integer id){
        TipoContatoDTO tipoContato = new TipoContatoDTO(this.porId(id));
        return tipoContato;
    }

    public TipoContatoEntity editarUsuarios(Integer id, TipoContatoDTO dto){
        TipoContatoEntity tipoContato = this.editar(dto.convert(), id);
        return tipoContato;
    }

}