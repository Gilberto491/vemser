package br.com.coworking.coworking.Exception;

public class ExceptionDescontoDoSaldo extends Exception{

    private String mensagem;

    public ExceptionDescontoDoSaldo(String mensagem) {
        super(mensagem);
        this.mensagem = mensagem;
    }

    public String getMensagem() {
        return mensagem;
    }
}
