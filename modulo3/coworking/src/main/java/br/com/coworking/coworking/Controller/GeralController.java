package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.GeralDTO;
import br.com.coworking.coworking.Service.*;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/api/geral")
public class GeralController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    AcessosService acessosService;

    @Autowired
    ClientesPacotesService clientesPacotesService;

    @Autowired
    ClientesService clientesService;

    @Autowired
    ContatoService contatoService;

    @Autowired
    ContratacaoService contratacaoService;

    @Autowired
    EspacosPacotesService espacosPacotesService;

    @Autowired
    EspacosService espacosService;

    @Autowired
    PacotesService pacotesService;

    @Autowired
    PagamentosService pagamentosService;

    @Autowired
    SaldoClienteService saldoClienteService;

    @Autowired
    TipoContatoService tipoContatoService;

    @Autowired
    UsuariosService usuariosService;

    /*
        Caso seja necessário implementar
        um relátorio/salvarTudo
    */

    @PostMapping(value = "salvaTudo")
    @ResponseBody
    public GeralDTO salvaTudo(@RequestBody GeralDTO geral){

        logger.info("Salvando Tudo");
        GeralDTO newGeral = new GeralDTO();
        newGeral.setAcessos(acessosService.salvar(geral.getAcessos()));
        newGeral.setClientes(clientesService.salvar(geral.getClientes()));
        newGeral.setClientesPacotes(clientesPacotesService.salvar(geral.getClientesPacotes()));
        newGeral.setContatos(contatoService.salvar(geral.getContatos()));
        newGeral.setContatacao(contratacaoService.salvar(geral.getContatacao()));
        newGeral.setEspacosPacotes(espacosPacotesService.salvar(geral.getEspacosPacotes()));
        newGeral.setEspacos(espacosService.salvar(geral.getEspacos()));
        newGeral.setPacotes(pacotesService.salvar(geral.getPacotes()));
        newGeral.setPagamentos(pagamentosService.salvar(geral.getPagamentos()));
        newGeral.setSaldoCliente(saldoClienteService.salvar(geral.getSaldoCliente()));
        newGeral.setTipoContato(tipoContatoService.salvar(geral.getTipoContato()));
        newGeral.setUsuarios(usuariosService.salvar(geral.getUsuarios()));

        return newGeral;
    }

    @PostMapping(value = "/relatorio")
    @ResponseBody
    public GeralDTO relatorio(@RequestBody GeralDTO geral){
        logger.info("Listando Tudo");
        logger.warn("Pode ser que alguma Entidade não tenha sido criada");
        GeralDTO newGeral = new GeralDTO();
        newGeral.setAcessos(acessosService.porId(geral.getAcessos().getId()));
        newGeral.setClientes(clientesService.porId(geral.getClientes().getId()));
        newGeral.setClientesPacotes(clientesPacotesService.porId(geral.getClientesPacotes().getId()));
        newGeral.setContatos(contatoService.porId(geral.getContatos().getId()));
        newGeral.setContatacao(contratacaoService.porId(geral.getContatacao().getId()));
        newGeral.setEspacosPacotes(espacosPacotesService.porId(geral.getEspacosPacotes().getId()));
        newGeral.setEspacos(espacosService.porId(geral.getEspacos().getId()));
        newGeral.setPacotes(pacotesService.porId(geral.getPacotes().getId()));
        newGeral.setPagamentos(pagamentosService.porId(geral.getPagamentos().getId()));
        newGeral.setSaldoCliente(saldoClienteService.porId(geral.getSaldoCliente().getId()));
        newGeral.setTipoContato(tipoContatoService.porId(geral.getTipoContato().getId()));
        newGeral.setUsuarios(usuariosService.porId(geral.getUsuarios().getId()));

        return newGeral;
    }
}
