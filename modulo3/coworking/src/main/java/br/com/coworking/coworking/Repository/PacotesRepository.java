package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.PacotesEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface PacotesRepository extends CrudRepository<PacotesEntity, Integer> {

    PacotesEntity findByValor(double valor);
}