package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.SaldoClienteDTO;
import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import br.com.coworking.coworking.Entity.SaldoClienteId;
import br.com.coworking.coworking.Repository.SaldoClienteRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class SaldoClienteService extends AbstractService<SaldoClienteRepository, SaldoClienteEntity, SaldoClienteId>{

    public List<SaldoClienteDTO> retornarListaSaldoCliente(){
        List<SaldoClienteDTO> listaDTO = new ArrayList<>();
        for (SaldoClienteEntity saldoCliente : this.todos()){
            listaDTO.add(new SaldoClienteDTO(saldoCliente));
        }
        return listaDTO;
    }

    public SaldoClienteEntity salvarSaldoCliente(SaldoClienteDTO dto){
        SaldoClienteEntity saldoCliente = this.salvar(dto.convert());
        return saldoCliente;
    }

    public SaldoClienteDTO retornarSaldoClienteEspecifico(SaldoClienteId id){
        SaldoClienteDTO saldoCliente = new SaldoClienteDTO(this.porId(id));
        return saldoCliente;
    }

    public SaldoClienteEntity editarSaldoCliente(SaldoClienteId id, SaldoClienteDTO dto){
        SaldoClienteEntity saldoCliente = this.editar(dto.convert(), id);
        return saldoCliente;
    }
}