package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.UsuariosEntity;

public class UsuariosDTO extends AbstractDTO<Integer>{

    private Integer id;
    private String nome;
    private String senha;
    private String email;
    private String login;

    public UsuariosDTO (UsuariosEntity usuarios){
        this.id = usuarios.getId();
        this.nome = usuarios.getNome();
        this.senha = usuarios.getSenha();
        this.email = usuarios.getEmail();
        this.login = usuarios.getLogin();

    }

    public UsuariosEntity convert(){
        UsuariosEntity usuarios = new UsuariosEntity();
        usuarios.setId(this.id);
        usuarios.setNome(this.nome);
        usuarios.setSenha(this.senha);
        usuarios.setEmail(this.email);
        usuarios.setLogin(this.login);

        return usuarios;
    }

    public UsuariosDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public String getNome() {
        return nome;
    }

    public void setNome(String nome) {
        this.nome = nome;
    }

    public String getSenha() {
        return senha;
    }

    public void setSenha(String senha) {
        this.senha = senha;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getLogin() {
        return login;
    }

    public void setLogin(String login) {
        this.login = login;
    }
}
