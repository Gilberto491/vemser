package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.EspacosPacotesDTO;
import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import br.com.coworking.coworking.Service.EspacosPacotesService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/espacosPacotes")
public class EspacosPacotesController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    EspacosPacotesService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<EspacosPacotesDTO> todosEspacosPacotes(){
        logger.info("Listando Espaços Pacotes");
        logger.warn("Espaços Pacotes pode não ter sido criado");
        return service.retornarEspacosPacotes();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public EspacosPacotesEntity salvarEspacosPacotes(@RequestBody EspacosPacotesDTO dto){
        logger.info("Novo Espaços Pacotes");
        return service.salvarEspacosPacotes(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public EspacosPacotesDTO espacosPacotesEspecifico(@PathVariable Integer id){
        logger.info("Vendo Espaços Pacotes Especifico");
        logger.warn("Espaços Pacotes pode não ter sido criado");
        return service.retornarEspacosPacotesEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public EspacosPacotesEntity editarEspacosPacotes(@PathVariable Integer id, @RequestBody EspacosPacotesDTO dto){
        logger.info("Editando Espaços Pacotes");
        logger.warn("Espaços Pacotes pode não ter sido criado");
        return service.editarEspacosPacotes(id,dto);
    }
}