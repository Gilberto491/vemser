package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.ContatoDTO;
import br.com.coworking.coworking.Entity.ContatoEntity;
import br.com.coworking.coworking.Service.ContatoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contato")
public class ContatoController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    ContatoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ContatoDTO> todosContato(){
        logger.info("Listando Contatos");
        logger.warn("Contatos pode não ter sido criado");
        return service.retornarListaContato();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ContatoEntity salvarContato(@RequestBody ContatoDTO dto){
        logger.info("Novo Contato");
        return service.salvarContato(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContatoDTO contatoEspecifico(@PathVariable Integer id){
        logger.info("Vendo Contato Especifico");
        logger.warn("Contatos pode não ter sido criado");
        return service.retornarContatoEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ContatoEntity editarContato(@PathVariable Integer id, @RequestBody ContatoDTO dto){
        logger.info("Editando Contato");
        logger.warn("Contatos pode não ter sido criado");
        return service.editarContato(id, dto);
    }
}