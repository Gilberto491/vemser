package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.PagamentosDTO;
import br.com.coworking.coworking.Entity.PagamentosEntity;
import br.com.coworking.coworking.Service.PagamentosService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/pagamentos")
public class PagamentosController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    PagamentosService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<PagamentosDTO> todosPagamentos(){
        logger.info("Listando Pagamentos");
        logger.warn("Pagamentos pode não ter sido criado");
        return service.retornarListaPagamentos();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public PagamentosEntity salvarPagamentos(@RequestBody PagamentosDTO dto){
        logger.info("Novo Pagamento");
        return service.salvarPagamentos(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public PagamentosDTO pagamentosEspecifico(@PathVariable Integer id){
        logger.info("Vendo Pagamento Especifico");
        logger.warn("Pagamentos pode não ter sido criado");
        return service.retornarPagamentosEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public PagamentosEntity editarPagamentos(@PathVariable Integer id, @RequestBody PagamentosDTO dto){
        logger.info("Editando Pagamento");
        logger.warn("Pagamentos pode não ter sido criado");
        return service.editarPagamentos(id, dto);
    }
}