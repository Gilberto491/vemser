package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.UsuariosEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UsuariosRepository extends CrudRepository<UsuariosEntity, Integer> {

    UsuariosEntity findByLoginAndSenha (String login, String senha);
    UsuariosEntity findByLogin (String login);
    UsuariosEntity findByEmail (String email);
    UsuariosEntity findBySenha (String senha);
    UsuariosEntity findByNome (String nome);
}