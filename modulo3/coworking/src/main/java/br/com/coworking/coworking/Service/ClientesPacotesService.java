package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.ClientesPacotesDTO;
import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import br.com.coworking.coworking.Repository.ClientesPacotesRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class ClientesPacotesService extends AbstractService<ClientesPacotesRepository, ClientesPacotesEntity, Integer>{

    public List<ClientesPacotesDTO> retornarListaClientesPacotes(){
        List<ClientesPacotesDTO> listaDTO = new ArrayList<>();
        for (ClientesPacotesEntity clientesPacotes : this.todos()){
            listaDTO.add(new ClientesPacotesDTO(clientesPacotes));
        }
        return listaDTO;
    }

    public ClientesPacotesEntity salvarClientesPacotes(ClientesPacotesDTO dto){
        ClientesPacotesEntity clientesPacotes = this.salvar(dto.convert());
        return clientesPacotes;
    }

    public ClientesPacotesDTO retornarClientesPacotesEspecifico(Integer id){
        ClientesPacotesDTO clientesPacotes = new ClientesPacotesDTO(this.porId(id));
        return clientesPacotes;
    }

    public ClientesPacotesEntity editarClientesPacotes(Integer id, ClientesPacotesDTO dto){
        ClientesPacotesEntity clientesPacotes = this.editar(dto.convert(), id);
        return clientesPacotes;
    }
}