package br.com.coworking.coworking.DTO;

public abstract class AbstractDTO<T> {

    public abstract T getId();

    public abstract void setId(T id);
}
