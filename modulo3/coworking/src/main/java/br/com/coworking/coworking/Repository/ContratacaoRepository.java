package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ContratacaoEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ContratacaoRepository extends CrudRepository<ContratacaoEntity, Integer> {

    ContratacaoEntity findByPrazo(int prazo);
    ContratacaoEntity findByDesconto(int desconto);
    ContratacaoEntity findByQuantidade(int quantidade);

}