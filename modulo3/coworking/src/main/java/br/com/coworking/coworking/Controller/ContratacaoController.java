package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.CoworkingApplication;
import br.com.coworking.coworking.DTO.ContratacaoDTO;
import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Service.ContratacaoService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/api/contratacao")
public class ContratacaoController {

    private Logger logger = LoggerFactory.getLogger(CoworkingApplication.class);

    @Autowired
    ContratacaoService service;

    @GetMapping( value = "/todos")
    @ResponseBody
    public List<ContratacaoDTO> todasContratacao(){
        logger.info("Listando Contratações");
        logger.warn("Contratação pode não ter sido criado");
        return service.retornarListaContratacao();
    }

    @PostMapping( value = "/novo")
    @ResponseBody
    public ContratacaoEntity salvarContratacao(@RequestBody ContratacaoDTO dto){
        logger.info("Nova Contratação");
        return service.salvarContratacao(dto);
    }

    @GetMapping(value = "/ver/{id}")
    @ResponseBody
    public ContratacaoDTO contratacaoEspecifico(@PathVariable Integer id){
        logger.info("Vendo Contratação Especifica");
        logger.warn("Contratação pode não ter sido criado");
        return service.retornarContracaoEspecifico(id);
    }

    @PutMapping( value = "/editar/{id}")
    @ResponseBody
    public ContratacaoEntity editarContratacao(@PathVariable Integer id, @RequestBody ContratacaoDTO dto){
        logger.info("Editanto Contratação");
        logger.warn("Contratação pode não ter sido criado");
        return service.editarContratacao(id, dto);
    }
}