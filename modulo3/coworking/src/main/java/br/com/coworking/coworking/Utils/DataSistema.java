package br.com.coworking.coworking.Utils;

import java.text.SimpleDateFormat;

public class DataSistema {

    /*
        Método para pegar a data do sistema
        e retornar uma String já que as datas
        das entidades estão como String
    */

    public String dataSistema(){
        SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy hh:mm");
        String retorno = sdf.toString();
        return retorno;
    }
}
