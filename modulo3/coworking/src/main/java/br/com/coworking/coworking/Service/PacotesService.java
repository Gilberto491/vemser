package br.com.coworking.coworking.Service;

import br.com.coworking.coworking.DTO.PacotesDTO;
import br.com.coworking.coworking.Entity.PacotesEntity;
import br.com.coworking.coworking.Repository.PacotesRepository;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

@Service
public class PacotesService extends AbstractService<PacotesRepository, PacotesEntity, Integer>{

    public List<PacotesDTO> retornarPacotes(){
        List<PacotesDTO> listaDTO = new ArrayList<>();
        for (PacotesEntity pacotes : this.todos()){
            listaDTO.add(new PacotesDTO(pacotes));
        }
        return listaDTO;
    }

    public PacotesEntity salvarPacotes(PacotesDTO dto){
        PacotesEntity pacotes = this.salvar(dto.convert());
        return pacotes;
    }

    public PacotesDTO retornarPacotesEspecifico(Integer id){
        PacotesDTO pacotes = new PacotesDTO(this.porId(id));
        return pacotes;
    }

    public PacotesEntity editarPacotes(Integer id, PacotesDTO dto){
        PacotesEntity pacotes = this.editar(dto.convert(), id);
        return pacotes;
    }
}