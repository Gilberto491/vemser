package br.com.coworking.coworking.DTO;

import br.com.coworking.coworking.Entity.ClientesEntity;
import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Entity.Enum.TipoContratacao;
import br.com.coworking.coworking.Entity.EspacosEntity;
import br.com.coworking.coworking.Entity.PagamentosEntity;

import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import java.util.List;

public class ContratacaoDTO extends AbstractDTO<Integer>{

    private Integer id;
    @Enumerated(EnumType.STRING)
    private TipoContratacao tipoContratacao;
    private int quantidade;
    private double desconto;
    private int prazo;
    private EspacosEntity espacos;
    private ClientesEntity clientes;
    private List<PagamentosEntity> pagamentos;

    public ContratacaoDTO (ContratacaoEntity contratacao){
        this.id = contratacao.getId();
        this.tipoContratacao = contratacao.getTipoContratacao();
        this.quantidade = contratacao.getQuantidade();
        this.desconto = contratacao.getDesconto();
        this.prazo = contratacao.getPrazo();
        this.espacos = contratacao.getEspacos();
        this.clientes = contratacao.getClientes();
        this.pagamentos = contratacao.getPagamentos();
    }

    public ContratacaoEntity convert(){
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setId(this.id);
        contratacao.setTipoContratacao(this.tipoContratacao);
        contratacao.setQuantidade(this.quantidade);
        contratacao.setDesconto(this.desconto);
        contratacao.setPrazo(this.prazo);
        contratacao.setEspacos(this.espacos);
        contratacao.setClientes(this.clientes);
        contratacao.setPagamentos(this.pagamentos);

        return contratacao;
    }

    public ContratacaoDTO(){}

    @Override
    public Integer getId() {
        return id;
    }

    @Override
    public void setId(Integer id) {
        this.id = id;
    }

    public TipoContratacao getTipoContratacao() {
        return tipoContratacao;
    }

    public void setTipoContratacao(TipoContratacao tipoContratacao) {
        this.tipoContratacao = tipoContratacao;
    }

    public int getQuantidade() {
        return quantidade;
    }

    public void setQuantidade(int quantidade) {
        this.quantidade = quantidade;
    }

    public double getDesconto() {
        return desconto;
    }

    public void setDesconto(double desconto) {
        this.desconto = desconto;
    }

    public int getPrazo() {
        return prazo;
    }

    public void setPrazo(int prazo) {
        this.prazo = prazo;
    }

    public EspacosEntity getEspacos() {
        return espacos;
    }

    public void setEspacos(EspacosEntity espacos) {
        this.espacos = espacos;
    }

    public ClientesEntity getClientes() {
        return clientes;
    }

    public void setClientes(ClientesEntity clientes) {
        this.clientes = clientes;
    }

    public List<PagamentosEntity> getPagamentos() {
        return pagamentos;
    }

    public void setPagamentos(List<PagamentosEntity> pagamentos) {
        this.pagamentos = pagamentos;
    }
}
