package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.AcessosEntity;
import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface AcessosRepository extends CrudRepository<AcessosEntity, Integer> {

    AcessosEntity findByData(String data);
}