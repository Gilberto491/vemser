package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.ContratacaoEntity;
import br.com.coworking.coworking.Repository.ContratacaoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ContratacaoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContratacaoRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoContratacao() throws Exception {
        URI uri = new URI("/api/contratacao/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmaContratacao() throws Exception {
        URI uri = new URI("/api/contratacao/novo");
        String json = "{\"quantidade\": \"13\", \"desconto\": \"15\", \"prazo\": \"2\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers

                .jsonPath("$.quantidade").value(13)
        ).andExpect(MockMvcResultMatchers

                .jsonPath("$.desconto").value(15)
        );
    }

    @Test
    public void deveRetornarUmaContratacao() throws Exception {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setQuantidade(13);
        contratacao.setDesconto(15);
        contratacao.setPrazo(2);

        ContratacaoEntity newContratacao = repository.save(contratacao);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contratacao/ver/{id}", newContratacao.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(13)
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.desconto")
                .value(15)
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.prazo")
                .value(2)
        );
    }

    @Test
    public void deveRetornarUmContratacaoEditada() throws Exception {
        ContratacaoEntity contratacao = new ContratacaoEntity();
        ContratacaoEntity newContratacao = repository.save(contratacao);

        String json = "{\"quantidade\": \"14\", \"desconto\": \"16.4\", \"prazo\": \"3\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/contratacao/editar/{id}", newContratacao.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value("14")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.desconto")
                .value("16.4")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.prazo")
                .value("3")
        );
    }
}
