package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.Enum.TipoContratacao;
import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import br.com.coworking.coworking.Repository.EspacosPacotesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class EspacosPacotesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacosPacotesRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoEspacosPacotes() throws Exception {
        URI uri = new URI("/api/espacosPacotes/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmEspacoPacote() throws Exception {
        URI uri = new URI("/api/espacosPacotes/novo");
        String json = "{\"quantidade\": \"13\", \"prazo\": \"15\", \"tipoContratacao\": \"HORAS\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value(13)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.prazo").value(15)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("HORAS")
        );
    }

    @Test
    public void deveRetornarUmEspacoPacote() throws Exception {
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        espacosPacotes.setQuantidade(13);
        espacosPacotes.setPrazo(15);
        espacosPacotes.setTipoContratacao(TipoContratacao.HORAS);

        EspacosPacotesEntity newEspacosPacotes = repository.save(espacosPacotes);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacosPacotes/ver/{id}", newEspacosPacotes.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(13)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.prazo")
                .value(15)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("HORAS")
        );
    }

    @Test
    public void deveRetornarUmEspacoPacoteEditado() throws Exception {
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        EspacosPacotesEntity newEspacosPacotes = repository.save(espacosPacotes);

        String json = "{\"quantidade\": \"14\", \"prazo\": \"16\", \"tipoContratacao\": \"MESES\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/espacosPacotes/editar/{id}", newEspacosPacotes.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value(14)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.prazo")
                .value(16)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoContratacao")
                .value("MESES")
        );
    }
}
