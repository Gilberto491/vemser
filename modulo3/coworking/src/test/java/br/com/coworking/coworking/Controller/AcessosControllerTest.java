package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.*;
import br.com.coworking.coworking.Repository.*;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class AcessosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private AcessosRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoAcessos() throws Exception {
        URI uri = new URI("/api/acessos/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmAcesso() throws Exception {

        URI uri = new URI("/api/acessos/novo");
        String json = "{\"data\": \"25/05/2000\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.data")
                .value("25/05/2000")
        );
    }

    @Test
    public void deveRetornarUmAcesso() throws Exception {
        AcessosEntity acessos = new AcessosEntity();
        acessos.setData("25/04/2000");

        AcessosEntity newAcessos = repository.save(acessos);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/acessos/ver/{id}", newAcessos.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.data")
                .value("25/04/2000")
        );
    }

    @Test
    public void deveRetornarUmAcessoEditado() throws Exception {
        AcessosEntity acessos = new AcessosEntity();
        AcessosEntity newAcessos = repository.save(acessos);
        String json = "{\"data\": \"25/05/1900\"}";

        mockMvc
           .perform(
                   MockMvcRequestBuilders
                         .put("/api/acessos/editar/{id}", newAcessos.getId())
                         .content(json)
                         .contentType(MediaType.APPLICATION_JSON)
           ).andExpect(MockMvcResultMatchers
                .jsonPath("$.data")
                .value("25/05/1900")
           );
    }
}
