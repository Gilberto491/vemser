package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ContratacaoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ContratacaoRepositoryTest {

    @Autowired
    private ContratacaoRepository repository;

    @Test
    public void salvarPrazoAndDescontoAndQuantidade(){
        ContratacaoEntity contratacao = new ContratacaoEntity();
        contratacao.setPrazo(2);
        contratacao.setDesconto(13);
        contratacao.setQuantidade(100);
        repository.save(contratacao);
        assertEquals(contratacao.getPrazo(), repository.findByPrazo(2).getPrazo());
        assertEquals(contratacao.getDesconto(), repository.findByDesconto(13).getDesconto());
        assertEquals(contratacao.getQuantidade(), repository.findByQuantidade(100).getQuantidade());
    }

    @Test
    public void buscarPrazoAndDescontoAndQuantidade(){
        int prazo = 2;
        int desconto = 13;
        int quantidade = 100;
        assertNull(repository.findByPrazo(2));
        assertNull(repository.findByDesconto(13));
        assertNull(repository.findByQuantidade(100));
    }
}
