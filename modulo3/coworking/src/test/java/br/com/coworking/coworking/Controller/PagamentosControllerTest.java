package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.Enum.TipoPagamento;
import br.com.coworking.coworking.Entity.PagamentosEntity;
import br.com.coworking.coworking.Repository.PagamentosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class PagamentosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PagamentosRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoPagamentos() throws Exception {
        URI uri = new URI("/api/pagamentos/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmPagamento() throws Exception {
        URI uri = new URI("/api/pagamentos/novo");
        String json = "{\"tipoPagamento\": \"DEBITO\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoPagamento")
                .value("DEBITO")
        );
    }

    @Test
    public void deveRetornarUmPagamento() throws Exception {
        PagamentosEntity pagamentos = new PagamentosEntity();
        pagamentos.setTipoPagamento(TipoPagamento.DEBITO);

        PagamentosEntity newPagamentos = repository.save(pagamentos);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pagamentos/ver/{id}", newPagamentos.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoPagamento")
                .value("DEBITO")
        );
    }

    @Test
    public void deveRetornarUmPagamentoEditado() throws Exception {
        PagamentosEntity pagamentos = new PagamentosEntity();
        PagamentosEntity newPagamentos = repository.save(pagamentos);

        String json = "{\"tipoPagamento\": \"CREDITO\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/pagamentos/editar/{id}", newPagamentos.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.tipoPagamento")
                .value("CREDITO")
        );
    }
}
