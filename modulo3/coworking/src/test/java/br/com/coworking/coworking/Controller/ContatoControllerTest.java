package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.ContatoEntity;
import br.com.coworking.coworking.Repository.ContatoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ContatoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ContatoRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoContato() throws Exception {
        URI uri = new URI("/api/contato/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmContato() throws Exception {
        URI uri = new URI("/api/contato/novo");
        String json = "{\"valor\": \"telefone\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor").value("telefone")
        );
    }

    @Test
    public void deveRetornarUmContato() throws Exception {
        ContatoEntity contatos = new ContatoEntity();
        contatos.setValor("telefone");

        ContatoEntity newContatos = repository.save(contatos);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/contato/ver/{id}", newContatos.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("telefone")
        );
    }

    @Test
    public void deveRetornarUmContatoEditado() throws Exception {
        ContatoEntity contatos = new ContatoEntity();
        ContatoEntity newContatos = repository.save(contatos);

        String json = "{\"valor\": \"celular\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/contato/editar/{id}", newContatos.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("celular")
        );
    }
}
