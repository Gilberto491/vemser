package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.AcessosEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.*;

@DataJpaTest
public class AcessosRepositoryTest {

    @Autowired
    private AcessosRepository repository;

    @Test
    public void salvarData(){
        AcessosEntity acessos = new AcessosEntity();
        acessos.setData("25/04/2020");
        repository.save(acessos);
        assertEquals(acessos.getData(), repository.findByData("25/04/2020").getData());
    }

    @Test
    public void buscarData(){
        String data = "25/04/2020";
        assertNull(repository.findByData(data));
    }

}
