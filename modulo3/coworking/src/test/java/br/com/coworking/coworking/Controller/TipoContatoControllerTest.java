package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.TipoContatoEntity;
import br.com.coworking.coworking.Repository.TipoContatoRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class TipoContatoControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoTipoContato() throws Exception {
        URI uri = new URI("/api/tipoContato/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmTipoContato() throws Exception {
        URI uri = new URI("/api/tipoContato/novo");
        String json = "{\"nome\": \"Teste\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Teste")

        );
    }

    @Test
    public void deveRetornarUmTipoContato() throws Exception {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("Teste");

        TipoContatoEntity newTipoContato = repository.save(tipoContato);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/tipoContato/ver/{id}", newTipoContato.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Teste")
        );
    }

    @Test
    public void deveRetornarUmTipoContatoEditado() throws Exception {
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        TipoContatoEntity newTipoContato = repository.save(tipoContato);

        String json = "{\"nome\": \"Teste1\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/tipoContato/editar/{id}", newTipoContato.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Teste1")
        );
    }
}
