package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.UsuariosEntity;
import br.com.coworking.coworking.Repository.UsuariosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class UsuariosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    UsuariosRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoUsuarios() throws Exception {
        URI uri = new URI("/api/usuarios/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmUsuario() throws Exception {
        URI uri = new URI("/api/usuarios/novo");
        String json = "{\"nome\": \"junior\", \"senha\": \"Teste123\", \"email\": \"Teste@gmail.com\", \"login\": \"Teste\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("junior")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.senha")
                .value("Teste123")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.email")
                .value("Teste@gmail.com")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.login")
                .value("Teste")
        );
    }

    @Test
    public void deveRetornarUmUsuario() throws Exception {
        UsuariosEntity usuarios = new UsuariosEntity();
        usuarios.setNome("junior");
        usuarios.setSenha("Teste123");
        usuarios.setEmail("Teste2@gmail.com");
        usuarios.setLogin("Teste2");

        UsuariosEntity newUsuarios = repository.save(usuarios);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/usuarios/ver/{id}", newUsuarios.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("junior")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.senha")
                .value("Teste123")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.email")
                .value("Teste2@gmail.com")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.login")
                .value("Teste2")
        );

    }

    @Test
    public void deveRetornarUmUsuarioEditado() throws Exception {
        UsuariosEntity usuarios = new UsuariosEntity();
        UsuariosEntity newUsuario = repository.save(usuarios);

        String json = "{\"nome\": \"junior2\", \"senha\": \"Teste1232\", \"email\": \"Teste3@gmail.com\", \"login\": \"Teste3\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/usuarios/editar/{id}", newUsuario.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("junior2")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.senha")
                .value("Teste1232")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.email")
                .value("Teste3@gmail.com")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.login")
                .value("Teste3")
        );
    }

   /* @Test
    public void deveLogarNaAplicacao() throws Exception {
        URI uri = new URI("/api/usuarios/login");
        String json = "{\"login\": \"TesteA\", \"senha\": \"Teste123\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.login")
                .value("TesteA")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.senha")
                .value("Teste123")
        );
    }*/
}
