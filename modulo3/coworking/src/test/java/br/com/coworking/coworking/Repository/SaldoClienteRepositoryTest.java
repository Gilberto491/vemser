package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class SaldoClienteRepositoryTest {

    @Autowired
    private SaldoClienteRepository repository;

    @Test
    public void salvarQuantidadeAndVencimento(){
        SaldoClienteEntity saldoCliente = new SaldoClienteEntity();
        saldoCliente.setQuantidade(3);
        saldoCliente.setVencimento("30/10/2020");

        repository.save(saldoCliente);
        assertEquals(saldoCliente.getQuantidade(), repository.findByQuantidade(3).getQuantidade());
        assertEquals(saldoCliente.getVencimento(), repository.findByVencimento("30/10/2020").getVencimento());
    }

    @Test
    public void buscarQuantidade(){
        int quantidade = 3;
        String vencimento = "30/10/2020";
        assertNull(repository.findByQuantidade(quantidade));
        assertNull(repository.findByVencimento(vencimento));
    }

}
