package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.UsuariosEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class UsuariosRepositoryTest {

    @Autowired
    private UsuariosRepository repository;

    @Test
    public void salvarLoginAndEmailAndSenhaAndNome(){
        UsuariosEntity usuarios = new UsuariosEntity();
        usuarios.setLogin("junior");
        usuarios.setEmail("juniorfredes8@gmail.com");
        usuarios.setSenha("teste123");
        usuarios.setNome("gilberto");
        repository.save(usuarios);
        assertEquals(usuarios.getLogin(), repository.findByLogin("junior").getLogin());
        assertEquals(usuarios.getEmail(), repository.findByEmail("juniorfredes8@gmail.com").getEmail());
        assertEquals(usuarios.getSenha(), repository.findBySenha("teste123").getSenha());
        assertEquals(usuarios.getNome(),  repository.findByNome("gilberto").getNome());
    }

    @Test
    public void buscarLoginAndEmailAndSenhaAndNome(){
        String login = "junior";
        String email = "juniorfredes8@gmail.com";
        String senha = "teste123";
        String nome = "gilberto";
        assertNull(repository.findByLogin("junior"));
        assertNull(repository.findByEmail("juniorfredes8@gmail.com"));
        assertNull(repository.findBySenha("teste123"));
        assertNull(repository.findByNome("gilberto"));
    }

}
