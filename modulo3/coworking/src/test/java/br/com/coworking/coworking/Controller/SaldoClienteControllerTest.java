package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.SaldoClienteEntity;
import br.com.coworking.coworking.Entity.SaldoClienteId;
import br.com.coworking.coworking.Repository.SaldoClienteRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class SaldoClienteControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private SaldoClienteRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoSaldoCliente() throws Exception {
        URI uri = new URI("/api/saldoCliente/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }
}
