package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ClientesEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;


import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ClientesRepositoryTest {

    @Autowired
    private ClientesRepository repository;


    @Test
    public void salvarCpfAndNomeAndDataNascimento(){
        ClientesEntity clientes = new ClientesEntity();
        clientes.setDataNascimento("25/03/2000");
        clientes.setNome("junior");
        clientes.setCpf("50095743622");
        repository.save(clientes);
        assertEquals(clientes.getDataNascimento(), repository.findByDataNascimento("25/03/2000").getDataNascimento());
        assertEquals(clientes.getNome(), repository.findByNome("junior").getNome());
        assertEquals(clientes.getCpf(), repository.findByCpf("50095743622").getCpf());
    }

    @Test
    public void buscarCpfAndNomeAndDataNascimento(){
        String data = "25/04/2020";
        String nome = "junior";
        String cpf = "50095743622";
        assertNull(repository.findByDataNascimento(data));
        assertNull(repository.findByDataNascimento(nome));
        assertNull(repository.findByDataNascimento(cpf));
    }
}
