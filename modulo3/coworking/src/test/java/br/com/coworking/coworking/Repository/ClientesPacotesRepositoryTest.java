package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ClientesPacotesRepositoryTest {

    @Autowired
    private ClientesPacotesRepository repository;

    @Test
    public void salvarQuantidade(){
        ClientesPacotesEntity clientesPacotes = new ClientesPacotesEntity();
        clientesPacotes.setQuantidade(12);
        repository.save(clientesPacotes);
        assertEquals(clientesPacotes.getQuantidade(), repository.findByQuantidade(12).getQuantidade());
    }

    @Test
    public void buscarQuantidade(){
        int quantidade = 12;
        assertNull(repository.findByQuantidade(12));
    }
}
