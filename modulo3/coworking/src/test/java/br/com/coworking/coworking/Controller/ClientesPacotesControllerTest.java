package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.ClientesPacotesEntity;
import br.com.coworking.coworking.Repository.ClientesPacotesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ClientesPacotesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClientesPacotesRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoAClientesPacotes() throws Exception {
        URI uri = new URI("/api/clientesPacotes/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmClientePacote() throws Exception {
        URI uri = new URI("/api/clientesPacotes/novo");
        String json = "{\"quantidade\": \"12\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade").value("12")
        );
    }

    @Test
    public void deveRetornarUmClientePacote() throws Exception {
        ClientesPacotesEntity clientesPacotes= new ClientesPacotesEntity();
        clientesPacotes.setQuantidade(12);

        ClientesPacotesEntity newClientes = repository.save(clientesPacotes);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/clientesPacotes/ver/{id}", newClientes.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value("12")
        );
    }

    @Test
    public void deveRetornarUmClientePacoteEditado() throws Exception {
        ClientesPacotesEntity clientesPacotes = new ClientesPacotesEntity();
        ClientesPacotesEntity newClientesPacotes = repository.save(clientesPacotes);

        String json = "{\"quantidade\": \"13\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/clientesPacotes/editar/{id}", newClientesPacotes.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.quantidade")
                .value("13")
        );
    }
}
