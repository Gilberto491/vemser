package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.PacotesEntity;
import br.com.coworking.coworking.Repository.PacotesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class PacotesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private PacotesRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoPacotes() throws Exception {
        URI uri = new URI("/api/pacotes/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );

    }

    @Test
    public void salvarERetornarUmPacote() throws Exception {
        URI uri = new URI("/api/pacotes/novo");
        String json = "{\"valor\": \"600.897\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("600.897")
        );
    }

    @Test
    public void deveRetornarUmPacote() throws Exception {
        PacotesEntity pacotes = new PacotesEntity();
        pacotes.setValor(600.897);

        PacotesEntity newPacotes = repository.save(pacotes);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/pacotes/ver/{id}", newPacotes.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("600.897")
        );
    }

    @Test
    public void deveRetornarUmPacoteEditado() throws Exception {
        PacotesEntity pacotes = new PacotesEntity();
        PacotesEntity newPacotes = repository.save(pacotes);

        String json = "{\"valor\": \"601.000\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/pacotes/editar/{id}", newPacotes.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value(601.000)
        );
    }
}
