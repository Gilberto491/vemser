package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.EspacosPacotesEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class EspacosPacotesRepositoryTest {

    @Autowired
    private EspacosPacotesRepository repository;

    @Test
    public void salvarPrazoAndQuantidade(){
        EspacosPacotesEntity espacosPacotes = new EspacosPacotesEntity();
        espacosPacotes.setPrazo(13);
        espacosPacotes.setQuantidade(100);
        repository.save(espacosPacotes);
        assertEquals(espacosPacotes.getPrazo(), repository.findByPrazo(13).getPrazo());
        assertEquals(espacosPacotes.getQuantidade(), repository.findByQuantidade(100).getQuantidade());
    }

    @Test
    public void buscarPrazoAndQuantidade(){
        int prazo = 10;
        int quantidade = 100;

        assertNull(repository.findByPrazo(13));
        assertNull(repository.findByQuantidade(100));
    }
}
