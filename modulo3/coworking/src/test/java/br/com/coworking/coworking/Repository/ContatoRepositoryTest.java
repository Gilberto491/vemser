package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.ContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class ContatoRepositoryTest {

    @Autowired
    private ContatoRepository repository;

    @Test
    public void salvarContato(){
        ContatoEntity contato = new ContatoEntity();
        contato.setValor("telefone");
        repository.save(contato);
        assertEquals(contato.getValor(), repository.findByValor("telefone").getValor());
    }

    @Test
    public void buscarContato(){
        String contato = "telefone";
        assertNull(repository.findByValor("telefone"));
    }
}
