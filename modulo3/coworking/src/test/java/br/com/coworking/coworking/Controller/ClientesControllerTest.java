package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.ClientesEntity;
import br.com.coworking.coworking.Repository.ClientesRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class ClientesControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ClientesRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoClientes() throws Exception {
        URI uri = new URI("/api/clientes/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmCliente() throws Exception {
        URI uri = new URI("/api/clientes/novo");
        String json = "{\"nome\": \"pedro\", \"cpf\": \"64385437833\", \"dataNascimento\": \"25/04/2000\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome").value("pedro")
        ).andExpect(MockMvcResultMatchers
            .jsonPath("$.cpf").value("64385437833")
        ).andExpect(MockMvcResultMatchers
            .jsonPath("$.dataNascimento").value("25/04/2000")
        );
    }

    @Test
    public void deveRetornarUmCliente() throws Exception {
        ClientesEntity clientes = new ClientesEntity();
        clientes.setNome("junior");
        clientes.setDataNascimento("25/04/2000");
        clientes.setCpf("54934385472");

        ClientesEntity newClientes = repository.save(clientes);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/clientes/ver/{id}", newClientes.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("junior")

        ).andExpect(MockMvcResultMatchers
            .jsonPath("$.dataNascimento")
            .value("25/04/2000")

        ).andExpect(MockMvcResultMatchers
            .jsonPath("$.cpf")
            .value("54934385472")
        );
    }

    @Test
    public void deveRetornarUmClienteEditado() throws Exception {
        ClientesEntity clientes = new ClientesEntity();
        clientes.setCpf("54934385472");
        clientes.setDataNascimento("26/04/2000");
        clientes.setNome("juniorEditado");
        ClientesEntity newClientes = repository.save(clientes);

        String json = "{\"nome\": \"juniorEditado\", \"cpf\": \"54944385472\", \"dataNascimento\": \"26/04/2000\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/clientes/editar/{id}", newClientes.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("juniorEditado")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.cpf")
                .value("54944385472")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.dataNascimento")
                .value("26/04/2000")
        );
    }
}