package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.TipoContatoEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class TipoContatoRepositoryTest {

    @Autowired
    private TipoContatoRepository repository;

    @Test
    public void salvarNome(){
        TipoContatoEntity tipoContato = new TipoContatoEntity();
        tipoContato.setNome("teste");
        repository.save(tipoContato);
        assertEquals(tipoContato.getNome(), repository.findByNome("teste").getNome());
    }

    public void buscarNome(){
        String nome = "teste";
        assertNull(repository.findByNome(nome));
    }
}
