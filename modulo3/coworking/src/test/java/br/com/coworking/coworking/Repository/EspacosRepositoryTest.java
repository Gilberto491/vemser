package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.EspacosEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class EspacosRepositoryTest {

    @Autowired
    private EspacosRepository repository;

    @Test
    public void salvarNomeAndQtdPessoasAndValor(){
        EspacosEntity espacos = new EspacosEntity();
        espacos.setNome("teste");
        espacos.setQtdPessoas(10);
        espacos.setValor(13.8);
        repository.save(espacos);
        assertEquals(espacos.getNome(), repository.findByNome("teste").getNome());
        assertEquals(espacos.getQtdPessoas(), repository.findByQtdPessoas(10).getQtdPessoas());
        assertEquals(espacos.getValor(), repository.findByValor(13.8).getValor());
    }

    @Test
    public void buscarNomeAndQtdPessoasAndValor(){
        String nome = "teste";
        int qtdPessoas = 10;
        double valor = 13.8;

        assertNull(repository.findByNome("teste"));
        assertNull(repository.findByQtdPessoas(10));
        assertNull(repository.findByValor(13.8));
    }
}
