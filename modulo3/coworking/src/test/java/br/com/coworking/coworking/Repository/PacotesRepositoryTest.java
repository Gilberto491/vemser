package br.com.coworking.coworking.Repository;

import br.com.coworking.coworking.Entity.PacotesEntity;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.orm.jpa.DataJpaTest;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNull;

@DataJpaTest
public class PacotesRepositoryTest {

    @Autowired
    private PacotesRepository repository;

    @Test
    public void salvarPacotes(){
        PacotesEntity pacotes = new PacotesEntity();
        pacotes.setValor(13.5);
        repository.save(pacotes);
        assertEquals(pacotes.getValor(), repository.findByValor(13.5).getValor());
    }

    @Test
    public void buscarPacotes(){
        double valor = 13.5;
        assertNull(repository.findByValor(valor));
    }
}
