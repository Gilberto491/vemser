package br.com.coworking.coworking.Controller;

import br.com.coworking.coworking.Entity.EspacosEntity;
import br.com.coworking.coworking.Repository.EspacosRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.ActiveProfiles;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.net.URI;

@SpringBootTest
@AutoConfigureMockMvc
@ActiveProfiles( "test" )
public class EspacosControllerTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private EspacosRepository repository;

    @Test
    public void deveRetornar200QuandoConsultadoEspacos() throws Exception {
        URI uri = new URI("/api/espacos/todos");

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get(uri)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .is(200)
        );
    }

    @Test
    public void salvarERetornarUmEspaco() throws Exception {
        URI uri = new URI("/api/espacos/novo");
        String json = "{\"nome\": \"Teste\", \"qtdPessoas\": \"15\", \"valor\": \"200.890\"}";

        mockMvc
                .perform(MockMvcRequestBuilders
                        .post(uri)
                        .content(json)
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.id").exists()

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Teste")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.qtdPessoas")
                .value(15)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value(200.890)
        );
    }

    @Test
    public void deveRetornarUmEspaco() throws Exception {
        EspacosEntity espacos = new EspacosEntity();
        espacos.setValor(200.890);
        espacos.setQtdPessoas(15);
        espacos.setNome("Teste2");

        EspacosEntity newEspacos = repository.save(espacos);

        mockMvc
                .perform(MockMvcRequestBuilders
                        .get("/api/espacos/ver/{id}", newEspacos.getId())
                        .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .status()
                .isOk()
        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value(200.890)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.qtdPessoas")
                .value(15)

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("Teste2")
        );
    }

    @Test
    public void deveRetornarUmEspacosEditados() throws Exception {
        EspacosEntity espacos = new EspacosEntity();
        espacos.setNome("TesteA");
        EspacosEntity newEspacos = repository.save(espacos);

        String json = "{\"nome\": \"TesteA\", \"qtdPessoas\": \"16\", \"valor\": \"200.900\"}";

        mockMvc
                .perform(
                        MockMvcRequestBuilders
                                .put("/api/espacos/editar/{id}", newEspacos.getId())
                                .content(json)
                                .contentType(MediaType.APPLICATION_JSON)
                ).andExpect(MockMvcResultMatchers
                .jsonPath("$.nome")
                .value("TesteA")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.qtdPessoas")
                .value("16")

        ).andExpect(MockMvcResultMatchers
                .jsonPath("$.valor")
                .value("200.9")
        );
    }
}
