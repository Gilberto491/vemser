class Partidas {

    constructor(mandante, visitante, placar) {
        this._mandante = mandante;
        this._visitante = visitante;
        this._placar = placar;
    }

    get mandante (){
        return this._mandante;
    }

    get visitante() {
        return this._visitante;
    }

    get placar() {
        return this._placar
    }

}

class Jogador {

    constructor(nome, numero) {
        this._nome = nome;
        this._numero = numero;
    }

    get nome (){
        return this._nome;
    }

    get numero() {
        return this._numero;
    }

}

class LigaLocal {
    constructor(nome, tipoEsporte, liga){
        this._nome = nome;
        this._tipoEsporte = tipoEsporte;
        this._status = "ativo";
        this._liga = liga;
        this._jogadores = [];
        this._partidas = [];
    }

    adicionarJogadores = (jogadores) => {
        this._jogadores.push(jogadores);
    }

    buscarJogadoresPorNome = (nomeJogador) => {
       return this._jogadores.filter(jogadores => jogadores.nome == nomeJogador);
    }

    buscarJogadoresPorNumero = (numeroJogador) => {
        return this._jogadores.filter(jogadores => jogadores.numero == numeroJogador);
    }

    adicionarPartida = (partidas) => {
        this._partidas.push(partidas);
    }

    historicoPartidas = () => {
        return this._partidas;
    }
}

/*CRIAR TIME*/
let time = new LigaLocal("inter", "futsal", "estadual");
let time2 = new LigaLocal("gremio", "futsal", "estadual");
let claudio = new Jogador("claudio", 1);
let josimar = new Jogador("josimar", 2);
console.log(time);
console.log(time2);

/*ADICIONAR JOGADORES NO TIME 1*/
time.adicionarJogadores(claudio);

/*ADICIONAR JOGADORES NO TIME 2*/
time2.adicionarJogadores(josimar);

/*BUSCAR JOGADORES*/

time.buscarJogadoresPorNome("claudio");
time2.buscarJogadoresPorNumero(2);

/*HISTÓRICO DE PARTIDAS*/
time.adicionarPartida(new Partidas("time1", "time2", "2x1"));
console.log(time.historicoPartidas());