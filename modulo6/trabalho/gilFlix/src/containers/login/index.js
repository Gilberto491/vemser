import React, { Component } from 'react';
import Header from '../../header';
import './login.css';
import loginIcone from './login.png'
import { useHistory } from 'react-router-dom';


export default class Login extends Component { 
  constructor( props ){
    super( props );
    this.state = {
     login: '',
     email: '',
     senha: ''
    }
    this.cadastrar = this.cadastrar.bind( this );
    this.acessar = this.acessar.bind( this );
    this.email = this.email.bind( this );
  }
  
  email = ( ev ) => {
    const emailRecebido = ev.target.value ;
    this.setState( {
      email: emailRecebido
    } )
    console.log( emailRecebido )
  }
  
  senha = ( ev ) => {
    const senhaRecebido = ev.target.value ;
    this.setState( {
      senha: senhaRecebido
    } )
    console.log( senhaRecebido )
  }
  
  cadastrar() {
    const { email, senha, login } = this.state;
    login = [ 
      email = email, 
      senha = senha 
    ]
    
    console.log( login )
    localStorage.setItem ( 'user', JSON.stringify ( login ) ) 
    alert('Cadastrado com sucesso')
    
  }
  
  acessar() {
    const history = useHistory();
    const { login } = this.state;
    if( localStorage.getItem( 'user',  login ) ) {
      history.push('/')
      alert('logado com sucesso');
    }else {
      alert('senha ou email incorreto');
    }
  }

  render() {
    return(
    <React.Fragment>
      <Header/>
            <div className='App-header'>
              <form className='form' action=''>
                <div className='login'>
                    <div className='login-top'>
                      <img className='imgLogin' src={ loginIcone } alt='login'/>
                      <h2 className='loginH2'>Tela de Login</h2>    
                    </div>

                    <div className='login-group'>
                      <label className='login-label'>Email: </label>
                      <input onChange={ this.email } type='text' name='email' placeholder='Ex. email@gmail.com' value={this.state.email}/>
                    </div>

                    <div className='login-group'>
                      <label className='login-label'>Senha: </label>
                      <input onChange={ this.senha } type='password' name='senha' placeholder='Digite sua senha' value={this.state.senha}/>
                    </div>

                    <div className='login-group'>
                      <button onClick={ this.acessar } className='login-button'>Acessar</button> 
                    </div>

                    <div className='login-group'>
                      <button onClick={ this.cadastrar } className='login-button'>Cadastrar</button>
                    </div>

                </div>
              </form>
            </div>
    </React.Fragment>
      )
    }
}