import React, { Component } from 'react';
import { EpisodiosApi, Episodio } from '../../models';
import './detalhesEpisodios.css'
import Header from '../../header'

export default class DetalhesEpisodios extends Component {
  constructor( props ) {
    super( props );
    this.episodioApi = new EpisodiosApi();
    this.state = {
      detalhes: null
    }
  }

  componentDidMount() {
    const episodioId = this.props.match.params.id;
    const requisicoes = [
      this.episodioApi.buscarEpisodio( episodioId ),
      this.episodioApi.buscarDetalhes( episodioId ),
      this.episodioApi.buscarNota( episodioId )
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        const { id, nome, duracao, temporada, ordemEpisodio, thumbUrl } = respostas[0];
        this.setState({
          episodio: new Episodio(id, nome, duracao, temporada, ordemEpisodio, thumbUrl),
          detalhes: respostas[1],
          objNota: respostas[2]
        })
      })
  }

  render() {
    const { episodio, detalhes, objNota } = this.state;
    

return(
  <React.Fragment>
    <Header/>
   <div className='App-header'>
      { episodio && <h3 className='nome'> { episodio.nome } </h3> } 
      { episodio && <img src = { episodio.url } alt = { episodio.nome }></img> } 
      {
        detalhes ?
        <React.Fragment>
            <p className='sinopse'>{ detalhes.sinopse }</p>
            <span>Data de estréia: { new Date( detalhes.dataEstreia ).toLocaleDateString() }</span>
            <span>IMDb: { detalhes.notaImdb * 0.5 }</span>
            <span>Sua nota: { objNota ? objNota.nota : 'N/D' }</span>
        </React.Fragment> : null
      }
          </div>
  </React.Fragment>
    )
  }
}