import React, { Component } from 'react';
import { BrowserRouter as Router, Route } from 'react-router-dom';

import { PrivateRoute } from '../components/rotaPrivada';

import Home from './home';
import ListaAvaliacoes from './avaliacoes';
import DetalhesEpisodios from './detalhesEpisodios';
import PaginaPrincipal from './paginaPrincipal';
import Login from './login';
import Formulario from '../components/formularioUi';

export default class App extends Component {
  render() {
    return ( 
    <div className="App">
      <Router>
        <Route path = '/' exact component = { PaginaPrincipal } /> 
        <Route path = '/ranking' exact component = { ListaAvaliacoes } />
        <Route path = '/formulario' exact component = { Formulario } />
        <Route path = '/login' exact component = { Login }/>
        <PrivateRoute path = '/episodios' exact component = { Home } />
        <PrivateRoute path = '/episodio/:id' exact component = { DetalhesEpisodios } />
      </Router>
    </div>
    )
  };
}