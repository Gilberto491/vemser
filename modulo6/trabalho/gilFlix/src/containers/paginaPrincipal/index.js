import React, { Component } from 'react';
import { EpisodiosApi, ListaEpisodios } from '../../models';
import './paginaPrincipal.css';
import { Link } from 'react-router-dom';
import gilFlix from '../../header/imagens/logo.png';
import lupa from '../../header/imagens/lupa.png';

export default class PaginaPrincipal extends Component {

  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: [],
      filmes: [],
      detalhes: null
    }
    this.loadFilmes = this.loadFilmes.bind( this );
  }

  componentDidMount() {
    this.loadFilmes();
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    Promise.all( requisicoes )
      .then( respostas => {
        let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
        this.setState( state => {
          return {
            ...state,
            listaEpisodios: listaEpisodios._todos.concat([])
          }
        })
      })
  }

  ///////////////////////////////////

  loadFilmes() {
    let url = 'http://localhost:9000/api/episodios';
    fetch( url )
      .then( r => r.json() )
      .then( json => {
        this.setState( { filmes: json } );
        console.log(json)
      } );
    }

    ///////////////////////////////////

  filtrarPorTermo = evt => {
    const termo = evt.target.value;
    this.episodiosApi.filtrarPorTermo( termo )
    .then( resultados => {
      this.setState({
        filmes: this.state.filmes.filter( e => resultados.some( r => r.episodioId === e.id ) )
        })
      } )
  }

  render() {
    const aleatorio = Math.floor( Math.random() * (this.state.listaEpisodios.length - 1) + 1);
    return (
        <div className='body'>
          <header>
            <div className='wrapper'>
              <Link to='/'><img className='logo' src={gilFlix} alt='logo'></img></Link>
              <nav>
                <ul>
                  <li> <Link to='/'>Filmaria</Link>  </li>
                  <li> <Link to={`/episodio/${aleatorio}`}>Aleatório</Link> </li>
                   <input type="text" placeholder="Ex.: ministro" onBlur={ this.filtrarPorTermo } />
                   <img className='lupa' src={lupa} alt='lupa'></img>
                </ul>
              </nav>
            </div>
          </header>

          <div className='cards'>
            <div className='content'>
              <div className='row lista-filmes'>
          {this.state.filmes.map( filme => {
            return (
              <article className='col col-sm-12 col-md-4 col-lg-2 filme'>
                <Link to={`/episodio/${ filme.id }`}> <img src={ filme.thumbUrl } alt='filme' className='img'/> </Link> 
                <h3 className='h3'> { filme.nome } </h3>
                <p> Temporada: { filme.temporada } </p>
                <p> Episódio: { filme.ordemEpisodio } </p>
                <Link to={`/episodio/${ filme.id }`}><button className='button'>Acessar</button></Link>
              </article>
            )
          } ) }
              </div>
            </div>
          </div>
        </div>
    
    )
  }

}