import React, { Component } from 'react'
import './header.css';
import { EpisodiosApi, ListaEpisodios } from '../models';
import gilFlix from './imagens/logo.png'
import { Link } from 'react-router-dom';

export default class Header extends Component {

  render() {
    return (
         <header>
            <div className='wrapper'>
              <Link to='/'><img className='logo' src={ gilFlix } alt='logo'></img></Link>
              <nav>
                <ul>
                  <li> <Link to='/'>Filmaria</Link>  </li>
                </ul>
              </nav>
            </div>
          </header>
    )
  }
}