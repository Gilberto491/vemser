import React, { useState, useEffect } from 'react';
import EpisodiosApi from '../../models/episodiosApi';

function Formulario() {
  const [ qtdAssistida, setqtdAssistida ] = useState( 1 );
  const [ nome, setNome ] = useState('Marcos');

  useEffect( () => {
    /*this.episodiosApi = new EpisodiosApi();
    this.episodioApi.registrarNota( { nota: 10, episodioId: 14 } );*/
  } )

  return (
    <React.Fragment>
      <p>Implementação de formulário:</p>
      <p>{ qtdAssistida }</p>
      <button type='button' onClick={ () => setqtdAssistida( qtdAssistida + 1 ) }>Já assisti!</button>
      <input type='text' onBlur={ evt => setNome( evt.target.value ) }/>
      <p>{ nome }</p>
    </React.Fragment>
  )
}

export default Formulario;