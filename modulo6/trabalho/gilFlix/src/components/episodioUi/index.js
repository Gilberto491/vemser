import React, { Component } from 'react';

export default class EpisodioUi extends Component {
  render() {
    const { episodio } = this.props;
    
    return (
      <React.Fragment>
         <h2>{ episodio.nome }</h2>
          <img src = { episodio.url } alt = { episodio.nome }></img>
          <span className = "descricao">Já Assisti: { episodio.assistido ? 'Sim' : 'Não' } </span>
          <span className = "descricao">Quantidade: { episodio.quantidade ? episodio.quantidade : "0" }</span>
          <span className = "descricao">Duração: { episodio.duracaoEmMin } </span>
          <span className = "descricao">Temporada/Episódio:  { episodio.temporadaEpisodio } </span> 
          <span className = "descricao">Nota: { episodio.nota ? episodio.nota : "0" }</span>
      </React.Fragment>
    )
  }
}