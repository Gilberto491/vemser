/*CALCULAR CIRCULO*/

let objeto = {
    raio: 3,
    tipoCalculo = "A"
}

function calcularCirculo({raio, tipoCalculo:tipo}) {
   
    return Math.ceil(tipo == "A" ? Math.PI * Math.pow(raio,2): 2 * Math.PI * raio);
}

console.log(calcularCirculo(objeto));

/*ANO BIXSSEXTO*/

function naoBissexto(ano) {
   return (ano % 400 == 0) || (ano % 4 == 0 && ano % 100 != 0) ? false : true;
}   

let anoBixssexto = 2016;
let anoNaoBixssexto = 2017;

console.log(naoBissexto(anoBixssexto));
console.log(naoBissexto(anoNaoBixssexto));

/*SOMAR PARES*/

function somarPares(paresExt) {

    let somatorio = 0;

    for (let i = 0; i < paresExt.length; i+=2) {
            somatorio += paresExt[i];  
    }
   console.log(somatorio);
}

somarPares([1, 56, 4.34, 6, -2]);

/*SOMA DIFERENTONA*/

function adicionar(a) {
    return function(b) {
        return a + b;
    };
  }
  
//let adicionar = a => b => a + b;

  console.log(adicionar(3)(4));
  console.log(adicionar(5642)(8749));

/*EXTRA*/

const aula = {
    turma: 2020,
    qtdAlunos: 12,
    qtdAulasNoModulo(modulo){
        switch (modulo) {
            case 1:
                console.log(10)
                break;
            default:
                break;
        }
    }
}

/* let is_divisivel = (divisor, numero) => !( numero % divisor );
const divisor = 2;
console.log(is_divisivel(divisor, 5));
console.log(is_divisivel(divisor, 8));
console.log(is_divisivel(divisor, 16));
console.log(is_divisivel(divisor, 9)); */

const divisivelPor = divisor => numero => !( numero % divisor );
const is_divisivel = divisivelPor(2);
console.log(is_divisivel(5));
console.log(is_divisivel(8));
console.log(is_divisivel(16));
console.log(is_divisivel(9));