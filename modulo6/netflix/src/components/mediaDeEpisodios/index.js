import React, { Component } from 'react';

class MediaDeEpisodios extends Component {
  render() {
    return (
      <div>
        <span>Média de Episódios: { this.props.metodo } episódios </span> <br></br>
      </div>
    )
  }
}

export default MediaDeEpisodios;