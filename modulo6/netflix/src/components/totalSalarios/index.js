import React, { Component } from 'react';

class TotalSalarios extends Component {
  render() {
    return (
      <div>
        <span>Salario Total:  { 'R$ ' + this.props.metodo }</span> <br></br>
      </div>
    )
  }
}

export default TotalSalarios;