import React, { Component } from 'react'
import SeriesInvalidas from './components/seriesInvalidas'
import FiltrarPorAno from './components/filtrarPorAno'
import ProcurarPorNome from './components/procurarPorNome'
import MediaDeEpisodios from './components/mediaDeEpisodios'
import TotalSalarios from './components/totalSalarios'
import BuscarPorGenero from './components/buscarPorGenero'
import BuscarPorTitulo from './components/buscarPorTitulo'
import Creditos from './components/creditos'
import Formatacao from './components/formatacao'
import Abreviar from './components/abreviar'

let series = new Array (
  {
    titulo: "Stranger Things",
    anoEstreia: 2036,
    diretor: [
      "Matt Duffer",
      "Ross Duffer"
    ],
    genero: [
      "Suspense",
      "Ficcao Cientifica",
      "Drama"
    ],
    elenco: [
      "Winona Ryder",
      "David Harbour",
      "Finn Wolfhard",
      "Millie Bobby Brown",
      "Gaten Matarazzo",
      "Caleb McLaughlin",
      "Natalia Dyer",
      "Charlie Heaton",
      "Cara Buono",
      "Matthew Modine",
      "Noah Schnapp"
    ],
    temporadas: 2,
    numeroEpisodios: 17,
    distribuidora: "Netflix"
  },
  {
    titulo: "Game Of Thrones",
    anoEstreia: 2011,
    diretor: [
      "David Benioff",
      "D. B. Weiss",
      "Carolyn Strauss",
      "Frank Doelger",
      "Bernadette Caulfield",
      "George R. R. Martin"
    ],
    genero: [
      "Fantasia",
      "Drama"
    ],
    elenco: [
      "Peter Dinklage",
      "Nikolaj Coster-Waldau",
      "Lena Headey",
      "Emilia Clarke",
      "Kit Harington",
      "Aidan Gillen",
      "Iain Glen ",
      "Sophie Turner",
      "Maisie Williams",
      "Alfie Allen",
      "Isaac Hempstead Wright"
    ],
    temporadas: 7,
    numeroEpisodios: 67,
    distribuidora: "HBO"
  },
  {
    titulo: "The Walking Dead",
    anoEstreia: 2010,
    diretor: [
      "Jolly Dale",
      "Caleb Womble",
      "Paul Gadd",
      "Heather Bellson"
    ],
    genero: [
      "Terror",
      "Suspense",
      "Apocalipse Zumbi"
    ],
    elenco: [
      "Andrew Lincoln",
      "Jon Bernthal",
      "Sarah Wayne Callies",
      "Laurie Holden",
      "Jeffrey DeMunn",
      "Steven Yeun",
      "Chandler Riggs ",
      "Norman Reedus",
      "Lauren Cohan",
      "Danai Gurira",
      "Michael Rooker ",
      "David Morrissey"
    ],
    temporadas: 9,
    numeroEpisodios: 122,
    distribuidora: "AMC"
  },
  {
    titulo: "Band of Brothers",
    anoEstreia: 20001,
    diretor: [
      "Steven Spielberg",
      "Tom Hanks",
      "Preston Smith",
      "Erik Jendresen",
      "Stephen E. Ambrose"
    ],
    genero: [
      "Guerra"
    ],
    elenco: [
      "Damian Lewis",
      "Donnie Wahlberg",
      "Ron Livingston",
      "Matthew Settle",
      "Neal McDonough"
    ],
    temporadas: 1,
    numeroEpisodios: 10,
    distribuidora: "HBO"
  },
  {
    titulo: "The JS Mirror",
    anoEstreia: 2017,
    diretor: [
      "Lisandro",
      "Jaime",
      "Edgar"
    ],
    genero: [
      "Terror",
      "Caos",
      "JavaScript"
    ],
    elenco: [
      "Amanda de Carli",
      "Alex Baptista",
      "Gilberto Junior",
      "Gustavo Gallarreta",
      "Henrique Klein",
      "Isaias Fernandes",
      "João Vitor da Silva Silveira",
      "Arthur Mattos",
      "Mario Pereira",
      "Matheus Scheffer",
      "Tiago Almeida",
      "Tiago Falcon Lopes"
    ],
    temporadas: 5,
    numeroEpisodios: 40,
    distribuidora: "DBC"
  },
  {
    titulo: "Mr. Robot",
    anoEstreia: 2018,
    diretor: [
      "Sam Esmail"
    ],
    genero: [
      "Drama",
      "Techno Thriller",
      "Psychological Thriller"
    ],
    elenco: [
      "Rami Malek",
      "Carly Chaikin",
      "Portia Doubleday",
      "Martin Wallström",
      "Christian Slater"
    ],
    temporadas: 3,
    numeroEpisodios: 32,
    distribuidora: "USA Network"
  },
  {
    titulo: "Narcos",
    anoEstreia: 2015,
    diretor: [
      "Paul Eckstein",
      "Mariano Carranco",
      "Tim King",
      "Lorenzo O Brien"
    ],
    genero: [
      "Documentario",
      "Crime",
      "Drama"
    ],
    elenco: [
      "Wagner Moura",
      "Boyd Holbrook",
      "Pedro Pascal",
      "Joann Christie",
      "Mauricie Compte",
      "André Mattos",
      "Roberto Urbina",
      "Diego Cataño",
      "Jorge A. Jiménez",
      "Paulina Gaitán",
      "Paulina Garcia"
    ],
    temporadas: 3,
    numeroEpisodios: 30,
    distribuidora: null
  },
  {
    titulo: "Westworld",
    anoEstreia: 2016,
    diretor: [
      "Athena Wickham"
    ],
    genero: [
      "Ficcao Cientifica",
      "Drama",
      "Thriller",
      "Acao",
      "Aventura",
      "Faroeste"
    ],
    elenco: [
      "Anthony I. Hopkins",
      "Thandie N. Newton",
      "Jeffrey S. Wright",
      "James T. Marsden",
      "Ben I. Barnes",
      "Ingrid N. Bolso Berdal",
      "Clifton T. Collins Jr.",
      "Luke O. Hemsworth"
    ],
    temporadas: 2,
    numeroEpisodios: 20,
    distribuidora: "HBO"
  },
  {
    titulo: "Breaking Bad",
    anoEstreia: 2008,
    diretor: [
      "Vince Gilligan",
      "Michelle MacLaren",
      "Adam Bernstein",
      "Colin Bucksey",
      "Michael Slovis",
      "Peter Gould"
    ],
    genero: [
      "Acao",
      "Suspense",
      "Drama",
      "Crime",
      "Humor Negro"
    ],
    elenco: [
      "Bryan Cranston",
      "Anna Gunn",
      "Aaron Paul",
      "Dean Norris",
      "Betsy Brandt",
      "RJ Mitte"
    ],
    temporadas: 5,
    numeroEpisodios: 62,
    distribuidora: "AMC"
  } )

  const anoAtual = new Date().getFullYear();

  //Series Invalidas
  Array.prototype.invalidas = function() { //arrumar function
    let resultado = [];
    series.map( elem => {
      if( elem.anoEstreia > anoAtual || 
        elem.distribuidora == null ||
        elem.diretor == null || 
        elem.elenco == null ||
        elem.genero == null ||
        elem.numeroEpisodios == null ||
        elem.temporadas == null ||
        elem.titulo == null ) {
        resultado.push( elem.titulo );
      }
    } );
    return resultado + ' ';
  }

  //Series de Determinado Ano
  Array.prototype.filtrarPorAno = function( ano ) {
    let resultado = [];
    series.map( elem => {
    if( elem.anoEstreia >= ano && elem.anoEstreia < anoAtual ) {
      resultado.push( elem.titulo );
    }
  } );
    return resultado + ' ';
  }

  //Procurar Por Nome
  Array.prototype.procurarPorNome = function( nome ) {
   for ( let i = 0; i < series.length; i++ ) {
     for ( let j = 0; j < series[ i ].elenco.length; j++ ) {
       if( series[ i ].elenco[ j ].includes( nome ) ){
         return true;
       }
     }
    }
    return false;
  }

  //Média de Episódios
  Array.prototype.mediaDeEpisodios = function() {
    var somador = 0;
    series.map( elem => {
      somador += elem.numeroEpisodios;
    } );
    let resultado =  somador / series.length;
    return Math.round( resultado );
  }

  //Mascada Em Séries
  Array.prototype.totalSalarios = function( especifico ) {
    
      let salarioDiretor = series[ especifico ].diretor.length * 10000;
      let salarioElenco = series[ especifico ].elenco.length * 40000;
      return salarioDiretor + salarioElenco;
  }

  //Buscas
  Array.prototype.queroGenero = function( genero ) {
    let resultado = [];
    for ( let i = 0; i < series.length; i++ ) {
      for ( let j = 0; j < series[ i ].genero.length; j++ ) {
        if( series[ i ].genero[ j ].includes( genero ) ){
          resultado.push( series[ i ].titulo );
        }
      }
     }
     return resultado;
  }

  Array.prototype.queroTitulo = function( titulo ) {
    let resultado = [];
    series.map( elem => {
      if( elem.titulo.search( titulo ) >= 0 ){
        resultado.push( elem.titulo );
      }
    } );
    return resultado;
  }

  //Créditos
  Array.prototype.creditos = function( index ) {
    let diretores = [];
    let elenco = [];
    let nome = series[ index ].titulo;
    
    for ( let i = 0; i < series[ index ].diretor.length; i++ ) {
      diretores.push( series[ index ].diretor[ i ] );
    }
    for (let j = 0; j < series[ index ].elenco.length; j++) {
      elenco.push( series[ index ].elenco[ j ] )
    }

    diretores.sort();
    elenco.sort();

    return (
      <Formatacao nome = { nome } diretores = { diretores } elenco = { elenco }/>
    )
  }

  //Hastag Secreta

  let temAbreviacao = ( string ) => {
    let resultado;
    resultado = string.indexOf( '.' );
    if( resultado > 0 ){
      return true;
    }
    return false;

  }

  Array.prototype.abreviar = function() {
    let resultado = [];
    let posicao = [];
    let array = [];
    let serieCorreta = 0;
    for ( let i = 0; i < series.length; i++ ) {
      for ( let j = 0; j < series[ i ].elenco.length; j++ ) {
       if( temAbreviacao( series[ i ].elenco[ j ] ) ){
         serieCorreta++;
         if(serieCorreta == series[ i ].elenco.length){
           for (let l = 0; l < series[ i ].elenco.length; l++) {
             posicao = series[ i ].elenco[ l ].indexOf('.') - 1;
             array = series[ i ].elenco[ l ].charAt( posicao )
             resultado.push( series[ i ].elenco[ l ] );
             console.log(array);
           }
         }
       }
      }
    }
    return resultado;
  }

  


function App() {
  return (
    <div >
      <SeriesInvalidas metodo = { series.invalidas() }/>
      <FiltrarPorAno metodo = { series.filtrarPorAno( 2015 ) }/>
      <ProcurarPorNome metodo = { series.procurarPorNome( 'QualquerCOisa' ) }/>
      <ProcurarPorNome metodo = { series.procurarPorNome( 'Bryan Cranston' ) }/>
      <ProcurarPorNome metodo = { series.procurarPorNome( 'Gilberto Junior' ) }/>
      <MediaDeEpisodios metodo = { series.mediaDeEpisodios() }/>
      <TotalSalarios metodo = { series.totalSalarios( 2 ) }/>
      <BuscarPorGenero metodo = { series.queroGenero( 'Caos' ) }/>
      <BuscarPorGenero metodo = { series.queroGenero( 'Terror' ) }/>
      <BuscarPorTitulo metodo = { series.queroTitulo( 'The' ) }/>
      <Creditos metodo = { series.creditos( 1 ) }/>
      <Abreviar metodo = { series.abreviar() }/>
    </div>
  );
}

export default App;
