import React from 'react';
import './navLogin.css';

export default function NavLogin() {
  return (
    <nav className='navCaptcha'>
      <ul>
        <li> <a>Protegido por reCAPTCHA </a> </li>
        <li> <a className='privacidadeEcondicoes'> - Privacidade </a> </li>
        <li> <a className='privacidadeEcondicoes'> - Condições</a> </li>
      </ul>
    </nav>
  )
}
