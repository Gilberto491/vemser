import React from 'react'
import './header.css';
import ConteudoHeader from '../conteudoHeader';

export default function Header() {
  return (
    <div>
      <header>
        <div className='wrapper'>
          <div className='display'>
            <p><span className='inf'>voa, frete, voa 🕊️ só R$ 3,99</span> - por tempo limitado nesse <u className='link'>baando de produtos</u></p> 
          </div>
        </div>
      </header>
      <ConteudoHeader/>
    </div>
  )
}
