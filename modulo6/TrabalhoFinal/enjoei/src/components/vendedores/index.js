import React from 'react'
import './vendedores.css';
import pessoa1 from '../../images/pessoa1.jpg';
import pessoa2 from '../../images/pessoa2.png';
import pessoa3 from '../../images/pessoa3.png';
import pessoa4 from '../../images/pessoa4.png';

export default function Vendedores() {
  return (
    <div className='rowVendedoresIcone'>
      <div className='containerVendedoresIcone'>
        <div className='containerTotalVendedores'>
          <ul>
            <li>
              <img className='formatarImagem' src={ pessoa1 }/>
              <h2 className='tituloNomeImagemVendedor'>nath araújo</h2> 
            </li>
            <li>
              <img className='formatarImagem' src={ pessoa2 }/>
              <h2 className='tituloNomeImagemVendedor'>marcela ceribelli</h2>
            </li>
            <li className='formatacaoLateralBorda'>
              <img className='formatarImagem' src={ pessoa3 }/>
              <h2 className='tituloNomeImagemVendedor'>alexandra herchcovitch</h2> 
            </li>
            <li>
              <img className='formatarImagem' src={ pessoa4 }/>
              <h2 className='tituloNomeImagemVendedor'>fiorella mattheis</h2> 
            </li>
          </ul>
        </div>
      </div>
    </div>
  )
}
