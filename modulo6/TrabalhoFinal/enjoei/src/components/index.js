import Header from './header';
import HeaderInterno from './headerInterno';
import Footer from './footer';
import Vendedores from './vendedores';
import Formulario from './formulario';
import Banner from './banner';
import NavLogin from './navLogin';
import Formatacao1index from './formatacao1index';
import Formatacao2index from './formatacao2index';
import PageErro from './erro';
import VendedorInterno from './vendedorInterno';
import CadastroPage from './cadastroPage';
import LoginPage from './loginPage';

export { LoginPage, CadastroPage, Header, HeaderInterno, Footer, Vendedores, Formulario, Banner, NavLogin, Formatacao1index, Formatacao2index, PageErro, VendedorInterno };