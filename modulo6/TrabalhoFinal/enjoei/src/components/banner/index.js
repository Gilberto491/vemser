import React, { Component } from 'react';
import './banner.css';
import { Carousel } from 'antd';

export default class Banner extends Component {
  constructor( props ){
    super( props );
    this.state = {
      banner: []
    }
  }

  componentDidMount() {
    this.loadApiBanners();
  }

  loadApiBanners() {
    let url = 'http://localhost:9000/api/banners';
    fetch( url ) 
      .then( r => r.json() )
      .then( json => {
        this.setState( { banner: json } );
    } );
  }

  render(){
    return (  
      <div className='espace'>
        <Carousel autoplay>
          {
            this.state.banner.map( ( banners ) => {
              return (
                <div className='banner'> <img className='bannerImg' src={ banners.imagem } alt=''/> </div>
              )
            } )
          }
        </Carousel>
      </div>
    )
  };
}
