import React, { Component } from 'react';
import icone from '../../images/icone.png';
import imgErro from '../../images/ops-img.png';
import { Link } from 'react-router-dom';
import './erro.css';

export default class PageErro extends Component {
  render(){
    return ( 
      <div className='erro'>
        <Link to='/'> <img src={ icone } alt='logo' className='logoHeader logoErro'/> <br/> </Link>
          <img className='imgErro' src={ imgErro } alt='imagem página não encontrada'/> <br/>
          <span className='bold'>foi mal, não encontramos essa página.</span> <br/>
          <p >e agora? e agora você mantém a calma, o sereno, não perde a compostura e segue o baile.</p> <br/>
        <Link to='/'> <button  className='buttonEjoei'>voltar para a home</button> </Link>
      </div>    
    )
  };
}
