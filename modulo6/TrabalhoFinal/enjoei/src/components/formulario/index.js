import React, { useState } from 'react';
import { setUser } from '../../services/Login';
import { useHistory } from 'react-router-dom';
import EnjoeiApi from '../../models/EnjoeiApi';

export default function Formulario() {

  const [ email, setEmail ] = useState( '' );
  const [ senha, setSenha ] = useState( '' );
  const [ confirmarSenha, setConfirmarSenha ] = useState( '' );
  const history = useHistory();
  const enjoeiApi = new EnjoeiApi();

  const handleSubmit = ( e ) => {
    e.preventDefault()
    if( senha === confirmarSenha ) {
      setUser( { email, senha } );
      enjoeiApi.registrarUsuario( { email, senha } );
      history.push( '/' );
    } else {
      setSenha( '' );
      alert( 'as senha não condizem' );
    }
  } 

  return (
    <div>
      <form onSubmit={ handleSubmit } >
        <div className='login-group'>
          <label className='login-label '>Email</label>
          <input type='email' name='email' onChange={ ( e ) => setEmail( e.target.value ) } placeholder='email' required autoComplete='off' /> 
          <label className='senha-label'>senha</label> 
          <input minLength='6' type='password' name='senha' onChange={ ( e ) => setSenha( e.target.value ) } placeholder='senha' required autoComplete='off'/>  
          <label className='senha-label'>Confirme a senha</label>
          <input minLength='6' type='password' name='senhaConfirmar' onChange={ ( e ) => setConfirmarSenha( e.target.value ) } placeholder='confirme a senha' required autoComplete='off'/>
        </div>
          <label className='checkboxLabel novidadesCheckbox'> <input type='checkbox'/> <span>aceito receber novidades do enjoei</span> </label> <br/>
          <label className='checkboxLabel'> <input type='checkbox' required/> <span>estou de acordo com os termos de serviço do enjoei</span> </label> <br/>
          <button className='buttonEjoei'>Cadastrar</button>
      </form>
    </div>
    )
  }
