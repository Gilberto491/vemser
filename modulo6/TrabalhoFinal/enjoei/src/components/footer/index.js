import React from 'react';
import './footer.css';
import icone from '../../images/icone.png';
import ListaFooter from './listaFooter';
import { Link } from 'react-router-dom';

export default function Footer() {
  return (
    <div>
      <ListaFooter />
      <hr className='hrFooter'/> <br/>
      <Link to='/'> <img src={ icone } className='imgFooter'/> </Link>
      <footer>
        <span className='msgFooter'>
          enjoei © 2020 - todos os direitos reservados - enjoei.com.br atividades de internet S.A. CNPJ: 16.922.038/0001-51 av. isaltino victor de moraes, 437, vila bonfim, embu das artes, sp, 06806-400 - (11) 3197-4883
        </span>
      </footer>
      <button className='enjuapp'>📱 baixe o enjuapp</button>
    </div>
  )
}
