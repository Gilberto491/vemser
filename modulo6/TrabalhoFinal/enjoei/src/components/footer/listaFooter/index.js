import React from 'react'

export default function ListaFooter() {
  return (
    <div>
      <hr className='hrFooter'/> <br/>
      <footer className='footerNav'>
        <nav className='footerAlinhamento'>
          <ul >
            <li> <h1 className='tituloFooter'>utilidades</h1> </li>
            <li> ajuda </li>
            <li> como vender </li>
            <li> marcas </li>
            <li> termos de uso </li>
            <li> politica de privacidade </li>
            <li> trabalhe no enjoei </li>
            <li> black friday </li>
            <li> investidores </li>
          </ul>
          <ul>
            <li> <h1 className='tituloFooter'>minha conta</h1> </li>
            <li> minha loja </li>
            <li> minhas vendas </li>
            <li> minhas compras </li>
            <li> enjubank </li>
            <li> yeyezados </li>
            <li> configurações </li>
          </ul>
          <ul >
            <li> <h1 className='tituloFooter'>marcas populares</h1> </li>
            <li> farm <span> zara </span></li>
            <li> melissa</li>
            <li> forever21 </li>
            <li> nike  </li>
            <li> adidas  </li>
            <li> kipling  </li>
            <li> ver todas </li>
          </ul>
          <ul >
            <li> <h1 className='tituloFooter'>siga a gente</h1> </li>
            <li> facebook </li>
            <li> twitter </li>
            <li> instagram </li>
          </ul>
        </nav>
      </footer>
    </div>
  )
}
