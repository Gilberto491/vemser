import React, { Component } from 'react';
import { Link } from 'react-router-dom';

export default class Formatacao2index extends Component {
  constructor( props ){
    super( props );
    this.state = {
      produtos: [],
      categorias: []
    }
  }

  componentDidMount() {
    this.loadApiProdutos();
    this.loadApiCategorias();
  }

  loadApiProdutos() {
    let url = 'http://localhost:9000/api/Produtos';
    fetch( url ) 
      .then( r => r.json() )
      .then( json => {
        this.setState( { produtos: json } );
    } );
  }

  loadApiCategorias() {
    let url = 'http://localhost:9000/api/categorias';
    fetch( url ) 
      .then( r => r.json() )
      .then( json => {
        this.setState( { categorias: json } );
    } );
  }

  render(){
    return ( 
      <div>
        <div className='espacamentoCategoriaProduto'>
          {
            this.state.categorias.map( ( categoria ) => {
              if( categoria.id === 3 ) {
                return (
                  <div>
                    <h2 className='tituloLoja'> { categoria.nome }</h2>
                    <span className='subTituloLoja'> { categoria.subTitulo } <u> arrasar ⇨</u>  </span>
                  </div>
                )
              }
            } )
          }
          <div className=' lista-loja'>
            {
              this.state.produtos.map( ( produto ) => {
                if( produto.idCategoria === 3 ) {
                  return (
                    <article className={ `produtoEspecifico-${ produto.id }` } >
                      <Link to={ `/detalhes/${ produto.id }` }> <img key={ produto.id } src={ produto.imagem } alt='imagem' className={ `imgHome imgHome-${ produto.id }` }/> </Link> 
                      <Link to={ `/detalhes/${ produto.id }` }> <span className='precoProduto'>R$ { produto.preco }</span> </Link>
                    </article>
                  )
                } 
              } )
            }
          </div>
        </div>

        <div className='espacamentoCategoriaProduto'>
          {
              this.state.categorias.map( ( categoria ) => {
                if( categoria.id === 4 ) {
                  return (
                    <div>
                      <h2 className='tituloLoja'> { categoria.nome }</h2>
                      <span className='subTituloLoja'> { categoria.subTitulo } <u> brilhar ⇨</u> </span>
                    </div>
                  )
                }
              } )
            }
            <div className=' lista-loja'>
              {
                this.state.produtos.map( ( produto ) => {
                  if( produto.idCategoria === 4 ){
                    return (
                      <article className={ `produtoEspecifico-${ produto.id }` } >
                        <Link to={ `/detalhes/${ produto.id }` }> <img key={ produto.id } src={ produto.imagem } alt='imagem' className={ `imgHome imgHome-${ produto.id }` }/> </Link> 
                        <Link to={ `/detalhes/${ produto.id }` }> <span className='precoProduto'>R$ { produto.preco }</span> </Link>
                      </article>
                    )
                  } 
                } )
              }
          </div>
        </div>    
      </div>
    )
  };
}
