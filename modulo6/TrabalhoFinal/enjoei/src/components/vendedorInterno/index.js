import React, { Component } from 'react';
import icone from '../../images/icone.png';

import './vendedor.css';

export default class VendedorInterno extends Component {
  
  constructor( props ) {
    super( props );
    this.state = {
      vendedor: []
    }
    this.formatar = this.formatar.bind( this );
  }

  loadApiVendedor() {
    const vendedorAleatorio = Math.ceil( Math.random() * ( 4 - 1 ) + 1);
    let url = `http://localhost:9000/api/vendedor/${ vendedorAleatorio }`;
      fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
          this.setState( { vendedor: json } );
    } );
  }

  formatar( criacao ) {
   const meses = [ "jan", "fev", "mar", "abr", "mai", "jun", "jul", "ago", "set", "out", "nov", "dez" ];
   return meses[ new Date( criacao ).getMonth() ] + '/' + new Date( criacao ).getFullYear();
  }

  componentDidMount() {
    this.loadApiVendedor();
  } 

  render() {
    return ( 
      <div>
        <div className='containerVendedor'> 
         <div className='vendedorInfPessoais'>
            <img className='iconeVendedor' src={ icone }></img>
              <div className='titulosVendedorInfPessoais'>
                <span className='vendedorNome'>{ this.state.vendedor.nome }</span> <br/>
                <span className='vendedorTituloPrincipal'>{ this.state.vendedor.cidade } { this.state.vendedor.estado }</span> <br/>
              </div>
          <div className='btnSeguir'>
            <button className='vendedorSeguir'>seguir</button>
          </div>
        </div>
        <div className='vendedorInfEntregas'>
          <div className='vendedorAvaliacao'>
            <span className='vendedorTituloPrincipal'>avaliação</span> <br/>
            <span className='vendedorSubTituloPrincial'>⭐⭐⭐⭐⭐</span>
          </div>
          <div className='vendedorEntregas'>
            <span className='vendedorTituloPrincipal'>últimas entregas</span> <br/>
            <span className='vendedorSubTituloPrincial'>🟢🟢🟢🟢🟢</span>
          </div>
          <div className='vendedorTempoMedio'>
            <span className='vendedorTituloPrincipal'>tempo médio de envio</span> <br/>
            <span className='vendedorSubTituloTempoMedio'>{ this.state.vendedor.tempoMedio }</span> <br/>
          </div>
        </div>
        <div className='vendedorVenda'>
          <span className='vendedorTituloPrincipal vendedorTituloVenda'>à venda</span> <br/>
          <span className='vendedorSubTituloVenda'>205</span>
        </div>
        <div className='vendedorVendidos'>
          <span className='vendedorTituloPrincipal vendedorTituloVendidos'>vendidos</span> <br/>
          <span className='vendedorSubTituloVendidos'>{ this.state.vendedor.qtdItensVendidos }</span> 
        </div>
        <div className='vendedorCriacao'>
          <span className='vendedorTituloPrincipal vendedorTituloCriacao'>no enjoi desde</span> <br/>
          <span className='vendedorSubTituloCriacao'>{ this.formatar( this.state.vendedor.criacao ) }</span>
        </div>
      </div>
      <div className='listagemVendedor'>
        <ul>
          <li>comprar no enjoei é seguro</li>
          <li>se não gostar, você pode devolver. entenda</li>
        </ul>
      </div>
    </div>      
    )
  }
};
