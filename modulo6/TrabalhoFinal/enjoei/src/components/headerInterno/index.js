import React from 'react'
import './headerInterno.css';

import ConteudoHeader from '../conteudoHeader';

export default function Header() {
  return (
    <div>
      <header>
        <div className='wrapper'>
          <div className='display'>
            <p><span className='infInterno'> 🚒 frete grátis na primeira compra </span> -promo válida no frete padrão até R$ 20 </p> 
          </div>
        </div>
      </header>
      <ConteudoHeader/>
    </div>
  )
}
