import React, { useState } from 'react';
import icone from '../../images/icone.png';
import flechaVoltar from '../../images/flechaVoltar.png';
import { Link, useHistory } from 'react-router-dom';
import { fakeLogin } from '../../services/fakeLogin';
import { setUser } from '../../services/Login';
import NavLogin from '../navLogin';
import VerificarLogin from '../../helpers/verificarLogin';
import './loginPage.css';

export default function LoginPage() {

  const history = useHistory();
  const [ email, setEmail ] = useState( '' );
  const [ senha, setSenha ] = useState( '' );

  const login = () => {
    const user = fakeLogin();

    const verificarLogin = new VerificarLogin();
    
    if(user.email === email && user.senha === senha ){
      setUser( { email, senha } );
      history.push( '/' );
    } else if( verificarLogin.verificarLogin( email, senha ) === true ) {
      setUser( { email, senha } );
      history.push( '/' );
    }else {
      alert( 'email ou senha incorreta' );
      window.location.reload( false );
    }
  }

  return (
    <div>
      <div className='login'>
        <Link to='/'> <img src={ flechaVoltar } alt='voltar' className='flechaVoltar'/> </Link> 
        <Link to='/'> <img src={ icone } alt='logo' className='logoLogin'/> </Link>  
        <hr className='hrLogo'/>
        <h2 className='tituloLogin'>venda o que te enjoa, compre de gente boa.</h2>
        <a href='https://www.facebook.com/login/'>
          <button className='buttonFacebook'>Entrar com o facebook</button>
        </a>
        <hr/>

        <div className='login-group'>
          <label className='login-label'>Email </label>
          <input onChange={ ( e ) => setEmail( e.target.value ) } required type='email' name='email' placeholder='email' autoComplete='off'/>
          <label className='senha-label'>senha </label>
          <input onChange={ ( e ) => setSenha( e.target.value ) } minLength='6' required type='password' name='senha' placeholder='senha'/>
        </div>

        <div>
          <label className='checkboxLabel'> <input type='checkbox' className='checkbox'/> <p className='manterLogado'>continuar conectado</p> </label>
          <span> <a href='https://consultaremedios.com.br/sistema-nervoso-central/alzheimer/c'className='esqueciAsenha' target='_blank'>  Esqueceu a senha? </a></span> <br/>
        </div>
      
        <button onClick={ login } className='buttonEjoei'>Entrar</button>
        <Link to='/cadastro'> <h4 className='naoTenhoConta'> não tenho conta </h4> </Link>
        <NavLogin/>
      </div>
    </div>      
  )
};
