import React from 'react'
import { Link } from 'react-router-dom';

import lupa from '../../images/lupa.png';
import icone from '../../images/icone.png';
import question from '../../images/question.png';
import { getUser, delUser } from '../../services/Login';

export default function ConteudoHeader() {
  return (
    <div>
      <div className='containerHeader'>
        <div className='header2 nav-wrapper'>
          <Link to='/'> <img src={ icone } alt='logo' className='logoHeader'/> </Link>
          <input className='inputBuscar' type='text' placeholder='buscar'></input>
          <img src={ lupa } alt='lupa' className='lupaHeader'/>
            <label for='menu-check' className='menu-button'>Abrir menu</label>
            <input type='checkbox' id='menu-check'/>
            <nav className='navHeader2'>
              <ul>
                <li> moças </li>
                <li> rapazes </li>
                <li> kids </li>
                <li> casalEtal </li>
                <img src={ question } alt='Foto da questão' className='question'/>
                {
                  getUser() ?
                  <li onClick={ delUser }>sair</li>:
                  <Link to='/login'> <li className='btnEntrar'>entrar</li> </Link> 
                }
                <li>
                    <button className='btnVender'>quero vender</button>
                </li>
              </ul>
            </nav>
          </div>
        </div>
      <hr className='hrHeader'/>
    </div>
  )
}