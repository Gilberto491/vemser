import React, { Component } from 'react';
import icone from '../../images/icone.png';
import { Link } from 'react-router-dom';
import flechaVoltar from '../../images/flechaVoltar.png';
import './cadastroPage.css';
import Formulario from '../formulario';

export default class CadastroPage extends Component {

  render() {
    return (
      <div>
        <div className='login'>
          <Link to='/login'> <img src={ flechaVoltar } alt='voltar' className='flechaVoltar'/> </Link> 
          <Link to='/'> <img src={ icone } alt='logo' className='logoLogin'/> </Link>  
          <hr className='hrLogo'/>
          <h2 className='tituloLogin'> crie sua conta no enjoi e receba novidades </h2>
          <hr/>
          <Formulario/>
          <hr/>
          <a href='https://pt-br.facebook.com/r.php'>
            <button className='buttonFacebook'> Cadastrar com o facebook </button>
          </a>
         </div>
      </div>        
    )
  }
};
