import React, { Component } from 'react';

export default class VerificarLogin extends Component {
  
  constructor( props ) {
    super( props );
    this.state = {
      usuario: []
    }
  }
  
  componentDidMount() {
   this.loadApiUsuarios();
  } 
  
  loadApiUsuarios() {
    let url = 'http://localhost:9000/api/usuarios';
      fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
         this.setState( { usuario: json } );
    } );
  }

  verificarLogin( email, senha ) {
    var verificar = false;
    this.state.usuario.map( ( usuarios ) => {
      console.log( usuarios.email );
      if( usuarios.email === email && usuarios.senha === senha ) {
        verificar = true;
      }
    } )
    return verificar;
  }
};
