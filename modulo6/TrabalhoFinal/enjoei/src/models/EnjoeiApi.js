import axios from 'axios';

const url = 'http://localhost:9000/api/';

const _get = url => new Promise( ( resolve, reject ) => axios.get( url ).then( response => resolve( response.data ) ) );
const _post = ( url, dados ) => new Promise( ( resolve, reject ) => axios.post( url, dados ).then( response => resolve( response.data ) ) );

export default class EpisodiosApi {
  
  async buscar() {
    return await _get( `${ url }produtos` );
  }

  async buscarTodosDetalhes() {
    return await _get( `${ url }detalhes` );
  }

  async buscarProduto( id ) {
    const response = await _get( `${ url }produtos?id=${ id }` );
    return response[ 0 ];
  }

  buscarUsuario() {
    return _get( `${ url }usuarios` );
  }

  buscarComentarios() {
    return _get( `${ url }comentarios` );
  }

  async registrarComentario( { comentario, produtoId } ) {
    const response = await _post( `${ url }comentarios`, { comentario, produtoId } );
    return response[ 0 ];
  }

  async registrarUsuario( { email, senha } ) {
    const response = await _post( `${ url }usuarios`, { email, senha } );
    return response[ 0 ];
  }
}
