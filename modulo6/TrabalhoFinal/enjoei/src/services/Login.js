export const getUser = () => {
  try {
      const user = localStorage.getItem( 'user' );
      return JSON.parse( user );
  } catch ( error ) {
      return null;
  }
}

export const setUser = ( user ) => {
  const value = JSON.stringify( user );
  localStorage.setItem('user', value);
} 

export const delUser = ( ) => {
  window.location.reload( false );
  localStorage.removeItem( 'user' );
}
