import React from 'react';
import { Header, Banner, Vendedores, Footer, Formatacao1index, Formatacao2index } from '../../components';

import './css/home.css';
import './css/fileira1.css';
import './css/fileira2.css';
import './css/fileira3.css';
import './css/fileira4.css';

export default function Home() {
  return ( 
    <div>
      <Header/>
      <div className='container'>
        <Banner/>
        <Formatacao1index/>
      <div className='espacamentoCategoriaProduto'>
        <Vendedores />
      </div>
        <Formatacao2index/>
        <Footer/>
      </div>
    </div> 
  )
};
