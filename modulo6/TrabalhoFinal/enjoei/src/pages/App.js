import React, { Component } from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import Home from './home';
import Detalhes from './detalhes';
import Login from './login';
import Cadastro from './cadastro';
import Erro from './erro';

import { PrivateRoute } from '../components/rotaPrivada';

export default class App extends Component {
  render() {
    return ( 
    <div className="App">
      <Router>
        <Switch>
          <Route path = '/' exact component = { Home } /> 
          <Route path = '/login' exact component = { Login }/>
          <Route path = '/cadastro' exact component = { Cadastro }/>  
          <PrivateRoute path = '/detalhes/:id' exact component = { Detalhes } />
          <Route path = '*' component = { Erro }/>
        </Switch>
      </Router>
    </div>
    )
  };
}