import React, { Component } from 'react';
import EnjoiApi from '../../models/EnjoeiApi';
import icone from '../../images/icone.png';
import { Link } from 'react-router-dom';
import etiqueta from '../../images/etiqueta.png';
import etiquetaPreco from '../../images/etiquetaPreco.png';
import { HeaderInterno, Footer, VendedorInterno } from '../../components';

import './css/grid.css';
import './css/detalhes.css';
import './css/comentarios.css';
import './css/containers.css';
import './css/titles-detalhes.css';

export default class Detalhes extends Component {
  
  constructor( props ) {
    super( props );
    this.state = {
      comentarioEspecifico: '',
      produtoId: '',
      comentario: '',
      comentarios: [],
      produtosNav: [],
      produto: [],
      produtos: [],
      perguntas: '',
      listaPerguntas: []
    }

    this.addPerguntas = this.addPergunta.bind( this );
    this.atualizar = this.atualizar.bind( this );
    this.handler = this.handler.bind( this );
    this.enjoiApi  = new EnjoiApi();
    this.comentar = this.comentar.bind( this );
  }
  
  handler = ( e ) => {
    e.preventDefault();
    this.enjoiApi.registrarComentario( { ...this.state.comentario, ...this.state.produtoId } );
    window.location.reload( false );
  }

  loadApiDetalhes() {
    const { id } = this.props.match.params;
    let url = `http://localhost:9000/api/detalhes/${ id }`;
      fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
         this.setState( { produtos: json } )
    } );
  }

  loadApiProdutos() {
    const { id } = this.props.match.params;
    let url = `http://localhost:9000/api/produtos/${ id }`;
      fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
          this.setState( { produto: json } );
    } );
  }

  loadApiProdutosNav() {
    let url = `http://localhost:9000/api/produtos`;
      fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
          this.setState( { produtosNav: json } );
    } );
  }

  loadApiComentarios() {
    const { id } = this.props.match.params;
    let url = `http://localhost:9000/api/comentarios`;
    fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
          this.setState( { comentarios: json } );
    } );
  }

  loadApiComentariosEspecificos() {
    const { id } = this.props.match.params;
    let url = `http://localhost:9000/api/comentarios/${ id }`;
    fetch( url )
        .then( ( r ) => r.json() )
        .then( ( json ) => {
          this.setState( { comentarioEspecifico: json } );
    } );
  }

  componentDidMount() {
    this.loadApiDetalhes();
    this.loadApiProdutos();
    this.loadApiProdutosNav();
    this.loadApiComentarios();
    this.loadApiComentariosEspecificos();
  } 
  
  atualizar = () => {
    setTimeout(() => {
      window.location.reload( false );
    }, 1 );
  }

  comentar = ( comentario, produtoId ) => {
    this.setState( { comentario: comentario, produtoId: produtoId } );
  }


  addPergunta = ( e ) => {
    let state = this.state;
    if( this._perguntasInput.value !== '' ) {
      let newPergunta = {
        text: this._perguntasInput.value,
        key: Date.now()
      };
      this.setState( { listaPerguntas: [ ...state.listaPerguntas, newPergunta ] } );
    }
    e.preventDefault();
    this.setState( { perguntas: ' ' } );
    this.setState( { _perguntasInput: ' ' } )
  }
  
  render() {
    return ( 
      <div>
      <HeaderInterno/>
        <div className='container'>
          <div className='col col-sm-12 col-md-12 col-lg-6'>
          <div className='zoom'>
            <img src={ this.state.produto.imagem } alt='imagem' className='imgDetalhes'/> <br />
            <img src={ this.state.produto.imagem } alt='imagem' className='imgDetalhesPequena'/>
          </div>
            <hr className='hrDetalhes'/> <br />
            <form onSubmit={ this.handler }>
              <input className='inputPergunta' type='text' onChange={ ( e ) => this.comentar(  { comentario: e.target.value, produtoId: this.state.produtos.id  }  ) } placeholder='pergunte ao vendedor' name='pergunta' ref={ ( event ) => this._perguntasInput = event }/>
              <button className='submitPergunta' type='submit'>Perguntar</button> <br />
            </form>
            <h2>últimas perguntas</h2> 
              <div className='containerListagemPerguntas'> 
                <ul className='ulListagem'>
                  { this.state.comentarios.map( ( comentarioEspecifico ) => {
                    if( comentarioEspecifico.produtoId === this.state.produtos.id  ) {
                      return (
                        <div className='containerPergunta'>
                          <img className='iconePergunta' src={ icone }/>
                          <span> { comentarioEspecifico.comentario } </span>
                        </div>
                      );
                    }           
                  } ) } 
                </ul> 
              </div> 
          </div>


        <div className='col col-sm-12 col-md-12 col-lg-6'>
        <span className='titleMenu'>moças / roupas / vestidos</span>
          <h2 className='titleProduto'> { this.state.produto.nome }</h2>
          <p className='titleMarca'> { this.state.produtos.marca } | <span>  seguir marca </span> </p>
          <h2 className='titlePreco'>R$ { this.state.produto.preco }</h2>
          <img className='etiquetaPreco' src={ etiquetaPreco }/>
          <button className='btnTitle primary'>eu quero</button>
          <button className='btnTitle secondary'>adicionar à sacolinha</button>
          <button className='btnTitle secondary'>fazer oferta</button>
          
          <div className='containerInf'>
            <div className='titleCenter'>
              <h3 className='titleInf '>tamanho</h3>
              <span>M</span>
            </div>
            <hr className='hrInf'/>
            <div className='containerMarca'>
              <h3 className='titleInf'>marca</h3> 
              <h4 className='subtituloInf'>{ this.state.produtos.marca }</h4>
            </div>
            <div className='containerCondicao'>
              <h3 className='titleInf'>condição</h3>
              <h4 className='subtituloInf'>{ this.state.produtos.condicao }</h4>
            </div>
            <div className='containerCodigo'>
              <h3 className='titleInf'>código</h3>
              <h4 className='subtituloInf'>{ this.state.produtos.codigo }</h4>
            </div>
          </div>
            <h3 className='titleInf desc'>descrição</h3>
        <VendedorInterno/>
      </div>    

          <h3 className='maisProdutos' >Leve mais produtos dessa loja e aproveite o <u> frete único </u> na sacolinha <u className='verMais'> ver mais ⇨</u> </h3>
                                  
          <div className=' lista-loja5'>
            {
              this.state.produtosNav.map( ( produto ) => {
                if( produto.id >= 1 && produto.id <=5  ){
                  return (
                  <article>
                      <Link to={ `/detalhes/${ produto.id }` } onClick={ this.atualizar }> <img key={ produto.id } src={ produto.imagem } alt='imagem' className='imgHome'/> </Link>
                        <img className='etiquetaProdutos'  src={ etiqueta }/>
                      <Link to={  `/detalhes/${ produto.id }` } onClick={ this.atualizar }> <span className='precoDetalhes'>R$ { produto.preco }</span> </Link> <br/>
                      <span className='nomeDetalhes'>{ produto.nome } ☝</span>
                  </article>
                  )
                } 
              } )
            }
          </div>

          <div> <br/> 
            <h3 className='maisProdutos' >você também vai curtir </h3>
          </div>
            
          <div className=' lista-loja5'>
            {
              this.state.produtosNav.map( ( produto ) => {
                if( produto.id >= 1 && produto.id <=5  ){
                  return (
                  <article>
                      <Link to={ `/detalhes/${ produto.id }` } onClick={ this.atualizar }> <img key={ produto.id } src={ produto.imagem } alt='imagem' className='imgHome'/> </Link> 
                        <img className='etiquetaProdutos'  src={ etiqueta }/>
                      <Link to={ `/detalhes/${ produto.id }` } onClick={ this.atualizar }> <span className='precoDetalhes'>R$ { produto.preco }</span> </Link> <br/>
                      <span className='nomeDetalhes'>{ produto.nome } ☝</span>
                  </article>
                  )
                } 
              } )
            }
          </div> <br/>
          <Footer/>
         </div>
      </div>      
    )
  }
};
