import React, { Component } from 'react';
import { EpisodiosApi, ListaEpisodios } from '../../models';
import './todosEpisodios.css';

import { BotaoUi, CampoBusca } from '../../components';

export default class TodosEpisodios extends Component {

  constructor( props ) {
    super( props );
    this.episodiosApi = new EpisodiosApi();
    this.state = {
      listaEpisodios: [],
      filmes: []
    }
    this.loadFilmes = this.loadFilmes.bind( this );
  }

  componentDidMount() {
    this.loadFilmes();
    const requisicoes = [
      this.episodiosApi.buscar(),
      this.episodiosApi.buscarTodosDetalhes(),
      this.episodiosApi.buscarTodasNotas()
    ];

    

    Promise.all( requisicoes )
      .then( respostas => {
        let listaEpisodios = new ListaEpisodios( respostas[0], respostas[2], respostas[1] );
        this.setState( state => {
          return {
            ...state,
            listaEpisodios: listaEpisodios._todos.concat([])
          }
        })
      })
  }

  ///////////////////////////////////

  loadFilmes() {
    let url = 'http://localhost:9000/api/episodios';
    fetch( url )
      .then( r => r.json() )
      .then( json => {
        this.setState( { filmes: json } );
        console.log(json)
      } );
    }

    ///////////////////////////////////

  filtrarPorTermo = evt => {
    const termo = evt.target.value;
    this.episodiosApi.filtrarPorTermo( termo )
      .then( resultados => {
        this.setState({
          filmes: this.state.filmes.filter( e => resultados.some( r => r.episodioId === e.id ) )
        })
      } )
  }

  linha( item, i ) {
    return <BotaoUi key={ i } classe={ item.cor } nome={ item.nome } metodo={ item.metodo } />
  }

  

  render() {
    const { listaEpisodios } = this.state;

    return (
      <React.Fragment>
        <header className="App-header">
          <h1>Todos Episódios</h1>
          <div className="inputEpisodios">
          <CampoBusca atualizaValor={ this.filtrarPorTermo } placeholder="Ex.: ministro" />
          </div>
          {this.state.filmes.map( filme => {
            return (
              <article>
                <img src={ filme.thumbUrl } alt='filme'/> 
                <h3> { filme.nome } </h3>
                <p> Temporada: { filme.temporada } </p>
                <p> Episódio: { filme.ordemEpisodio } </p>
               <button type='button'>Acessar</button>
              </article>
            )
          } ) }
          <BotaoUi link="/" nome="Página Inicial" />
        </header>
      </React.Fragment>
    )
  }

}