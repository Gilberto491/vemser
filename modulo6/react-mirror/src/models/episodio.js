import EpisodiosApi from "./episodiosApi";

export default class Episodio {
  constructor( id, nome, duracao, temporada, ordemEpisodio, thumbUrl, nota, detalhe ) {
    this.id = id;
    this.nome = nome;
    this.duracao = duracao;
    this.temporada = temporada;
    this.ordem = ordemEpisodio;
    this.url = thumbUrl;
    this.quantidade = 0;
    this.episodioApi = new EpisodiosApi();
    this.nota = ( nota || {} ).nota;
    this.dataEstreia = ( detalhe || {} ).dataEstreia;
  }

  get duracaoEmMin() {
    return `${ this.duracao } min`;
  }

  get temporadaEpisodio() {
    return `${ this.temporada.toString().padStart( 2,0 ) }/${ this.ordem.toString().padStart( 2,0 ) }`
  }

  avaliar( nota ) {
    this.nota = parseInt( nota );
    return this.episodioApi.registrarNota( { nota: this.nota, episodioId: this.id } );
  }

  marcarComoNaoAssistido() {
    this.assistido = false;
    this.quantidade = 0;
  }

  marcarComoAssistido() {
    this.assistido = true;
    this.quantidade++;
  }

  validarNota( nota ) {
    return ( nota >= 1 && nota <= 5 );
  }
}