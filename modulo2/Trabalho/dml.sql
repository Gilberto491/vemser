CREATE SEQUENCE BANCOS_SEQ
    START WITH 1
    INCREMENT BY 1
    NOCYCLE;

CREATE SEQUENCE ENDERECO_SEQ;
CREATE SEQUENCE CONSOLIDACAO_SEQ;
CREATE SEQUENCE TIPOS_DE_CONTAS_SEQ;
CREATE SEQUENCE CONTA_SEQ;
CREATE SEQUENCE PESSOAS_SEQ;
CREATE SEQUENCE AGENCIAS_SEQ;
CREATE SEQUENCE CLIENTES_SEQ;

--EXERCICIO O5
    
/*BANCO ALFA*/
INSERT INTO BANCOS (ID_BANCOS, NOME,  CODIGO) VALUES (BANCOS_SEQ.NEXTVAL, 'BANCO Alfa', '011');
INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '0001', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) VALUES (ENDERECO_SEQ.NEXTVAL,'Web - Rua Testando','n 55','loja 1',' Bairro NA','Cidade NA','Estado NA','Pais Brasil');

INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '0002', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) 
VALUES (ENDERECO_SEQ.NEXTVAL,'Rua Testing','122','Casa','Between Hyde and Powell Streets','San Francisco','California','EUA');

INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '0101', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) VALUES
(ENDERECO_SEQ.NEXTVAL,'Londres - Rua Tesing','525','loja 1','Croydon','Londres','Boroughs','England');
/**/

/*BANCO BETA*/
INSERT INTO BANCOS (ID_BANCOS, NOME,  CODIGO) VALUES (BANCOS_SEQ.NEXTVAL, 'BANCO Beta', '241');
INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '0001', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) VALUES (ENDERECO_SEQ.NEXTVAL,'Web - Rua Testando','n 55','loja 2',' Bairro NA','Cidade NA','Estado NA','Pais Brasil');
/**/

/*BANCO OMEGA*/
INSERT INTO BANCOS (ID_BANCOS, NOME,  CODIGO) VALUES (BANCOS_SEQ.NEXTVAL, 'BANCO Omega', '307');
INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '0001', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) VALUES (ENDERECO_SEQ.NEXTVAL,'Web - Rua Testando','n 55','loja 3',' Bairro NA','Cidade NA','Estado NA','Pais Brasil');

INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '8761', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) 
VALUES (ENDERECO_SEQ.NEXTVAL, 'Itu - Rua do meio', 'n 2233', '', 'Bairro Qualquer', 'Cidade Itu', 'Estado São Paulo', 'Pais Brasil');

INSERT INTO AGENCIAS(ID_AGENCIAS, CODIGO, NOME) VALUES (AGENCIAS_SEQ.NEXTVAL, '4567', 'Agências');
INSERT INTO CONSOLIDACAO(ID_CONSOLIDACAO, SALDO_ATUAL, SAQUES, DEPOISTOS, NUMERO_DE_CORRENTISTAS) VALUES (CONSOLIDACAO_SEQ.NEXTVAL, 0.0, 0.0, 0.0, 0);
INSERT INTO ENDERECO(ID_ENDERECO,LOGRADOURO,NUMERO,COMPLEMENTO,BAIRRO,CIDADE,ESTADO,PAIS) 
VALUES (ENDERECO_SEQ.NEXTVAL, 'Hermana - Rua da boca', 'n 222', '', 'Bairro caminito', 'Cidade Buenos Aires', 'Estado Buenos Aires', 'Pais Argentina');
/**/

--EXERCICIO 06

INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE1', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE2', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE3', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE4', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE5', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE6', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE7', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE8', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, 'CLIENTE9', '', '', '');
INSERT INTO CLIENTES(ID_CLIENTES, NOME, CPF, ESTADO_CIVIL, DATA_NASCIMENTO, ENCERECO) VALUES (CLIENTES_SEQ.NEXTVAL, '1CLIENTE0', '', '', '');

--INSERT INTO CONTA(ID_CONTA, DEBITO, CREDITO

--INSERT INT CONTA_X_CLIENTES (ID_CONTA, ID CLIENTES) VALUES ()

/*EXERCICIO NÃO COMPLETO*/





