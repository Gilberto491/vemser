import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoVerdeTest{
    
    @Test
    public void experienciaEmDobro(){
        ElfoVerde elfoverde = new ElfoVerde("elfo");
        Dwarf anaoQualquer = new Dwarf ("anao");
        elfoverde.atirarFlecha(anaoQualquer);
        
        assertEquals(2, elfoverde.getExperiencia());
    }
    
    @Test
    public void experienciaEmDobroComMaisTentativas(){
        ElfoVerde elfoverde = new ElfoVerde("elfo");
        Dwarf anaoQualquer = new Dwarf ("anao");
        elfoverde.atirarFlecha(anaoQualquer);
        elfoverde.atirarFlecha(anaoQualquer);
       
        assertEquals(4, elfoverde.getExperiencia());
    }
    
    @Test
    public void adicionarItemComDescricaoValida(){
        Inventario inventario;
        ElfoVerde elfoverde = new ElfoVerde("elfo");
        Item arcoDeVidro = new Item(1, "Espada de aço valiriano");
        elfoverde.ganharItem(arcoDeVidro);
        //posiçao 0 e 1 referente ao arco e flecha
        
        assertEquals(arcoDeVidro, elfoverde.inventario.getItem(2));
    }
    
    @Test
    public void adicionarItemComDescricaoInvalida(){
        ElfoVerde elfoverde = new ElfoVerde("elfo");
        Item arcoForjado = new Item(1, "Espada de aço fojada");
        elfoverde.ganharItem(arcoForjado);
        Inventario inventario = elfoverde.getInventario();
        
        assertNull(inventario.buscarString("Espada de aço fojada"));
    }
    
    @Test
    public void perderItemComDescricaoValida(){
        ElfoVerde elfoverde = new ElfoVerde("elfo");
        Item arcoDeVidro = new Item(1, "Arco de Vidro");
        elfoverde.ganharItem(arcoDeVidro);
        
        Inventario inventario = elfoverde.getInventario();
        assertEquals( new Item(1, "Arco"), inventario.getItem(0));
        assertEquals( new Item(2, "Flechas"), inventario.getItem(1));
        assertEquals( arcoDeVidro, inventario.getItem(2) );
        
        elfoverde.perderItem(arcoDeVidro);
        assertNull( inventario.buscarString("Arco de Vidro") );
    }
    
    @Test
    public void perderItemComDescricaoInvalida(){
        ElfoVerde elfoverde = new ElfoVerde("elfo");
        Item arcoForjado = new Item(1, "Arco Forjado");
        elfoverde.perderItem(arcoForjado);
        
        Inventario inventario = elfoverde.getInventario();
        assertEquals( new Item(1, "Arco"), inventario.getItem(0));
        assertEquals( new Item(2, "Flechas"), inventario.getItem(1));
        
    }
}