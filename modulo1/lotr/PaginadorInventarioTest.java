import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class PaginadorInventarioTest{
   
    @Test
    public void pularLimitarComApenasUmItem(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        inventario.setItem(espada);
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(1);
        
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(1, primeiraPagina.size());
    
    }
    
    @Test
    public void paginadorComDuasRequisicoesDentroDosLimites(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item lanca = new Item(10, "Lança");
        
        inventario.setItem(espada);
        inventario.setItem(escudo);
        inventario.setItem(lanca);
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(2);
        
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudo, primeiraPagina.get(1));
        assertEquals(2, primeiraPagina.size());
        
        assertEquals(lanca, segundaPagina.get(0));
        assertEquals(1, segundaPagina.size());
    } 
    
     @Test
    public void paginadorComDuasRequisicoesForaDosLimites(){
        Inventario inventario = new Inventario();
        Item espada = new Item(1, "Espada");
        Item escudo = new Item(2, "Escudo");
        Item lanca = new Item(10, "Lança");
        
        inventario.setItem(espada);
        inventario.setItem(escudo);
        inventario.setItem(lanca);
        
        PaginadorInventario paginador = new PaginadorInventario(inventario);
        paginador.pular(0);
        ArrayList<Item> primeiraPagina = paginador.limitar(2);
        paginador.pular(2);
        ArrayList<Item> segundaPagina = paginador.limitar(1000);
        
        assertEquals(espada, primeiraPagina.get(0));
        assertEquals(escudo, primeiraPagina.get(1));
        assertEquals(2, primeiraPagina.size());
        
        assertEquals(lanca, segundaPagina.get(0));
        assertEquals(1, segundaPagina.size());
    } 
}