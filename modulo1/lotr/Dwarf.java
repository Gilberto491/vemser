public class Dwarf extends Personagem{
    
   private boolean equipado;
    
   {  
       this.equipado = false;
       this.qtdDano = 10.0;
   }
   
   public Dwarf(String nome){
       super(nome);
       this.vida = 110.0;
       Item escudo = new Item(1, "Escudo");
       this.inventario.setItem(escudo);
   }
   
   @Override
   protected double calcularDano(){
     return this.equipado ? 5.0 : this.qtdDano; 
   }
    
   public Status equiparItem(){
       this.equipado = true;
       return this.status = Status.ESTA_EQUIPADO;
   }
   
   public String imprimirNomeDaClasse(){
       return "Dwarf"; 
   }
}