import java.util.*;

public class Exercito {
  
    private ArrayList <Elfo> exercito;
    private HashMap<Status, ArrayList<Elfo>> porStatus;
    private final static ArrayList<Class> ATACANTES_VALIDOS = new 
    ArrayList(Arrays.asList(
       ElfoVerde.class,
       ElfoNoturno.class
    ));;
    
    {
      this.exercito = new ArrayList<>();
      this.porStatus = new HashMap<>();
    }
    
    public void alistarElfos(Elfo elfo){
        boolean podeAlistar = ATACANTES_VALIDOS.contains(elfo.getClass());
        if(podeAlistar){
            this.exercito.add(elfo);
            ArrayList<Elfo> elfosDeUmStatus = porStatus.get(elfo.getStatus());
            if( elfosDeUmStatus == null) {
              elfosDeUmStatus = new ArrayList<>();
              porStatus.put( elfo.getStatus(), elfosDeUmStatus);
            }
            elfosDeUmStatus.add(elfo);
        }
    }
    
    public ArrayList<Elfo> listaDeElfosAlistados(){
       return this.exercito;
    }
    
    public ArrayList<Elfo> buscarListaPorStatus(Status status){
       return this.porStatus.get(status);
    }
    
    public int quantidadeElfosAlistados(){
       return exercito.size();
    }
}