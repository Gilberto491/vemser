import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoNoturnoTest{
   @Test 
   public void aumentarXpTresVezesMais(){
       ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo noturno");
       Dwarf anaoQualquer = new Dwarf("anao maligno");
       elfoNoturno.atirarFlecha(anaoQualquer);
       
       assertEquals(3, elfoNoturno.getExperiencia());
   }
   
   @Test 
   public void aumentarXpTresVezesMaisComMaisTentativas(){
       ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo noturno");
       Dwarf anaoQualquer = new Dwarf("anao maligno");
       elfoNoturno.atirarFlecha(anaoQualquer);
       elfoNoturno.atirarFlecha(anaoQualquer);
       
       assertEquals(6, elfoNoturno.getExperiencia());
   }
   
   @Test 
   public void perderQuizeUnidadesDeVida(){
       ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo noturno");
       Dwarf anaoQualquer = new Dwarf("anao maligno");
       elfoNoturno.atirarFlecha(anaoQualquer);
       
       assertEquals(85.0, elfoNoturno.getVida(), .00001);
   }
   
   @Test
   public void perderTrintaUnidadesDeVida(){
       ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo noturno");
       Dwarf anaoQualquer = new Dwarf("anao maligno");
       elfoNoturno.atirarFlecha(anaoQualquer);
       elfoNoturno.atirarFlecha(anaoQualquer);
       
       assertEquals(70.0, elfoNoturno.getVida(), .00001);
   }
   
    @Test
    public void elfoNoturnoAtira7FlechasEMorre(){
        ElfoNoturno elfoNoturno = new ElfoNoturno("Elfo noturno");
        elfoNoturno.getInventario().getItem(1).setQuantidade(1000);
        
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        elfoNoturno.atirarFlecha(new Dwarf("anao"));
        
        
        assertEquals( .0, elfoNoturno.getVida(), 1e-9 );
        assertEquals( Status.MORTO, elfoNoturno.getStatus() );
    }
}
