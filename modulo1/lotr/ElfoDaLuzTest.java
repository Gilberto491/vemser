import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ElfoDaLuzTest{
    
   @Test
   public void nascendoComEspada(){
     Elfo elfoQualquer = new ElfoDaLuz("elfo");
     ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
        new Item(1, "Arco"),
        new Item(2, "Flechas"),
        new Item(1, "Espada de Galvorn")
     ));
     
     assertEquals(esperado, elfoQualquer.getInventario().getItens());
   }
    
   @Test
   public void atacandoComEspadaApenasUmaVezPerdendoVida(){
     ElfoDaLuz elfoQualquer = new ElfoDaLuz("Elfo da Luz");
     Dwarf dwarf = new Dwarf("dwarf");
     elfoQualquer.atacarComEspada(dwarf);
     
     assertEquals(79.0, elfoQualquer.getVida(), .0001);
   }
   
   @Test
   public void atacandoComEspadaQuantidadeParGanhandoVida(){
     ElfoDaLuz elfoQualquer = new ElfoDaLuz("Elfo da Luz");
     Dwarf dwarf = new Dwarf("dwarf");
     elfoQualquer.atacarComEspada(dwarf);
     elfoQualquer.atacarComEspada(dwarf);
     
     assertEquals(89.0, elfoQualquer.getVida(), .0001);
   }
   
   @Test
   public void atacandoComEspadaQuantidadeImparPerdendoVida(){
     ElfoDaLuz elfoQualquer = new ElfoDaLuz("Elfo da Luz");
     Dwarf dwarf = new Dwarf("dwarf");
     elfoQualquer.atacarComEspada(dwarf);
     elfoQualquer.atacarComEspada(dwarf);
     elfoQualquer.atacarComEspada(dwarf);
     
     assertEquals(68.0, elfoQualquer.getVida(), .0001);
   }
   
   @Test
   public void morrendoAoAtacarMulplasVezes() {
     ElfoDaLuz elfoQualquer = new ElfoDaLuz("elfo");
     Dwarf dwarf = new Dwarf("anao");
     for (int i = 0; i < 17; i++) {
       elfoQualquer.atacarComEspada(dwarf);
     }
     
     assertEquals(0, elfoQualquer.getVida(), .0001);
     assertEquals(0, dwarf.getVida(), 0001);
     assertEquals(Status.MORTO, elfoQualquer.getStatus());
     assertEquals(Status.MORTO, dwarf.getStatus());
   }
   
   @Test
   public void naoPerdeEspada(){
     Elfo elfoQualquer = new ElfoDaLuz("elfo");
     elfoQualquer.perderItem(new Item(1, "Espada de Galvorn"));
     ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
        new Item(1, "Arco"),
        new Item(2, "Flechas"),
        new Item(1, "Espada de Galvorn")
     ));
     
     assertEquals(esperado, elfoQualquer.getInventario().getItens());
   }
   
   @Test
    public void podePerderArco() {
     Elfo elfoQualquer = new ElfoDaLuz("elfo");
     elfoQualquer.perderItem(new Item(1, "Arco"));
     ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(
        new Item(2, "Flechas"),
        new Item(1, "Espada de Galvorn")
     ));
                
     assertEquals(esperado, elfoQualquer.getInventario().getItens());
   }
   
   @Test
   public void atacandoComUnidadeDeEspada() {
     ElfoDaLuz elfoQualquer = new ElfoDaLuz("elfo");
     elfoQualquer.getInventario().getItens().get(2).setQuantidade(0);
     elfoQualquer.atacarComEspada(new Dwarf("anao"));
     
     assertEquals(79, elfoQualquer.getVida(), .00001);
   }
}