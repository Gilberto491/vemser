import java.util.*;

public class ElfoVerde extends Elfo{
    
   private final ArrayList<String> descricoesValidas = new ArrayList<>(Arrays.asList(
   "Espada de aço valiriano",
   "Arco de Vidro",
   "Flecha de Vidro")); 
    
   public ElfoVerde(String nome){
       super(nome);
       this.qtdExperienciaPorAtaque = 2;
   }
   
   private boolean isDescricaoValida(String item){
     return descricoesValidas.contains(item);
   }
   
   @Override
   public void ganharItem(Item item){
      if(this.isDescricaoValida(item.getDescricao())){
          this.inventario.setItem(item);
      }
   }
   
   @Override
   public void perderItem(Item item){
      if(this.isDescricaoValida(item.getDescricao())){
         this.inventario.removeItem(item);
      }
   }
}