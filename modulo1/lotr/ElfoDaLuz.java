import java.util.*;

public class ElfoDaLuz extends Elfo{
    
   private final double QTD_VIDA_GANHA = 10; 
   private int qtdAtaques;
   
   {
     qtdAtaques = 0; 
   }
   
    private final ArrayList<String> DESCRICOES_OBRIGATORIAS = new ArrayList<>(
        Arrays.asList(
            "Espada de Galvorn"
        )    
    );
    
   public ElfoDaLuz(String nome){
    super(nome);
    this.qtdDano = 21;
    this.vida = 100.0;
    super.ganharItem(new ItemSempreExistente(1, DESCRICOES_OBRIGATORIAS.get(0)));
   }
   
   private boolean parOuImpar(){
     return qtdAtaques % 2 ==1;
   }
    
   private void ganharVida() {
       vida += QTD_VIDA_GANHA;
   }
    
   @Override
   public void perderItem(Item item){
      boolean possoPerder = !DESCRICOES_OBRIGATORIAS.contains(item.getDescricao());
      if(possoPerder){
         super.perderItem(item);  
      }
   }
   
   public void atacarComEspada(Dwarf dwarf){
     qtdAtaques++;
     dwarf.sofrerDano();
     if( parOuImpar() ) {
       this.sofrerDano();
     }else{ 
       this.ganharVida();
     }
   }
}