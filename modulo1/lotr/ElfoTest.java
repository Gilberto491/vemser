import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ElfoTest {
    
    @After
    public void tearDown(){
        System.gc();
    }

    @Test
    public void elfoDeveNascerComDuasFlechas() {
        Elfo elfoQualquer = new Elfo("Legolas");
        assertEquals(2, elfoQualquer.getQtdFlechas());
    }
    
    @Test
    public void atirarFlechaDevePerderFlechaAumentarXP() {
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anaoQualquer = new Dwarf ("anao");
        elfoQualquer.atirarFlecha(anaoQualquer);
        
        assertEquals(1, elfoQualquer.getQtdFlechas());
        assertEquals(1, elfoQualquer.getExperiencia());
    }
    
    @Test
    public void atirarMaisDeUmaFlechaAumentarXP(){
        Elfo elfoQualquer = new Elfo("Legolas");
        Dwarf anao1 = new Dwarf ("anao1");
        Dwarf anao2 = new Dwarf ("anao2");
        Dwarf anao3 = new Dwarf ("anao3");
        
        elfoQualquer.atirarFlecha(anao1);
        elfoQualquer.atirarFlecha(anao2);
        elfoQualquer.atirarFlecha(anao3);
        
        assertEquals(0, elfoQualquer.getQtdFlechas());
        assertEquals(2, elfoQualquer.getExperiencia());
    }
    
    @Test 
    public void atirarFlechaDwarfPerdeVida(){
        
       Elfo elfoQualquer = new Elfo("Legolas");
       Dwarf anaoQualquer = new Dwarf ("anao maligno");
    
       elfoQualquer.atirarFlecha(anaoQualquer);
       assertEquals(100.0, anaoQualquer.getVida(), .001);
    
    } 
    
    @Test
    public void criarUmElfoIncrementaContadorUmaVez(){
       new Elfo("Legolas");
       
       assertEquals(1, Elfo.getQtdElfos());
    }
    
    @Test
    public void criarDoisElfosIncrementaContadorUmaVez(){
       new Elfo("Legolas");
       new Elfo("Legolas");
       
       assertEquals(2, Elfo.getQtdElfos());
    }
}