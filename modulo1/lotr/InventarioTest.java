import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class InventarioTest{

  @Test
  public void adicionandoItem(){
      Inventario inventarioQualquer = new Inventario();
      Item adaga = new Item(1, "Adaga");
      inventarioQualquer.setItem(adaga);
      assertEquals(adaga, inventarioQualquer.getItens().get(0));
  }
    
  @Test
  public void adicionandoMaisDeUmItem(){
      Inventario inventarioQualquer = new Inventario();
      Item adaga = new Item(1,"Adaga");
      Item espada = new Item(1, "Espada");
      inventarioQualquer.setItem(adaga);
      inventarioQualquer.setItem(espada);
        
      assertEquals(adaga, inventarioQualquer.getItens().get(0));
      assertEquals(espada, inventarioQualquer.getItens().get(1));
    }
    
  @Test
  public void adicionandoItensObtendoItens(){
      Inventario inventarioQualquer = new Inventario();
      Item adaga = new Item(2,"adaga");
      inventarioQualquer.setItem(adaga);
      assertEquals(adaga, inventarioQualquer.getItem(0));
  }
    
  @Test
  public void adicionandoItemRemovendoItem(){
     Inventario inventarioQualquer = new Inventario();
     Item adaga = new Item(1,"Adaga");
     inventarioQualquer.setItem(adaga);
     //assertEquals(1, inventarioQualquer.getItens().size());
     inventarioQualquer.removeItem(0);
     assertEquals(0, inventarioQualquer.getItens().size());
  }
    
  @Test
  public void getDescricoesVariosItens(){
      Inventario inventarioQualquer = new Inventario();
      Item espada = new Item(1,"Espada");
      Item escudo = new Item(2,"Escudo");
      inventarioQualquer.setItem(espada);
      inventarioQualquer.setItem(escudo);
      String resultado = inventarioQualquer.getDescricao();
      assertEquals("Espada,Escudo", resultado);
  }
    
  @Test
  public void getItemMaiorQuantidade(){
      Inventario inventarioQualquer = new Inventario();
      Item lanca = new Item(1, "Lança");
      Item espada = new Item(5, "Espada");
      Item escudo = new Item(2, "Escudo");
      inventarioQualquer.setItem(lanca);
      inventarioQualquer.setItem(espada);
      inventarioQualquer.setItem(escudo);
      Item resultado = inventarioQualquer.getItemMaior();
      assertEquals(espada, resultado);
  }
  
  @Test
  public void buscaDescricaoPelaString(){
      Inventario inventarioQualquer = new Inventario();
      Item lanca = new Item(1, "Lança");
      inventarioQualquer.setItem(lanca);
      assertEquals(lanca, inventarioQualquer.buscarString("Lança"));
  }
  
  @Test
  public void verificarSeListaEstaInvertida(){
     Inventario inventarioQualquer = new Inventario();
     ArrayList<Item> inverter = new ArrayList<>();
    
     Item lanca = new Item(1, "Lança");
     Item espada = new Item(1, "Espada");
     inventarioQualquer.setItem(lanca);
     inventarioQualquer.setItem(espada);
     inverter.add(espada);
     inverter.add(lanca);
    
     assertEquals(inverter, inventarioQualquer.inverter());
  }
  
  @Test
  public void verificarSeListaEstaInvertidaComMaisItens(){
     Inventario inventarioQualquer = new Inventario();
     ArrayList<Item> inverter = new ArrayList<>();
    
     Item lanca = new Item(1, "Lança");
     Item espada = new Item(1, "Espada");
     Item armadura = new Item(1, "Armadura");
     inventarioQualquer.setItem(lanca);
     inventarioQualquer.setItem(espada);
     inventarioQualquer.setItem(armadura);
     
     inverter.add(armadura);
     inverter.add(espada);
     inverter.add(lanca);
    
     assertEquals(inverter, inventarioQualquer.inverter()); 
  }
  
  @Test
  public void ordenarComApenasUmItem(){
      Inventario inventario = new Inventario();
      Item espada = new Item(1, "Espada");
      inventario.setItem(espada);
      inventario.ordenarItens();
      ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada));
      
      assertEquals(esperado, inventario.getItens());
  }
  
  @Test
  public void ordenarComlistaDesordenada(){
      Inventario inventario = new Inventario();
      Item cafe = new Item(3, "Termica de cafe");
      Item escudo = new Item(2, "Escudo");
      Item espada = new Item(1, "Espada");
      
      inventario.setItem(cafe);
      inventario.setItem(escudo);
      inventario.setItem(espada);
      
      inventario.ordenarItens();
      ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(espada, escudo, cafe));
      assertEquals(esperado, inventario.getItens()); 
  }
  
  @Test
  public void ordenarComlistaOrdenada(){
      Inventario inventario = new Inventario();
      Item cafe = new Item(1, "Termica de cafe");
      Item escudo = new Item(2, "Escudo");
      Item espada = new Item(3, "Espada");
      
      inventario.setItem(cafe);
      inventario.setItem(escudo);
      inventario.setItem(espada);
      
      inventario.ordenarItens();
      ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(cafe, escudo, espada));
      assertEquals(esperado, inventario.getItens());
  }
  
  @Test
  public void ordenarComlistaDeItensIguais(){
      Inventario inventario = new Inventario();
      Item cafe = new Item(1, "Termica de cafe");
      Item escudo = new Item(1, "Escudo");
      Item espada = new Item(1, "Espada");
      
      inventario.setItem(cafe);
      inventario.setItem(escudo);
      inventario.setItem(espada);
      
      inventario.ordenarItens();
      ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(cafe, escudo, espada));
      assertEquals(esperado, inventario.getItens());
  }
  
  @Test 
  public void ordenarDescComApenasUmItem(){
    Inventario inventario = new Inventario();
    Item cafe = new Item(1, "Termica de cafe");
    inventario.setItem(cafe);
    
    inventario.ordenarItens(TipoOrdenacao.DESC);
    ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(cafe));
    assertEquals(esperado, inventario.getItens());
  }
  
  @Test 
  public void ordenarDescTotalmenteOrdenado(){
    Inventario inventario = new Inventario();
    Item cafe = new Item(3, "Termica de cafe");
    Item espada = new Item(2, "Espada");
    Item arco = new Item(1, "Arco");
    
    inventario.setItem(cafe);
    inventario.setItem(espada);
    inventario.setItem(arco);
    
    inventario.ordenarItens(TipoOrdenacao.DESC);
    ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(cafe, espada, arco));
    assertEquals(esperado, inventario.getItens());
  }
  
  @Test 
  public void ordenarDescTotalmenteDesordenado(){
    Inventario inventario = new Inventario();
    Item cafe = new Item(1, "Termica de cafe");
    Item espada = new Item(2, "Espada");
    Item arco = new Item(3, "Arco");
    
    inventario.setItem(cafe);
    inventario.setItem(espada);
    inventario.setItem(arco);
    
    inventario.ordenarItens(TipoOrdenacao.DESC);
    ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(arco, espada, cafe));
    assertEquals(esperado, inventario.getItens());
  }
  
  @Test
    public void ordernarDescQuantidadesIguais() {
        Inventario inventario = new Inventario();
        Item cafe = new Item(3, "Térmica de café");
        Item escudo = new Item(3, "Escudo");
        Item espada = new Item(3, "Espada");
        inventario.setItem(escudo);
        inventario.setItem(cafe);
        inventario.setItem(espada);
        inventario.ordenarItens(TipoOrdenacao.DESC);
        ArrayList<Item> esperado = new ArrayList<>(Arrays.asList(escudo, cafe, espada));
           
        assertEquals(esperado, inventario.getItens());
    }
}