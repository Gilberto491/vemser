public class Elfo extends Personagem implements Atacar{
    /* nome
     * arco
     * flechas
     * experiencia */
   private int indiceFlecha;
   private static int qtdElfos;
  
   {
    this.indiceFlecha = 1;   
   }   
    
   public Elfo( String nome ) {
       super(nome);
       this.vida = 100.0;
       this.inventario.setItem(new Item(1,"Arco"));
       this.inventario.setItem(new Item(2, "Flechas"));
       qtdElfos++;
   }
   
   @Override
   public void finalize() throws Throwable{
     Elfo.qtdElfos--;
   }
   
   public static int getQtdElfos(){
       return Elfo.qtdElfos; 
   }
    
   public void atacarDwarf(Dwarf dwarf) {
        this.atirarFlecha(dwarf);
    }
    
   public Item getFlecha() {
       return this.inventario.getItem(indiceFlecha);
   }
    
   public int getQtdFlechas() {
       return this.getFlecha().getQuantidade();
   }
    
   protected boolean podeAtirarFlechas(){
       return this.getQtdFlechas() > 0;
   }
   
   public void atirarFlecha(Dwarf dwarf) {
       int qtdAtual = this.getQtdFlechas();
       if( podeAtirarFlechas() ) {
           this.getFlecha().setQuantidade(qtdAtual - 1);
           aumentarXP();
           this.sofrerDano();
           dwarf.sofrerDano();
       }
   }
   
   public String imprimirNomeDaClasse(){
       return "Elfo"; 
   }
}