import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class ItemSempreExistenteTest{
    
    @Test
    public void naoPodeSerZerado(){
        Item item = new ItemSempreExistente(1, "espada");
        item.setQuantidade(0);
        
        assertEquals(1, item.getQuantidade());
    }
    
    @Test
    public void naoPodeSerNegativo(){
        Item item = new ItemSempreExistente(1, "espada");
        item.setQuantidade(-10);
        
        assertEquals(1, item.getQuantidade());
    }
    
    @Test
    public void rodeReceberOutraQuantidade(){
        Item item = new ItemSempreExistente(2, "Espada forjada");
        item.setQuantidade(1);
       
        assertEquals(1, item.getQuantidade());
    }
    
    @Test
    public void naoPodeSerCriadoComZero(){
       Item item = new ItemSempreExistente(0, "Espada forjada");   
       item.setQuantidade(1);
          
       assertEquals(1, item.getQuantidade()); 
    }
}