public class DwarfBarbaLonga extends Dwarf{
  
    private Sorteador sorteador;
    public DadoD6 dado;
    
   {
    this.dado = new DadoD6();
   }
    
   public DwarfBarbaLonga(String nome){
     super(nome);
     this.qtdDano = 10.0;
     this.vida = 110.0;
     this.sorteador = new DadoD6();
   }
   
   public DwarfBarbaLonga(String nome, Sorteador sorteador){
     super(nome);
     this.qtdDano = 10.0;
     this.vida = 110.0;
     this.sorteador = sorteador;
   }
    
   public void sofrerDano(){
     boolean devePerderVida = sorteador.sortearDado() <= 4;
     if(devePerderVida){
       super.sofrerDano();   
     }
   }
}