import java.util.*;

public class ExercitoQueAtaca extends Exercito{
    private EstrategiaDeAtaque estrategia;
    
    public ExercitoQueAtaca( EstrategiaDeAtaque estrategia ) {
        this.estrategia = estrategia;
    }
    
    public void trocarEstrategia( EstrategiaDeAtaque estrategia ) {
        this.estrategia = estrategia;
    }
    
    public void atacar(ArrayList<Dwarf> dwarfs){
        ArrayList<Elfo> ordem = this.estrategia.getOrdemDeAtaque(this.listaDeElfosAlistados());
        for ( Elfo elfo : ordem ) {
            for ( Dwarf dwarf : dwarfs ) {
                elfo.atirarFlecha(dwarf);
            }
        }
    }
}