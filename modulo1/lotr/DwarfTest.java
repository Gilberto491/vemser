import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class DwarfTest{
    
 @Test 
 public void dwardDeveTer110DeVida(){
    Dwarf dwarfQualquer = new Dwarf("anao maligno");
    
    assertEquals(110.0, dwarfQualquer.getVida(), .000001);
    assertEquals( Status.RECEM_CRIADO, dwarfQualquer.getStatus() );
 }
   
 @Test 
 public void dwardDevePerder10DeVida(){
     Dwarf dwarfQualquer = new Dwarf("anao maligno");
     Elfo elfoQualquer = new Elfo("Legolas");
     
     elfoQualquer.atirarFlecha(dwarfQualquer);
     assertEquals(100.0, dwarfQualquer.getVida(), .000001);
     assertEquals(Status.SOFREU_DANO, dwarfQualquer.getStatus());
 }
    
 @Test
 public void atirarFlechaDevePerderFlechaAumentarXP() {
     Elfo elfoQualquer = new Elfo("Legolas");
     Dwarf anaoQualquer = new Dwarf ("anao maligno"); 
     elfoQualquer.atirarFlecha(anaoQualquer);
     
     assertEquals(1, elfoQualquer.getQtdFlechas());
     assertEquals(1, elfoQualquer.getExperiencia());
 }
    
 @Test
 public void dwarfZeraVida(){
      Dwarf anaoQualquer = new Dwarf ("anao maligno");
      for(int i = 0; i<12; i++){
         anaoQualquer.sofrerDano();
      }
      
      assertEquals(0.0, anaoQualquer.getVida(), .01);
      assertEquals(Status.MORTO, anaoQualquer.getStatus());
 }
 
 @Test
 public void dwarfEstaComecandoComEscudo(){
    Dwarf anaoQualquer = new Dwarf("anao maligno");
    Item esperado = new Item(1, "Escudo");
    Item resultado = anaoQualquer.getInventario().getItem(0);
    
    assertEquals(resultado, esperado);
 }
 
 @Test
 public void dwarfEstaEquipandoItem(){
     Dwarf anaoQualquer = new Dwarf("anao maligno");
     anaoQualquer.equiparItem();
     
     assertEquals(anaoQualquer.getStatus(), Status.ESTA_EQUIPADO);
 }
 
 @Test
 public void dwardEstaTomandoMenosDano(){
     Dwarf anaoQualquer = new Dwarf("anao maligno");
     Elfo elfoQualquer = new Elfo("Legolas");
     anaoQualquer.equiparItem();
     elfoQualquer.atirarFlecha(anaoQualquer);
     
     assertEquals(105.0, anaoQualquer.getVida(), .000001);
 }
}