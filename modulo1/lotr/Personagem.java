public abstract class Personagem{
   protected String nome;
   protected Status status;
   protected Inventario inventario;
   protected double vida;
   protected int experiencia, qtdExperienciaPorAtaque;
   protected double qtdDano;
   
   { 
     this.experiencia = 0;
     this.inventario = new Inventario();
     this.status = Status.RECEM_CRIADO;
     this.qtdExperienciaPorAtaque = 1;
     this.qtdDano = 0.0;
     this.vida = 110.0;
   }
   
   public Personagem( String nome ){
       this.nome = nome; 
   }
   
   public String getNome() {
        return this.nome;
   }
    
   public void setNome( String nome ) {
        this.nome = nome;
   }
   
   public Status getStatus() {
        return this.status;
   }
   
   public Inventario getInventario(){
        return this.inventario; 
   }
   
   public double getVida(){
        return this.vida; 
   }
   
   public int getExperiencia() {
       return this.experiencia;
   }
   
   public void ganharItem(Item item){
       this.inventario.setItem(item);
   }
   
   public void perderItem(Item item){
       this.inventario.removeItem(item);
   }
   
   protected void aumentarXP(){
       this.experiencia += qtdExperienciaPorAtaque;
   }
   
   protected boolean podeSofrerDano(){
       return this.vida > 0;
   }
   
   protected double calcularDano(){
     return this.qtdDano;
   }
   
   protected void sofrerDano(){  
      this.vida -= this.vida >= calcularDano() ? calcularDano() : this.vida;
      this.status = podeSofrerDano() ? Status.SOFREU_DANO : Status.MORTO;
   }
   
   public abstract String imprimirNomeDaClasse();
}