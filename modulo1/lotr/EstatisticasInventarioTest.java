import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class EstatisticasInventarioTest{
    
    @Test
    public void calcularMediaInventarioVazio() {
        Inventario inventario = new Inventario();
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertTrue(Double.isNaN(resultado));
    }
    
    @Test
    public void calcularMediaUmItem() {
        Inventario inventario = new Inventario();
        Item escudo = new Item(3, "Escudo de Madeira");
        inventario.setItem(escudo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMedia();
        assertEquals(3, resultado, 1e-8);
    }
    
    @Test
    public void mediaDasEstatisticasDoInventario(){
        Inventario itens = new Inventario();
        EstatisticasInventario inventarioQualquer = new EstatisticasInventario(itens);
        Item adaga = new Item(2,"Adaga");
        Item flechas = new Item(12,"Flechas");
        itens.setItem(adaga);
        itens.setItem(flechas);
        
        assertEquals(7, inventarioQualquer.calcularMedia(), 1e-8);
    }
    
    @Test
    public void calcularMedianaApenasUmItem() {
        Inventario inventario = new Inventario();
        Item flechasDeGelo = new Item(6, "Flechas de Gelo");
        inventario.setItem(flechasDeGelo);
        EstatisticasInventario estatisticas = new EstatisticasInventario(inventario);
        double resultado = estatisticas.calcularMediana();
        
        assertEquals(6, resultado, 1e-8);
    }
    
    @Test
    public void medianaDasEstatisticasDoInventario(){
        Inventario itens = new Inventario();
        EstatisticasInventario inventarioQualquer = new EstatisticasInventario(itens);
        Item adaga = new Item(2,"Adaga");
        Item flechas = new Item(12,"Flechas");
        Item arco = new Item(22,"arco");
        Item espada = new Item(44,"espada");
        itens.setItem(adaga);
        itens.setItem(flechas);
        itens.setItem(arco);
        itens.setItem(espada);
      
        assertEquals(17, inventarioQualquer.calcularMediana(),1);
    }
    
    @Test
    public void medianaDasEstatisticasDoInventarioComQuantidadeImpar(){
        Inventario itens = new Inventario();
        EstatisticasInventario inventarioQualquer = new EstatisticasInventario(itens);
        Item adaga = new Item(2,"Adaga");
        Item flechas = new Item(12,"Flechas");
        Item arco = new Item(22,"arco");
        itens.setItem(adaga);
        itens.setItem(flechas);
        itens.setItem(arco);

        assertEquals(12, inventarioQualquer.calcularMediana(),1);
    }
    
    @Test
    public void quantidadeDeItensAcimaDaMedia(){
        Inventario itens = new Inventario();
        EstatisticasInventario inventarioQualquer = new EstatisticasInventario(itens);
        Item adaga = new Item(2,"Adaga");
        Item flechas = new Item(12,"Flechas");
        Item arco = new Item(22,"arco");
        Item espada = new Item(44,"espada");
        itens.setItem(adaga);
        itens.setItem(flechas);
        itens.setItem(arco);
        itens.setItem(espada);
        
        assertEquals(1, inventarioQualquer.qtdItensAcimaDaMedia(),1);
    }
}
