import java.util.*;

public class Inventario extends Object{
    
  private String nome;  
  private ArrayList<Item> itens;
  private int i;
  
  { 
  this.i = 0;
  }
 
  public Inventario(){
      this.itens = new ArrayList<>();
  }
  
  public ArrayList<Item> getItens(){
      return this.itens;
  }
 
  public void setItem(Item item){ //adicionar
      this.itens.add(item);
  }  
  
  public Item getItem(int posicao){ //obter
     return this.itens.get(posicao);
  }
    
  public void removeItem(int posicao){ //remover 
       this.itens.remove(posicao);  
  }
  
  public void removeItem(Item item){ //remover por item
       this.itens.remove(item); 
  }
  
  public String getDescricao(){
      StringBuilder descricoes = new StringBuilder();
      for (Item item : this.itens){ 
           descricoes.append(item.getDescricao());
           descricoes.append(",");
        }
         return descricoes.toString().substring(0, (descricoes.length() - 1));
      }
    
  public Item getItemMaior(){
     int indice = 0, maiorQuantidadeParcial = 0;
     for (int i = 0; i < this.itens.size(); i++){
         int qtdAtual = this.itens.get(i).getQuantidade();
         if(qtdAtual > maiorQuantidadeParcial){
            maiorQuantidadeParcial = qtdAtual;
            indice = i;
         }
     }
     return this.itens.size() > 0 ? this.itens.get(indice) : null;
  }  
  
  public Item buscarString(String buscar){
    for(Item item : this.itens){
        if(item.getDescricao().equals(buscar)){
           return item;
        }
    } 
    return null;
  }
  
  public ArrayList<Item> inverter(){
    ArrayList<Item> listaInvertida = new ArrayList<>();
    for(int i = this.itens.size() - 1; i >= 0; i--){
        listaInvertida.add(this.itens.get(i)); 
    }
    return listaInvertida;
  }
  
  public void ordenarItens(){
      this.ordenarItens(TipoOrdenacao.ASC);
  }
  
  public void ordenarItens(TipoOrdenacao ordenacao){
     for(int i = 0; i < this.itens.size(); i++){
        for( int j = 0; j < this.itens.size() - 1; j++){
          Item atual = this.itens.get(j);
          Item proximo = this.itens.get(j + 1);
          boolean deveTrocar = ordenacao == TipoOrdenacao.ASC ? atual.getQuantidade() > proximo.getQuantidade() : atual.getQuantidade() < proximo.getQuantidade();
          if(deveTrocar){
              Item itemTrocado = atual;
              this.itens.set(j, proximo);
              this.itens.set(j + 1, itemTrocado);
          }
        }
     }
  }
}