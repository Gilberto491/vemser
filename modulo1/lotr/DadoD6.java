import java.util.Random;

public class DadoD6 implements Sorteador{
  
  Random random;
    
  {
    this.random = new Random();  
  }
  
  public int sortearDado(){
    return random.nextInt(6) + 1;  
  }
  
  public int sortearDado(int numero){ //sortear dado com numero definido para testes
    return numero;
  }
  
}