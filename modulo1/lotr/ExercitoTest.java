import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import java.util.*;

public class ExercitoTest{
    
    @Test
    public void exercitoDeElfosComecandoZerado(){
       Exercito exercito = new Exercito();
       assertEquals(0,exercito.quantidadeElfosAlistados());
    }
    
    @Test 
    public void alistandoApenasUmElfoValido(){
       ElfoNoturno elfoQualquer = new ElfoNoturno("elfo");
       Exercito exercito = new Exercito();
       exercito.alistarElfos(elfoQualquer);
      
       assertTrue(exercito.listaDeElfosAlistados().contains(elfoQualquer));
    }
    
    @Test
    public void alistandoMaisELfosValidos(){
       ElfoNoturno elfoQualquer = new ElfoNoturno("elfo");
       ElfoNoturno elfoQualquer1 = new ElfoNoturno("elfo1");
       ElfoVerde elfoQualquer2 = new ElfoVerde("elfo2");
       ElfoVerde elfoQualquer3 = new ElfoVerde("elfo3");
       ElfoNoturno elfoQualquer4 = new ElfoNoturno("elfo4");
    
       Exercito exercito = new Exercito();
       exercito.alistarElfos(elfoQualquer);
       exercito.alistarElfos(elfoQualquer1);
       exercito.alistarElfos(elfoQualquer2);
       exercito.alistarElfos(elfoQualquer3);
       exercito.alistarElfos(elfoQualquer4);
      
       assertEquals(5, exercito.quantidadeElfosAlistados());    
    }
    
    @Test
    public void alistandoElfosInvalidos(){
       ElfoDaLuz elfoQualquer = new ElfoDaLuz("elfo");
    
       Exercito exercito = new Exercito();
       exercito.alistarElfos(elfoQualquer);
    
       assertFalse(exercito.listaDeElfosAlistados().contains(elfoQualquer));  
    }
    
    @Test
    public void pegandoStatusDosElfosRecemCriado(){
        ElfoNoturno elfoQualquer = new ElfoNoturno("elfo");
        Exercito exercito = new Exercito();
        exercito.alistarElfos(elfoQualquer);
        Status status = Status.RECEM_CRIADO;

        assertTrue(exercito.buscarListaPorStatus(status).contains(elfoQualquer));
    }
}