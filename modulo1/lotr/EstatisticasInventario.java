public class EstatisticasInventario{
   
   private Inventario inventario = new Inventario(); 
 
   public EstatisticasInventario(Inventario inventario){
      this.inventario = inventario;
   }
    
   public double calcularMedia(){
      if(this.inventario.getItens().isEmpty()){ 
          return Double.NaN;
      }
     
      double calcularMedia = .0;
      for(Item item : this.inventario.getItens()){
           calcularMedia += item.getQuantidade();
        }
        
      return calcularMedia/this.inventario.getItens().size();   
   }
    
   public double calcularMediana(){ 
      if(this.inventario.getItens().isEmpty()){ 
          return Double.NaN;
      }
      
         if( this.inventario.getItens().size() % 2 == 0){
            double tamanhoUmEsquerda = this.inventario.getItens().size() / 2 + 0.5;
            double tamanhoDoisDireita = (tamanhoUmEsquerda - 1);
            double resultadoMediana = (this.inventario.getItem((int)tamanhoUmEsquerda).getQuantidade() + this.inventario.getItem((int)tamanhoDoisDireita).getQuantidade())/2;
            return resultadoMediana;
         }else{
            int meio = (this.inventario.getItens().size() - 1) / 2;
            double resultadoMediana = this.inventario.getItem(meio).getQuantidade();
            return resultadoMediana;
         }

   }
    
   public double qtdItensAcimaDaMedia(){
      double acimaDaMedia = this.calcularMedia();
      int contador = 0;
      for(Item item : this.inventario.getItens()){
         if(acimaDaMedia < item.getQuantidade()){
             contador++;
         }
      }
      return contador;
   }
}