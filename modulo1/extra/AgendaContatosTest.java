import static org.junit.Assert.*;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;

public class AgendaContatosTest{
    @Test
    public void verificandoSeAlistaComecaVazia(){
      AgendaContatos agenda = new AgendaContatos();
    
      assertEquals(0,  agenda.quantidadeContatosAgendados());
    }
    
    @Test
    public void adicionandoApenasUmContato(){
      AgendaContatos agenda = new AgendaContatos();
      agenda.adicionarContato("Adriano" , "982726812");
      String resultado = agenda.consultar("Adriano");
      
      assertEquals(resultado, "982726812");
    }
    
    @Test
    public void adicionandoMaisDeUmContatosGerandoCSV(){
      AgendaContatos agenda = new AgendaContatos();
      agenda.adicionarContato("Adriano" , "982726812");
      agenda.adicionarContato("Alice" , "882435324");
      String resultado = String.format("Adriano, 982726812\nAlice, 882435324\n");
      assertEquals(resultado, agenda.csv());
    }
    
    @Test 
    public void procurandoTelefonePorNome(){
      AgendaContatos agenda = new AgendaContatos();
      agenda.adicionarContato("Adriano" , "982726812");
      String resultado = agenda.consultarPeloTelefone("982726812");
      
      assertEquals("Adriano", resultado);
    }
    
    @Test
    public void procurandoNomePorTelefone(){
      AgendaContatos agenda = new AgendaContatos();
      agenda.adicionarContato("Adriano" , "982726812");
      String resultado = agenda.consultarPeloNome("Adriano");
      
      assertEquals("982726812", resultado);
    }
}