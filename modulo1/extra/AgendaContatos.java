import java.util.*;

public class AgendaContatos{
  
  HashMap<String, String> contatos;
  
  public AgendaContatos(){
    this.contatos = new LinkedHashMap<>();
  }
  
  public void adicionarContato(String nome, String telefone){
      contatos.put(nome, telefone);
  }
  
  public String consultar(String nome){
    return contatos.get(nome);  
  }
  
  public String csv(){
    StringBuilder builder = new StringBuilder();
    String separador = System.lineSeparator();
    for( HashMap.Entry<String, String> par : contatos.entrySet()){
       String chave = par.getKey();
       String valor = par.getValue();
       String contato = String.format("%s, %s%s", chave, valor, separador);
       builder.append(contato);
    }
    return builder.toString();
  }
  
  public String consultarPeloTelefone(String telefone){
    for(HashMap.Entry<String, String> par : contatos.entrySet()) {
      if(par.getValue().equals(telefone)){
        return par.getKey();  
      }
    } 
    return null;
  }
  
  public String consultarPeloNome(String nome){
    for(HashMap.Entry<String, String> par : contatos.entrySet()) {
      if(par.getKey().equals(nome)){
        return par.getValue();  
      }
    } 
    return null;
  }
 
  public int quantidadeContatosAgendados(){
       return contatos.size();
    }
}